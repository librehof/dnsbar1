### dnsbar 1 | changelog

AGPL (C) 2024 [librehof.org](http://librehof.org) [(top)](/README.md)

2024-04-12 (v1.5)
- added some unit tests
- minor fixes/improvements

2024-04-09 (v1.4)
- fix: daily temp list newline

2024-04-08 (v1.3)
- daily list "diff" log (diff_cmd)

2024-04-06 (v1.2)
- fix: conf/ruleset report now with report_log_dir
