## dnsbar 1

AGPL (C) 2024 [librehof.org](http://librehof.org)

### Summery

- **Blocks DNS queries based on whitelists** (or blacklists)
- Placed between LAN hosts (on inner network) and DNS server
- Written in bare metal C++ with minimum amount of code and **dependencies**:
  - Run: **GNU/Linux**
  - Build: **g++** (c++11)
- Special features:
  - Updating nftables "whitelist" IP set (before IP is returned to client)
  - Can sync lists and reports with a remote host (using ssh/scp)
- Generates periodic reports and statistics, see **Directory content**
- List format:
  - \# comment
  - **BEGIN** \# optional begin mark
  - **domain.name** \# exact domain
  - **\*.domain.name** \# wildcard base name (including base)
  - **!some.domain.name** \# exclude from wildcard domain
  - **n.n.n.n** \# include any domain with this IPv4 answer
  - **END** \# end mark (required if begin mark)
- Easy to build from source (know what you run)
  - `./build [debug|release]`
- Changelog [here](doc/changelog.md)  

### Download and build

```
git clone --depth 10 https://gitlab.com/librehof/dnsbar1.git
cd dnsbar1
sudo apt update
sudo apt install g++ # or build-essential
./build release
```

### [Re]install

```
# cd dnsbar1
git pull # if you want the latest
md5sum --check meta/source.md5
./build release
sudo ./install
```

### [Re]install in black mode

```
# cd dnsbar1
git pull # if you want the latest
md5sum --check meta/source.md5
./build release
sudo ./install black
```

### [Re]install from staging host

Run from staging host with g++ installed and same OS environment as target:
```
# cd dnsbar1
git pull # if you want the latest
md5sum --check meta/source.md5
./distribute <user>@<targethost>
ssh <user>@<targethost>
cd dnsbar
[sudo] ./install [black]
```

### TODO

- TCP support (needed or not?)

### Details

- See **templates/** for config and service examples
- Usage:
  - `./dnsbar [<confdir>]`
- Default confdir:
  - root: **/etc/dnsbar**
  - non-root: **bin_\<arch\>\[d\]/../conf**

### Directory content

conf = "current directory" when running

- bin = /sbin or ./bin_\<arch\>
  - **dnsbar**
- conf = /etc/dnsbar, /etc/dnsblack or ./conf
  - **dnsbar.conf**
  - \*.**list** # static list
- var = /var/lib/dnsbar, /var/lib/dnsblack or ./data
  - \*.**list** # dynamic list
  - \*.**list+** # to be applied on next sync
  - \*.**temp** # dynamic list, commented-out daily
  - \*.**temp+** # to be applied on next sync
- report = /var/lib/dnsbar, /var/lib/dnsblack or ./data
  - **dnsbar-conf.txt** # conf at startup
  - **dnsbar-ruleset.txt** # current conf+lists
  - **dnsbar-illegal.packet** # last illegal (binary packet)
  - **dnsbar-passed.txt** # passed (accumulated during the day)
  - **dnsbar-blocked.txt** # blocked during last sync period
  - **dnsbar-blocked.txt**.*n* # intra-day history
  - **dnsbar-sync.err** # error message if last sync failed
  - **dnsbar-sync.ok** # touched at successful end-of-sync
  - **dnsbar-sync.done** # touched at end-of-sync
- log = /var/log/dnsbar, /var/log/dnsblack or ./data
  - **dnsbar**-*mm*-*dd*.**txt** # daily ruleset+passed+blocked
  - **dnsbar**-*mm*-*dd*.**diff** # daily list changes

### Setup with nftables and local DNS service

Prerequisite: Linux host with some DNS service + nftables + dnsbar
- DNS service typically binds at some LAN n.n.n.**1:53** and loopback **127.0.0.1:53**
- dnsbar binds at n.n.n.**1:54** (53+1)
- nftables rule preroutes all n.n.n.**1:53** to n.n.n.**1:54** (port redirect)
- dnsbar will forward to DNS service at **127.0.0.1:53** and process responses
- verified to work with [dnsmasq](https://thekelleys.org.uk/dnsmasq/doc.html)

#### dnsbar.conf example
```
...

# this service
incoming_ip="n.n.n.1"
incoming_port=54

# actual DNS service
outgoing_ip="127.0.0.1"

...

restart_hour=04 # hour of day (host time)
restart_internal=1 # 0 = restarted by systemd (on-failure)

# clear nft4 set and nft6 set on [re]start
nft_clear=1

# nftables ip4 set
nft4_family="ip"
nft4_table="filter"
nft4_set="white4out"

...
```

#### nftables.conf example
```
...

table ip filter {

  set white4out {
    type  ipv4_addr
    size  40000
    flags timeout
    timeout 30h
  }

...

table ip nat {

  chain prerouting {
    type nat hook prerouting priority dstnat; policy accept;

    ip daddr n.n.n.1 udp dport 53 redirect to :54
    ip daddr n.n.n.1 tcp dport 53 redirect to :54 # not used (no reply)

...
```

### Debug

- sudo apt install clangd # language server
- VSCodium (or vscode) - https://github.com/VSCodium/vscodium/releases
- Extensions:
  - clangd
  - CodeLLDB
  - Code Spell Checker 
- copy dev/launch.json, dev/settings.json and dev/tasks.json to .vscode/
- allow use of port 53 when performing non-root debugging:
  - `sudo sysctl net.ipv4.ip_unprivileged_port_start=0`
- prepare configuration:
  - `cp ./templates/dnsbar.conf ./conf/`
  - set `incoming_ip="127.0.0.1"`
  - set `outgoing_ip="<your DNS server as given in /etc/resolv.conf>"`
- temporarly edit your /etc/resolv.conf
  - set `nameserver 127.0.0.1`
