#!/bin/bash
set -e

SCRIPT="$(readlink -f "${0}")"
ORIGIN="$(dirname "${SCRIPT}")"
cd "${ORIGIN}"
ORIGIN="$(pwd)"

IFS="
"

#------------------------------------
# default

LABEL="$(basename "${ORIGIN}")"
if [ -f "meta/label.var" ]; then
  LABEL="$(cat "meta/label.var")"
fi

VERSION="dev"
if [ -f "meta/version.var" ]; then
  VERSION="$(cat "meta/version.var")"
fi

CPPDIR="cpp11"
if [ ! -d "${CPPDIR}" ]; then
  echo "Missing: ${CPPDIR}"
  exit 2
fi

#------------------------------------
# input

if [ "${1}" = "" ]; then
  MAIN="${LABEL}"
  BUILD="debug"
  METHOD="update"
elif [ -f "${CPPDIR}/${1}.h" ] || [ -f "${CPPDIR}/${1}.cpp" ]; then
  MAIN="${1}"
  BUILD="${2}"
  METHOD="${3}"
else
  MAIN="${LABEL}"
  BUILD="${1}"
  METHOD="${2}"
fi

if [ "${BUILD}" = "" ]; then
  BUILD="debug"
  METHOD="update+"
fi

if [ "${METHOD}" = "" ]; then
  METHOD="all"
fi

if [ "${BUILD}" != "debug" ] &&
   [ "${BUILD}" != "release" ] &&
   [ "${BUILD}" != "small" ] &&
   [ "${BUILD}" != "fast" ]
then
  echo "Usage: ${0} [<main>] [debug|release|small|fast [all|update][+]]"
  exit 2
fi

CPU="single"
COMMENT="${METHOD}"
if [ "${METHOD}" = "all+" ]; then
  METHOD="all"
  CPU="multi"
elif [ "${METHOD}" = "update+" ]; then
  METHOD="update"
  CPU="multi"
fi

if [ "${METHOD}" != "all" ] && [ "${METHOD}" != "update" ]; then
  echo "Unknown method: ${METHOD}"
  exit 2
fi

#------------------------------------
# init

ARCH="$(uname -m)"
if [ "${ARCH}" = "" ]; then
  ARCH="x86_64"
fi
if [ "${BUILD}" = "debug" ]; then
  ARCH="${ARCH}d"
fi

TMPDIR="/tmp/${MAIN}_${ARCH}"

BINDIR="bin_${ARCH}"
BINFILE="${BINDIR}/${MAIN}"

mkdir -p "${TMPDIR}"
mkdir -p "${BINDIR}"

echo "Building ${BUILD} ${MAIN} ${VERSION} (${COMMENT})"

if [ "${CPU}" = "multi" ] && [ -e "/proc/cpuinfo" ]; then
  CORES="$(grep -c processor /proc/cpuinfo)"
  if [ ${CORES} -ge 2 ]; then
    echo "Multicore (${CORES})"
  elif [ ${CORES} = 1 ]; then
    echo "Singlecore!"
    sleep 1
  fi
fi

if [ "${METHOD}" = "all" ]; then
  echo "Cleaning"
  rm -rf "${TMPDIR}"
  mkdir "${TMPDIR}"
fi
rm -f "${TMPDIR}/multi.log"
rm -f "${BINFILE}"

OBJFILES=""

#------------------------------------
# fingerprint

# source hash
NEWHASH=""
if [ "${METHOD}" = "all" ] || [ ! -f "${CPPDIR}/repo.h" ]; then
  # exclude repo.h from hash
  rm -f "${CPPDIR}/repo.h"
  # copy repo version
  if [ -f "${CPPDIR}/${LABEL}-module.h" ]; then
    echo "#define MODULE_LABEL \"${LABEL}\"" > "${CPPDIR}/${LABEL}-module.h"
    echo "#define MODULE_VERSION \"${VERSION}\"" >> "${CPPDIR}/${LABEL}-module.h"
    if [ -f "meta/owner.list" ]; then
      owners=""
      ownerlist="$(cat "meta/owner.list")"
      for owner in ${ownerlist}
      do
        if [ "${owner}" != "" ]; then
          if [ "${owners}" = "" ]; then
            owners="${owner}"
          else
            owners="${owners}, ${owner}"
          fi
        fi
      done
      echo "#define MODULE_OWNERS \"${owners}\"" >> "${CPPDIR}/${LABEL}-module.h"
    fi
  fi
  # old hash
  if [ -f "meta/hash.var" ]; then
    OLDHASH="$(cat "meta/hash.var")"
  else
    OLDHASH=""
  fi
  # new hash
  find ./cpp11 -type f -exec md5sum {} + | LC_ALL=C sort > "meta/source.md5"
  HASH="$(md5sum "meta/source.md5" | cut -d ' ' -f 1)"
  if [ "${HASH}" != "${OLDHASH}" ]; then
    echo "New hash!"
    NEWHASH="${HASH}"
    echo "${NEWHASH}" > "meta/hash.var"
  fi
  echo "Hash: ${HASH}"
fi

# source date
if [ ! -f "meta/date.var" ]; then
  echo "Initial date"
  NEWDATE="$(date +%Y%m%d)"
  echo "${NEWDATE}" > "meta/date.var"
  rm -f "${CPPDIR}/repo.h"
  sleep 1
fi
DATE="$(cat "meta/date.var")"
if [ "${NEWHASH}" != "" ]; then
  NEWDATE="$(date +%Y%m%d)"
  if [ "${DATE}" != "${NEWDATE}" ]; then
    echo "New date!"
    sleep 1
    echo "${NEWDATE}" > "meta/date.var"
    rm -f "${CPPDIR}/repo.h"
    sleep 1
  fi
  DATE="$(cat "meta/date.var")"
fi

# repo.h
if [ ! -f "${CPPDIR}/repo.h" ]; then
  echo "#define REPO_LABEL \"${LABEL}\"" > "${CPPDIR}/repo.h"
  echo "#define REPO_VERSION \"${VERSION}\"" >> "${CPPDIR}/repo.h"
  echo "#define REPO_DATE ${DATE}" >> "${CPPDIR}/repo.h"
  echo "#ifdef NDEBUG" >> "${CPPDIR}/repo.h"
  echo "#define REPO_HASH \"${HASH}\"" >> "${CPPDIR}/repo.h"
  echo "#else" >> "${CPPDIR}/repo.h"
  echo "#define REPO_HASH \"debug\"" >> "${CPPDIR}/repo.h"
  echo "#endif" >> "${CPPDIR}/repo.h"
fi

echo "Date: ${DATE}"
echo "Cache: ${TMPDIR}"

#------------------------------------
# compile source

# ${1}="<relative cpp path>"
# ${2}="single|multi"
# all="true|false"
# BUILD="debug|release|small|fast"

# multi pid >> "${TMPDIR}/multi.pids"
# multi log >> "${TMPDIR}/multi.log"

compile()
{
  cppfile="${1}"
  cpu="${2}"
  if [ ! -f "${cppfile}" ]; then
    echo "Missing: ${cppfile}"
    exit 2
  fi
  cppname="$(basename "${cppfile}")"
  objfile="${TMPDIR}/${cppname}.o"

  # add objfile to global link list
  if [ "${OBJFILES}" = "" ]; then
    OBJFILES="${objfile}"
  else
    OBJFILES="${OBJFILES} ${objfile}"
  fi

  skip="true"
  if [ "${all}" = "true" ]; then
    skip="false"
  elif [ ! -f "${objfile}" ]; then
    skip="false"
  elif [ "${cppfile}" -nt "${objfile}" ]; then
    skip="false"
  fi
  if [ "${skip}" = "true" ]; then
    return # skip
  fi

  # compile
  rm -f "${objfile}"
  if [ "${cpu}" = "multi" ]; then
    # multi
    if [ "${BUILD}" = "small" ]; then
      echo "${cppfile} (small+)"
      g++ -c -s -Os -DNDEBUG -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}" >> "${TMPDIR}/multi.log" &
      pid=$!
    elif [ "${BUILD}" = "fast" ]; then
      echo "${cppfile} (fast+)"
      g++ -c -O3 -DNDEBUG -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}" >> "${TMPDIR}/multi.log" &
      pid=$!
    elif [ "${BUILD}" = "release" ]; then
      echo "${cppfile} (release+)"
      g++ -c -O2 -DNDEBUG -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}" >> "${TMPDIR}/multi.log" &
      pid=$!
    else
      echo "${cppfile} (+)"
      g++ -c -Wall -Wconversion -g -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}" >> "${TMPDIR}/multi.log" &
      pid=$!
    fi
    echo "${pid}" >> "${TMPDIR}/multi.pids"
  else
    # single
    if [ "${BUILD}" = "small" ]; then
      echo "${cppfile} (small)"
      g++ -c -s -Os -DNDEBUG -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}"
    elif [ "${BUILD}" = "fast" ]; then
      echo "${cppfile} (fast)"
      g++ -c -O3 -DNDEBUG -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}"
    elif [ "${BUILD}" = "release" ]; then
      echo "${cppfile} (release)"
      g++ -c -O2 -DNDEBUG -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}"
    else
      echo "${cppfile}"
      g++ -c -Wall -Wconversion -g -DMAIN=\"${MAIN}\" -std=c++11 "${cppfile}" -o "${objfile}"
    fi
  fi
}

#------------------------------------

# ${1}="<header name>"
# all="true|false"
# oldest="<oldest objfile>"

# if hfile>oldest:
#  will set all="true" and newer="<info>"

header()
{
  if [ "${1}" = "" ]; then
    return
  fi
  if [ "${all}" = "true" ]; then
    return
  fi

  hname="${1}"
  hfile="${CPPDIR}/${hname}"
  if [ ! -f "${hfile}" ]; then
    echo "Missing: ${hfile}"
    exit 2
  fi

  if [ "${hfile}" -nt "${oldest}" ]; then
    all="true"
    newer="${hfile}>${oldest}"
  fi
}

#------------------------------------
# compile module

module()
{
  mname="${1}"

  # get oldest module objfile

  oldest="" # blank if "all"
  if [ "${METHOD}" != "all" ]; then
    cppfiles="$(ls -1 ${CPPDIR}/${mname}-*.cpp)"
    for cppfile in ${cppfiles}
    do
      cppname="$(basename "${cppfile}")"
      objfile="${TMPDIR}/${cppname}.o"
      if [ "${oldest}" = "" ] || [ "${objfile}" -ot "${oldest}" ]; then
        oldest="${objfile}"
      fi
    done
  fi

  # dirty header?

  all="false"
  newer=""
  if [ "${METHOD}" = "all" ]; then
    all="true"
  elif [ "${oldest}" = "" ]; then
    all="true"
  elif [ "${2}" = "" ]; then
    # check all headers < oldest
    hfiles="$(ls -1 ${CPPDIR}/*.h)"
    for hfile in ${hfiles}
    do
      hname="$(basename "${hfile}")"
      header "${hname}"
    done
  else
    # check listed headers < oldest
    header "${2}"
    header "${3}"
    header "${4}"
    header "${5}"
    header "${6}"
    header "${7}"
    header "${8}"
    header "${9}"
  fi

  if [ "${all}" = "true" ]; then
    if [ "${newer}" = "" ]; then
      echo "# ${mname} module (all)"
    else
      echo "# ${mname} module (${newer})"
    fi
  else
    echo "# ${mname} module (update)"
  fi

  # compile sources

  if [ -e "${TMPDIR}/multi.pids" ]; then
    echo "waiting 3 seconds on old ${TMPDIR}/multi.pids ..."
    sleep 3
  fi
  rm -f "${TMPDIR}/multi.pids"
  rm -f "${TMPDIR}/multi.log"

  cppfiles="$(ls -1 ${CPPDIR}/${mname}-*.cpp)"
  for cppfile in ${cppfiles}
  do
    cppname="$(basename "${cppfile}")"
    compile "${CPPDIR}/${cppname}" "${CPU}"
  done

  # multi?

  if [ -f "${TMPDIR}/multi.pids" ]; then
    # wait
    pids="$(cat "${TMPDIR}/multi.pids")"
    state="running"
    while [ "${state}" = "running" ]
    do
      state="done"
      for pid in ${pids}
      do
        if [ -e "/proc/${pid}" ]; then
          state="running"
          echo "compiling (${pid}) ..."
          sleep 1
          break
        fi
      done
    done
    rm -f "${TMPDIR}/multi.pids"
    # log
    if [ -f "${TMPDIR}/multi.log" ]; then
      cat "${TMPDIR}/multi.log"
    fi
    # check
    cppfiles="$(ls -1 ${CPPDIR}/${mname}-*.cpp)"
    for cppfile in ${cppfiles}
    do
      cppname="$(basename "${cppfile}")"
      objfile="${TMPDIR}/${cppname}.o"
      if [ ! -f "${objfile}" ]; then
        echo "ERROR: ${cppfile}"
        exit 2
      fi
    done
  fi
}

#------------------------------------
# compile single

single()
{
  name="${1}"
  cppname="${name}.cpp"
  if [ ! -f "${CPPDIR}/${cppname}" ]; then
    echo "Missing: ${CPPDIR}/${cppname}"
    exit 2
  fi

  oldest="" # blank if "all"
  if [ "${METHOD}" != "all" ]; then
    objfile="${TMPDIR}/${cppname}.o"
    oldest="${objfile}"
  fi

  # dirty header?

  all="false"
  newer=""
  if [ "${METHOD}" = "all" ]; then
    all="true"
  elif [ "${oldest}" = "" ]; then
    all="true"
  elif [ "${2}" = "" ]; then
    # check all headers < oldest
    hfiles="$(ls -1 ${CPPDIR}/*.h)"
    for hfile in ${hfiles}
    do
      hname="$(basename "${hfile}")"
      header "${hname}"
    done
  else
    # check listed headers < oldest
    header "${2}"
    header "${3}"
    header "${4}"
    header "${5}"
    header "${6}"
    header "${7}"
    header "${8}"
    header "${9}"
  fi

  if [ "${all}" = "true" ]; then
    if [ "${newer}" = "" ]; then
      echo "* ${name} (force)"
    else
      echo "* ${name} (${newer})"
    fi
  else
    echo "* ${name} (update)"
  fi

  # compile source

  compile "${CPPDIR}/${cppname}" "single"
}

#------------------------------------
# compile sources

if [ "${MAIN}" = "millitest" ]; then
  module millicpp millicpp.h
  single ${MAIN}
elif [ "${MAIN}" = "dnsbar" ]; then
  module millicpp millicpp.h
  module dnsbar
  single ${MAIN}
else
  echo "Unknown main: ${MAIN}"
  exit 2
fi

#------------------------------------
# build executable

IFS=" "

if [ "${BUILD}" = "small" ]; then
  echo "Linking: ${BINFILE} (small release)"
  g++ -s -Os -DNDEBUG -std=c++11 -pthread ${OBJFILES} -o "${BINFILE}"
elif [ "${BUILD}" = "fast" ]; then
  echo "Linking: ${BINFILE} (fast release)"
  g++ -O3 -DNDEBUG -std=c++11 -pthread ${OBJFILES} -o "${BINFILE}"
elif [ "${BUILD}" = "release" ]; then
  echo "Linking: ${BINFILE} (release)"
  g++ -O2 -DNDEBUG -std=c++11 -pthread ${OBJFILES} -o "${BINFILE}"
else
  echo "Linking: ${BINFILE} (debug)"
  g++ -g -std=c++11 -pthread ${OBJFILES} -o "${BINFILE}"
fi

if [ "${BUILD}" = "release" ] && [ "${METHOD}" = "all" ]; then
  echo "cleanup"
  rm -rf "${TMPDIR}"
fi

#------------------------------------
