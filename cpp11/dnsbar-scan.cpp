#include "dnsbar.h"

/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

void Domain::clear()
{
  this->level = 0;
  this->size = 0;
  this->name[0] = '\0';
}

bool Domain::scan(const char* packet, int psize, Subref& subdata, bool capture)
{
  clear();
  return subscan(packet, psize, subdata, capture);
}

bool Domain::subscan(const char* packet, int psize, Subref& subdata, bool capture)
{
  this->level++;
  if( this->level > 10 ) return false;
  for(int labels=0;;labels++) {
    // safe-guard
    if( labels > 64 ) return false;
    // len
    if( !subdata.span(1) ) return false;
    uint8_t len = subdata.uint8();
    if( !subdata.step() ) return false;
    if( (len & 0xC0) == 0 ) {
      // inplace label
      if( len == 0 ) {
        // null label
        break; // end of name
      }
      if( !subdata.span(len) ) return false;
      if( (this->size+len) > 260 ) return false;
      if( capture ) {
        int dest = this->size;
        memcpy(this->name+dest, subdata.ptr, subdata.size);
        dest += subdata.size;
        this->name[dest++] = '.';
        this->name[dest] = '\0';
      }
      this->size += len+1;
      if( !subdata.step() ) return false;
    }
    else if( (len & 0xC0) == 0xC0 ) {
      // jump to offset (reuse base name)
      if( !subdata.span(1) ) return false;
      uint8_t offset = subdata.uint8();
      if( !subdata.step() ) return false;
      Subref recursive(packet,0,psize);
      if( !recursive.forward(offset) ) return false;
      subscan(packet, psize, recursive, capture);
      break; // end of name
    }
    else {
      // illegal label length
      return false;
    }
  }
  if( capture ) {
    if( this->size > 0 ) {
      // remove root dot
      this->size--;
      this->name[this->size] = '\0';
    }
  }
  return true;
}

//----------------------------------------------------------------

int DnsBar::scan_response(Ip4be caller_ip, char* packet, int psize, int* form)
{
  *form = 0;
  this->query_domain.clear();
  this->answer_domain.clear();

  // *** header ***

  Subref header(packet, 0, psize);
  if( !header.span(12) ) return 0;

  uint8_t h2 = header.uint8(2);
  uint8_t h3 = header.uint8(3);
  uint16_t qdcount = header.uint16be(4);
  uint16_t ancount = header.uint16be(6);

  uint8_t qr = h2 & 0x80;
  uint8_t opcode = (h2 & 0x78) >> 3;
  uint8_t rcode = h3 & 0x0F;
  if( qr == 0 ) {
    // query
  }
  if( opcode != 0 ) {
    // not a standard query
    if( opcode <= 2 ) {
      // IQUERY or STATUS => passthrough
      return psize; // TODO: IQUERY pass/block?
    }
    else {
      // unexpected opcode => illegal
      return 0;
    }
  }
  if( rcode != 0 ) {
    // error
  }
  if( qdcount == 0 ) {
    // no query entries => passthrough
    return psize;
  }
  if( ancount == 0 ) {
    // no answer entries => passthrough
    return psize;
  }

  // *** queries ***

  Subref query(packet, 0, psize);
  query.forward(header.size);

  // loop queries (name+2+2)
  // should only be one in modern systems
  for(int i=0; i<qdcount; i++) {
    // name = label.label...
    if( !this->query_domain.scan(packet,psize,query,true) ) return 0;
    // qtype+qclass
    if( !query.span(2+2) ) return 0;
    // next
    if( !query.step() ) return 0;
  }

  string domain(this->query_domain.name, query_domain.size);

  // *** answers ***

  Subref answer(query.ptr, 0, query.space);

  // loop answers (name+2+2+4+2+rdata)
  bool passed = false;
  for(int i=0; i<ancount; i++) {
    // name = label.label...
    if( !this->answer_domain.scan(packet,psize,answer,false) ) return 0;
    // tail
    if( !answer.span(2+2+4) ) return 0;
    uint16_t ty = answer.uint16be(0);
    if( *form != 1 ) *form = ty;
    uint16_t cl = answer.uint16be(2);
    if( cl != 1 ) {
      // expected IN=1 (internet) => illegal
      return 0;
    }
    uint32_t ttl = answer.uint32be(4);
    if( ty==1 || ty==28 ) {
      // A/AAAA: check
      *form = 1;
      if( qdcount > 1 ) {
        // unsupported multi-query answer => illegal
        return 0;
      }
      // cap ttl
      if( ttl < this->ttl_min_sec ) {
        ttl = this->ttl_min_sec;
      }
      if( ttl > this->ttl_max_sec ) {
        ttl = this->ttl_max_sec;
      }
      answer.uint32be(4, ttl);
      assert( answer.uint32be(4) == ttl );
    }
    if( !answer.step() ) return 0;
    // rdlength
    if( !answer.span(2) ) return 0;
    uint16_t rdlength = answer.uint16be();
    if( !answer.step() ) return 0;
    // rdata
    if( !answer.span(rdlength) ) return 0;
    if( ty == 1 ) {
      // A: IPv4
      if( rdlength != 4 ) return 0;
      Ip4be answer_ip4;
      uint8_t* byteptr = (uint8_t*)&answer_ip4;
      memcpy(byteptr, answer.ptr, sizeof(Ip4be));
      if( answer_ip4!=0 && byteptr[0]!=127 ) {
        // non-localhost
        if( this->black ) {
          passed = black_ip4(domain, caller_ip, answer_ip4);
        }
        else {
          passed = white_ip4(domain, caller_ip, answer_ip4);
        }
        if( !passed ) {
          // block => send header with query only (0-answer)
          memset(packet+4+2, 0, 12-4-2); // clear non-query counters
          return ptr_sub(query.ptr, packet);
        }
      }
    }
    else if( ty == 28 ) {
      // AAAA: IPv6
      if( rdlength != 4+4+4+4 ) return 0;
      Ip6be answer_ip6;
      uint8_t* byteptr = (uint8_t*)&answer_ip6.dual;
      memcpy(byteptr, answer.ptr, sizeof(Ip6be));
      bool blank_ip6 = ip6_equal(answer_ip6, IP6BE_BLANK);
      bool loop_ip6 = ip6_equal(answer_ip6, IP6BE_LOOPBACK);
      if( !blank_ip6 && !loop_ip6 ) {
        // non-localhost
        if( this->black ) {
          passed = black_ip6(domain, caller_ip, answer_ip6);
        }
        else {
          passed = white_ip6(domain, caller_ip, answer_ip6);
        }
        if( !passed ) {
          // block => send header with query only (0-answer)
          memset(packet+4+2, 0, 12-4-2); // clear non-query counters
          return ptr_sub(query.ptr, packet);
        }
      }
    }
    else if( ty == 2 ) {
      // NS: authoritative name server
    }
    else if( ty == 5 ) {
      // CNAME: canonical name for an alias
      //if( !this->domain.scan(packet, psize, subdata) ) return 0;
      //detailf("CNAME = %s", dnsname.name);
    }
    else if( ty == 6 ) {
      // SOA: marks the start of a zone of authority
    }
    else if( ty == 12 ) {
      // PTR: domain name pointer
      //if( !this->domain.scan(packet, psize, subdata) ) return 0;
      //detailf("PTR = %s", dnsname.name);
    }
    else if( ty == 15 ) {
      // MX: mail exchange
    }
    else if( ty == 16 ) {
      // TXT: text strings
    }
    // next
    if( !answer.step() ) return 0;
  }
  // pass
  return psize;
}

//----------------------------------------------------------------
