#include "dnsbar.h"

/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

bool DnsBar::white_ip4(
  const string& domain, Ip4be caller_ip, Ip4be answer_ip4)
{
  // already passed?
  bool quick_pass = false;
  if( this->pdomain_ip4.contains(domain) ) {
    quick_pass = true;
  }
  else if( this->pdomain_ip6.contains(domain) ) {
    // pass because ip6 passed
    quick_pass = true;
    this->pdomain_ip4.add_missing(domain, ValVec<Ip4be>());
  }
  if( quick_pass ) {
    // quick pass => ensure ip in list
    auto& ips = this->pdomain_ip4[domain];
    for(auto& ip : ips) {
      if( ip == answer_ip4 ) {
        // existing ip4
        return true;
      }
    }
    // new ip4
    ips.add(answer_ip4);
    this->pset_ip4.add_new(answer_ip4);
    nft4_add(answer_ip4);
    return true;
  }

  // ensure caller has a block list
  if( !this->caller_blocked.contains(caller_ip) ) {
    this->caller_blocked.add_missing(caller_ip, ValSet<string>());
  }

  // get caller's block list
  auto& blocked = this->caller_blocked[caller_ip];

  // already blocked?
  if( blocked.contains(domain) ) {
    // quick domain block!
    day_blocked.add(domain);
    return false;
  }

  // *** first time ***

  // check ip against ip4nets
  for(auto entry=this->ip4nets.begin(); entry<this->ip4nets.end();)
  {
    Ip4be net = *entry++;
    Ip4be mask = *entry++;
    Ip4be answer_net = answer_ip4 & mask;
    if( answer_net == net ) {
      // first time ip4nets pass!
      this->pdomain_ip4.add_missing(domain, ValVec<Ip4be>());
      auto& ips = this->pdomain_ip4[domain];
      ips.add(answer_ip4);
      this->pset_ip4.add_new(answer_ip4);
      nft4_add(answer_ip4);
      return true;
    }
  }

  // check ip against ip4list
  if( this->ip4list.contains(answer_ip4) ) {
    // first time ip4list pass!
    this->pdomain_ip4.add_missing(domain, ValVec<Ip4be>());
    auto& ips = this->pdomain_ip4[domain];
    ips.add(answer_ip4);
    this->pset_ip4.add_new(answer_ip4);
    nft4_add(answer_ip4);
    return true;
  }

  // check domain against matchlist and wildlist/neglist
  if( check_domain(domain) ) {
    // first time domain pass!
    this->pdomain_ip4.add_missing(domain, ValVec<Ip4be>());
    auto& ips = this->pdomain_ip4[domain];
    ips.add(answer_ip4);
    this->pset_ip4.add_new(answer_ip4);
    nft4_add(answer_ip4);
    return true;
  }
  else {
    // first time domain block!
    blocked.add(domain);
    day_blocked.add(domain);
    return false;
  }
}

//----------------------------------------------------------------

bool DnsBar::white_ip6(
  const string& domain, Ip4be caller_ip, const uint64_t* answer_ip6)
{
  // already passed?
  bool quick_pass = false;
  if( this->pdomain_ip6.contains(domain) ) {
    quick_pass = true;
  }
  else if( this->pdomain_ip4.contains(domain) ) {
    // pass because ip4 passed
    quick_pass = true;
    this->pdomain_ip6.add_missing(domain, ValVec<Ip6be>());
  }
  if( quick_pass ) {
    // quick pass => ensure ip in list
    auto& ips = this->pdomain_ip6[domain];
    for(auto& ip : ips) {
      if( ip6_equal(ip,answer_ip6) ) {
        // existing ip6
        return true;
      }
    }
    // new ip6
    ips.add(answer_ip6);
    this->pset_ip6.add_new(answer_ip6);
    nft6_add(answer_ip6);
    return true;
  }

  // ensure caller has a block list
  if( !this->caller_blocked.contains(caller_ip) ) {
    this->caller_blocked.add_missing(caller_ip, ValSet<string>());
  }

  // get caller's block list
  auto& blocked = this->caller_blocked[caller_ip];

  // already blocked?
  if( blocked.contains(domain) ) {
    // quick domain block!
    day_blocked.add(domain);
    return false;
  }

  // *** first time ***

  // check domain against matchlist and wildlist/neglist
  if( check_domain(domain) ) {
    // first time domain pass!
    this->pdomain_ip6.add_missing(domain, ValVec<Ip6be>());
    auto& ips = this->pdomain_ip6[domain];
    ips.add(answer_ip6);
    this->pset_ip6.add_new(answer_ip6);
    nft6_add(answer_ip6);
    return true;
  }
  else {
    // first time domain block!
    blocked.add(domain);
    day_blocked.add(domain);
    return false;
  }
}

//----------------------------------------------------------------
