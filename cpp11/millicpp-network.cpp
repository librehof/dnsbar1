#include "millicpp.h"

/*
 * millicpp * Apache 2.0 (C) 2023 librehof.org
 */

//================================================================
// PortIp
//----------------------------------------------------------------

Ip4be IP4BE_BLANK = 0;
Ip4be IP4BE_LOOPBACK = htonl(0x7F000000);

const uint64_t* IP6BE_BLANK = (const uint64_t*)
  (const uint8_t[]) { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
const uint64_t* IP6BE_LOOPBACK = (const uint64_t*)
  (const uint8_t[]) { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1 };

//----------------------------------------------------------------

int get_ipv(const char* ip)
{
  if( str_blank(ip) ) {
    throw Error("Blank IPv4/v6 address");
  }
  const char* ver4 = strstr(ip, ".");
  const char* ver6 = strstr(ip, ":");
  if( ver4 != nullptr && ver6 != nullptr ) {
    throw Error("Unexpected IPv4/v6 address: %s", ip);
  }
  if( ver4 == nullptr && ver6 == nullptr ) {
    throw Error("Invalid IPv4/v6 address: %s", ip);
  }
  if( ver4 != nullptr ) {
    return 4;
  }
  else {
    return 6;
  }
}

//----------------------------------------------------------------

void PortIp::address(char* buffer, int space) noexcept
{
  if( buffer==nullptr || space<2 ) {
    ERROR_PLUS;
    return;
  }
  else if( space < IPSTR_SPACE ) {
    ERROR_PLUS;
    strcpy(buffer, "?");
    return;
  }
  char ip[IPSTR_SPACE];
  this->get_ip(ip,IPSTR_SPACE);
  int port = this->port();
  if( is_ip4() ) {
    if( port == 0 ) {
      snprintf(buffer, space, "%s", ip);
    }
    else {
      snprintf(buffer, space, "%s:%d", ip, port);
    }
  }
  else {
    if( port == 0 ) {
      snprintf(buffer, space, "%s", ip);
    }
    else {
      snprintf(buffer, space, "[%s]:%d", ip, port);
    }
  }
}

string PortIp::address() noexcept
{
  char address[IPSTR_SPACE];
  this->address(address, IPSTR_SPACE);
  string result = address;
  return result;
}

//----------------------------------------------------------------

bool PortIp::is_blank_ip() noexcept
{
  if( is_ip4() ) {
    return ( this->addr4.sin_addr.s_addr == 0 );
  }
  else {
    int64_t* ptr = (int64_t*)&this->addr6.sin6_addr;
    return ( ptr[0]==0 && ptr[1]==0 );
  }
}

//----------------------------------------------------------------

int PortIp::ipv() noexcept
{
  if( this->addr.sa_family == AF_INET ) {
    return 4;
  }
  else if( this->addr.sa_family == AF_INET6 ) {
    return 6;
  }
  else {
    // reset (non-IP)
    ERROR_PLUS;
    bzero(this, sizeof(PortIp));
    this->addr.sa_family = AF_INET;
    return 4;
  }
}

void PortIp::get_ip(char* buffer, int space) noexcept
{
  if( buffer==nullptr || space<2 ) {
    ERROR_PLUS;
    return;
  }
  else if( space < IPSTR_SPACE ) {
    ERROR_PLUS;
    strcpy(buffer, "?");
    return;
  }
  if( is_ip4() ) {
    const char* ptr = ::inet_ntop(AF_INET, &this->addr4.sin_addr, buffer, IPSTR_SPACE-1);
    if( ptr == nullptr ) {
      // reset (invalid IPv4)
      ERROR_PLUS;
      this->addr4.sin_addr.s_addr = 0;
      strcpy(buffer, "0.0.0.0");
      return;
    }
  }
  else {
    const char* ptr = ::inet_ntop(AF_INET6, &this->addr6.sin6_addr, buffer, IPSTR_SPACE-1);
    if( ptr == nullptr ) {
      // reset (invalid IPv6)
      ERROR_PLUS;
      bzero(&this->addr6.sin6_addr, sizeof(this->addr6.sin6_addr));
      strcpy(buffer, "::");
      return;
    }
  }
  buffer[IPSTR_SPACE-1] = '\0';
}

string PortIp::ip() noexcept
{
  char ip[IPSTR_SPACE];
  this->get_ip(ip, IPSTR_SPACE);
  string result = ip;
  return result;
}

//----------------------------------------------------------------

int PortIp::set(const char* ip, int port, bool ip4only)
{
  bzero(this, sizeof(PortIp));
  this->addr.sa_family = AF_INET;
  int ver = set_ip(ip, ip4only);
  set_port(port);
  return ver;
}

int PortIp::set(const sockaddr& rawportip)
{
  bzero(this, sizeof(PortIp));
  if( rawportip.sa_family == AF_INET ) {
    memcpy(&this->addr, &rawportip, sizeof(sockaddr_in));
    return 4;
  }
  else if( rawportip.sa_family == AF_INET6 ) {
    memcpy(&this->addr, &rawportip, sizeof(sockaddr_in6));
    return 6;
  }
  else {
    // reset (non-IP)
    ERROR_PLUS;
    this->addr.sa_family = AF_INET;
    return 4;
  }
}

int PortIp::set4(Ip4be ip4be, int port)
{
  bzero(this, sizeof(PortIp));
  if( port < 0 || port > 65535 ) {
    port = 0;
  }
  this->addr4.sin_port = htons((uint16_t)port);
  return set_ip4(ip4be);
}

int PortIp::set6(const uint64_t* ip6be, int port)
{
  bzero(this, sizeof(PortIp));
  if( port < 0 || port > 65535 ) {
    port = 0;
  }
  this->addr6.sin6_port = htons((uint16_t)port);
  return set_ip6(ip6be);
}

//----------------------------------------------------------------

void PortIp::set_port(int port)
{
  // check port
  if( port < 0 || port > 65535 ) {
    char ip[IPSTR_SPACE];
    this->get_ip(ip, IPSTR_SPACE);
    throw Error("IP address %s with invalid port (%d)", ip, port);
  }
  this->addr4.sin_port = htons((uint16_t)port);
}

int PortIp::set_ip(const PortIp& source)
{
  if( source.addr.sa_family == AF_INET ) {
    return set_ip4(source.addr4.sin_addr);
  }
  else if( source.addr.sa_family == AF_INET6 ) {
    return set_ip6(source.addr6.sin6_addr);
  }
  else {
    // reset ip (non-IP)
    ERROR_PLUS;
    this->addr.sa_family = AF_INET;
    this->addr4.sin_addr.s_addr = 0;
    return 4;
  }
}

int PortIp::set_ip(const char* ip, bool ip4only)
{
  if( str_blank(ip) ) {
    ip = "0.0.0.0"; // default
  }

  // detect ip4/ip6
  int ver = get_ipv(ip);
  if( ip4only && ver!=4 ) {
    throw Error("Expected IPv4 address (%s)", ip);
  }

  // convert to raw
  if( ver == 4 ) {
    this->addr.sa_family = AF_INET;
    in_addr* ptr = &this->addr4.sin_addr;
    int result = inet_pton(AF_INET, ip, ptr);
    if( result != 1 ) {
      throw Error("Invalid IPv4 address: %s", ip);
    }
  }
  else {
    this->addr.sa_family = AF_INET6;
    in6_addr* raw_ptr = &this->addr6.sin6_addr;
    int result = inet_pton(AF_INET6, ip, raw_ptr);
    if( result != 1 ) {
      throw Error("Invalid IPv6 address: %s", ip);
    }
  }

  return ver;
}

int PortIp::set_ip4(Ip4be ip4be)
{
  this->addr.sa_family = AF_INET;
  this->addr4.sin_addr.s_addr = (in_addr_t)(ip4be);
  return 4;
}

int PortIp::set_ip6(const uint64_t* ip6be)
{
  this->addr.sa_family = AF_INET6;
  uint64_t* this6be = (uint64_t*)&this->addr6.sin6_addr;
  this6be[0] = ip6be[0];
  this6be[1] = ip6be[1];
  return 6;
}

//----------------------------------------------------------------

bool PortIp::operator==(PortIp& src) noexcept
{
  if( this->addr.sa_family != src.addr.sa_family ) {
    return false;
  }
  if( this->addr.sa_family == AF_INET && equals_ip(src.addr4.sin_addr) ) {
    return true;
  }
  if( this->addr.sa_family == AF_INET6 && equals_ip(src.addr6.sin6_addr) ) {
    return true;
  }
  return false;
}

//----------------------------------------------------------------

bool PortIp::equals_ip4(Ip4be ip4be) noexcept
{
  if( this->addr.sa_family != AF_INET ) {
    return false;
  }
  return( (Ip4be)this->addr4.sin_addr.s_addr == ip4be );
}

bool PortIp::equals_ip6(const uint64_t* ip6be) noexcept
{
  if( this->addr.sa_family != AF_INET6 ) {
    return false;
  }
  uint64_t* this6be = (uint64_t*)&this->addr6.sin6_addr;
  return ( this6be[0]==ip6be[0] && this6be[1]==ip6be[1] );
}

//================================================================
// IpSocket (Handle)
//----------------------------------------------------------------

void IpSocket::set(int handle)
{
  Handle::set(handle);
  if( handle >= 0 ) {
    // prepare new
    int64_t now = get_mono();
    this->last_sent = now;
    this->last_received = now;
    enable_non_blocking();
    if( this->want_sbuf_size != NULL_SIZE ) {
      set_sbuf_size(this->want_sbuf_size);
    }
    if( this->want_rbuf_size != NULL_SIZE ) {
      set_rbuf_size(this->want_rbuf_size);
    }
  }
  if( this->list != nullptr ) {
    // propagate to list
    IpSockets::set(this->list, this->pos, handle);
  }
}

bool IpSocket::unlist() noexcept
{
  if( this->list != nullptr ) {
    IpSockets::unlist(this->list, this->pos);
    return true;
  }
  else {
    return false;
  }
}

void IpSocket::close() noexcept
{
  if( this->list != nullptr ) {
    // propagate to list
    IpSockets::set(this->list, this->pos, NULL_HANDLE);
  }
  Handle::close();
}

//----------------------------------------------------------------

int IpSocket::get_local_port()
{
  if( handle < 0 ) {
    throw Error(this, "Socket is closed");
  }
  PortIp raw;
  socklen_t len = (socklen_t)sizeof(raw);
  int result = ::getsockname(this->handle, &raw.addr, &len);
  if( result < 0 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to get local IP port (%d)", code);
  }
  return raw.port();
}

//----------------------------------------------------------------

int IpSocket::get_sbuf_size()
{
  if( handle < 0 ) {
    throw Error(this, "Socket is closed");
  }
  int option = 0;
  socklen_t optlen = (socklen_t)sizeof(int);
  int result = getsockopt(this->handle, SOL_SOCKET, SO_SNDBUF, &option, &optlen);
  if( result != 0 ) {
    int code = errno;
    string address = this->address();
    throw Error(this, "Failed to get s-buffer size for %s (%d)", address.c_str(), code);
  }
  return option;
}

int IpSocket::get_rbuf_size()
{
  if( handle < 0 ) {
    throw Error(this, "Socket is closed");
  }
  int option = 0;
  socklen_t optlen = (socklen_t)sizeof(int);
  int result = getsockopt(this->handle, SOL_SOCKET, SO_RCVBUF, &option, &optlen);
  if( result != 0 ) {
    int code = errno;
    string address = this->address();
    throw Error(this, "Failed to get r-buffer size for %s (%d)", address.c_str(), code);
  }
  return option;
}

//----------------------------------------------------------------

bool IpSocket::wait(int wait_timeout)
{
  // closed?
  if( handle < 0 ) {
    throw Error(this, "Socket is closed");
  }

  // cap
  wait_timeout = std::max(wait_timeout, 1*MILLI);
  wait_timeout = std::min(wait_timeout, 1*SECOND);

  // poll
  struct pollfd pfd;
  pfd.fd = this->handle;
  pfd.events = POLLIN | POLLPRI;
  pfd.revents = 0;
  int result = ::poll(&pfd, 1, wait_timeout);
  if( result < 0 ) {
    int code = errno;
    if( code == EINTR ) {
      // interrupted
      short_sleep(1);
      return false;
    }
    throw Error(this, "Failed to poll IP socket %d (%d)", this->handle, code);
  }
  return ( result > 0 );
}

//----------------------------------------------------------------

bool IpSocket::wait_to_send(int wait_timeout)
{
  if( handle < 0 ) {
    throw Error(this, "Socket is closed");
  }

  // cut off
  if( wait_timeout < 0 ) {
    wait_timeout = 0;
  }
  else if( wait_timeout > SECOND ) {
    wait_timeout = SECOND;
  }

  // poll
  struct pollfd pfd;
  pfd.fd = this->handle;
  pfd.events = POLLOUT;
  pfd.revents = 0;
  int result = ::poll(&pfd, 1, wait_timeout);
  if( result < 0 ) {
    int code = errno;
    throw Error(this, "Failed to poll sending IP socket %d (%d)", this->handle, code);
  }
  return ( result > 0 );
}

//----------------------------------------------------------------

void IpSocket::enable_non_blocking()
{
  if( this->handle < 0 ) {
    return; // silent return
  }

  // get flags
  int flags = ::fcntl(this->handle, F_GETFL);
  if( flags == -1 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to get IP socket flags (%d)", code);
  }

  // enable non-blocking flag
  flags |= O_NONBLOCK;
  int result = ::fcntl(this->handle, F_SETFL, flags);
  if( result < 0 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to enable IP non-blocking I/O (%d)", code);
  }
}

void IpSocket::set_tcp_keep_alive(int sec)
{
  if( this->handle < 0 ) {
    return; // silent return
  }

  int result;

  int enable = 0;
  if( sec > 0 ) {
    enable = 1;
  }

  if( enable )
  {
    // idle time [sec] before sending first probe
    int idle = sec;
    result = ::setsockopt(handle, IPPROTO_TCP, TCP_KEEPIDLE, &idle, sizeof(int));
    if( result < 0 ) {
      close();
      throw Error(this, "Failed to set TCP_KEEPIDLE (%d)", errno);
    }

    // interval [sec] between probes
    int interval;
    if( sec <= 10 ) { interval = 1; }
    else if( sec <= 30 ) { interval = 2; }
    else if( sec <= 60 ) { interval = 3; }
    else { interval = 5; }
    result = ::setsockopt(handle, IPPROTO_TCP, TCP_KEEPINTVL, &interval, sizeof(int));
    if( result < 0 ) {
      close();
      throw Error(this, "Failed to set TCP_KEEPINTVL (%d)", errno);
    }

    // number of probes [count] before dropping connection
    int count = 3;
    result = ::setsockopt(handle, IPPROTO_TCP, TCP_KEEPCNT, &count, sizeof(int));
    if( result < 0 ) {
      close();
      throw Error(this, "Failed to set TCP_KEEPCNT (%d)", errno);
    }
  }

  // enable/disable
  result = ::setsockopt(handle, SOL_SOCKET, SO_KEEPALIVE, &enable, sizeof(int));
  if( result < 0 ) {
    close();
    throw Error(this, "Failed to set SO_KEEPALIVE (%d)", errno);
  }
}

//----------------------------------------------------------------

void IpSocket::set_sbuf_size(int bytes)
{
  if( bytes<=0 || this->handle<0 ) {
    return; // silent return
  }
  int option = bytes;
  socklen_t optlen = (socklen_t)sizeof(int);
  int result = setsockopt(this->handle, SOL_SOCKET, SO_SNDBUF, &option, optlen);
  if( result != 0 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to set IP s-buffer size (%d)", code);
  }
}

void IpSocket::set_rbuf_size(int bytes)
{
  if( bytes<=0 || this->handle<0 ) {
    return; // silent return
  }
  int option = bytes;
  socklen_t optlen = (socklen_t)sizeof(int);
  int result = setsockopt(this->handle, SOL_SOCKET, SO_RCVBUF, &option, optlen);
  if( result != 0 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to set IP r-buffer size (%d)", code);
  }
}

//----------------------------------------------------------------

string IpSocket::address() noexcept
{
  if( this->remote.is_blank() ) {
    return this->local.address();
  }
  else {
    return this->remote.address();
  }
}

//================================================================
// IpSockets (container)
//----------------------------------------------------------------

IpSockets::~IpSockets() noexcept
{
  for(auto ipsocket : this->sockets) {
    if( ipsocket->list == this ) {
      ipsocket->list = nullptr;
      ipsocket->pos = NULL_POS;
    }
  }
}

//----------------------------------------------------------------

bool IpSockets::add(IpSocket* ipsocket)
{
  // check
  if( ipsocket == nullptr ) {
    throw Error("No socket (IpSockets)");
  }

  // unlist first?
  int pos = ipsocket->pos;
  if( pos >= 0 ) {
    if( ipsocket->list == nullptr ) {
      ERROR_PLUS;
      pos = NULL_POS;
    }
    else if( ipsocket->list != this ) {
      IpSockets::unlist(ipsocket->list, pos);
      pos = NULL_POS;
    }
  }

  // reuse?
  if( pos >= 0 ) {
    pollfd* poller = this->pfds.data() + pos;
    poller->fd = ipsocket->handle;
    if( ipsocket->handle < 0 ) {
      poller->events = 0;
    }
    else {
      poller->events = POLLIN | POLLPRI;
    }
    poller->revents = 0;
    // reused
    return false;
  }

  // add socket last
  this->sockets.add(ipsocket);

  // add pfd last
  pollfd pfd;
  pfd.fd = ipsocket->handle;
  if( ipsocket->handle < 0 ) {
    pfd.events = 0;
  }
  else {
    pfd.events = POLLIN | POLLPRI;
  }
  pfd.revents = 0;
  pos = this->pfds.size();
  this->pfds.add(pfd);

  // link
  ipsocket->list = this;
  ipsocket->pos = pos;

  // added
  return true;
}

//----------------------------------------------------------------

bool IpSockets::prune()
{
  bool removed = false;
  int pos = 0;
  int size = this->sockets.size();
  while( pos < size ) {
    IpSocket* ipsocket = this->sockets[pos];
    if( ipsocket->is_closed() ) {
      // remove
      IpSockets::unlist(this, pos);
      ipsocket->list = nullptr;
      ipsocket->pos = NULL_POS;
      size = this->sockets.size();
      removed = true;
    }
    else {
      // keep
      ipsocket->pos = pos;
      pos++;
    }
  }
  return removed;
}

//----------------------------------------------------------------

bool IpSockets::wait(int wait_timeout)
{
  // cap
  wait_timeout = std::max(wait_timeout, 1*MILLI);
  wait_timeout = std::min(wait_timeout, 1*SECOND);

  // poll
  pollfd* array = pfds.data();
  int result = ::poll(array, pfds.size(), wait_timeout);
  if( result < 0 ) {
    int code = errno;
    if( code == EINTR ) {
      // interrupted
      short_sleep(1);
      return false;
    }
    throw Error("Failed to poll IP sockets (%d)", code);
  }
  return ( result > 0 );
}

//----------------------------------------------------------------

void IpSockets::set(IpSockets* list, int pos, int handle)
{
  pollfd* poller = list->pfds.data() + pos;
  poller->fd = handle;
  if( handle < 0 ) {
    poller->events = 0;
  }
  else {
    poller->events = POLLIN | POLLPRI;
  }
  poller->revents = 0;
}

void IpSockets::unlist(IpSockets* list, int pos) noexcept
{
  list->sockets.remove(pos);
  list->pfds.remove(pos);
}

//----------------------------------------------------------------

//================================================================
// UdpSocket (IpSocket)
//----------------------------------------------------------------

void UdpSocket::init(bool reuse)
{
  close();

  int remote_ipv = this->remote.ipv();
  int local_ipv = this->local.ipv();

  int ipv = remote_ipv;
  if( remote_ipv != local_ipv ) {
    // try up-convert to IPv6
    if( remote_ipv==4 && this->remote.is_blank_ip() ) {
      this->remote.set("::", this->remote.port());
    }
    else if( local_ipv==4 && this->local.is_blank_ip() ) {
      this->local.set("::", this->local.port());
    }
    else {
      throw Error(this, "UDP socket with mixed local/remote IP type (IPv4/IPv6)");
    }
    ipv = 6;
  }

  // allocate socket
  int handle;
  if( ipv == 4 ) {
    handle = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  }
  else {
    handle = ::socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
  }
  if( handle < 0 ) {
    int code = errno;
    string address = this->address();
    throw Error(this, "Failed to allocate UDP socket for %s (%d)", address.c_str(), code);
  }
  set(handle);

  int result;
  if( this->local.port() == 0 ) {
    // randomized port
    if( !this->local.is_blank_ip() ) {
      string ip = this->local.ip();
      throw Error(this, "UDP socket can not bind to %s (local port is 0)", ip.c_str());
    }
  }
  else {
    // fixed port (and ip)
    if( reuse ) {
      int enable = 1;
      result = ::setsockopt(this->handle, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
      if( result < 0 ) {
        int code = errno;
        close();
        throw Error(this, "Failed to enable REUSEADDR on UDP socket (%d)", code);
      }
    }
    result = ::bind(this->handle, &this->local.addr, sizeof(this->local));
    if( result < 0 ) {
      int code = errno;
      close();
      string address = this->address();
      throw Error(this, "Failed to bind UDP socket to %s (%d)", address.c_str(), code);
    }
  }
}

void UdpSocket::init(const char* remote_ip, int remote_port, const char* local_ip, int local_port, bool reuse, bool ip4only)
{
  // establish address pair
  close();
  this->local.set(local_ip, local_port, ip4only);
  this->remote.set(remote_ip, remote_port, ip4only);

  // raw address init
  init(reuse);
}

//----------------------------------------------------------------

void UdpSocket::open_bcast(const char* bcast_ip4, int remote_port)
{
  init(bcast_ip4, remote_port, nullptr, 0, false, true);

  // allow sending broadcast
  int enable = 1;
  int result = ::setsockopt(this->handle, SOL_SOCKET, SO_BROADCAST, &enable, sizeof(int));
  if( result < 0 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to enable broadcast (%d)", code);
  }
}

//----------------------------------------------------------------

void UdpSocket::open_mcast(const char* group_ip, int remote_port, const char* local_ip, bool loop, int ttl)
{
  open(group_ip, remote_port);
  this->local.set(local_ip, 0);

  if( this->remote.addr.sa_family != this->local.addr.sa_family ) {
    close();
    throw Error(this, "Mixed mulicast IPv4/IPv6 group/local address");
  }

  int result;

  // loopback
  uint8_t loop8 = (uint8_t)loop;
  result = ::setsockopt(this->handle, IPPROTO_IP, IP_MULTICAST_LOOP, &loop8, sizeof(loop8));
  if( result < 0 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to set multicast loopback (%d)", code);
  }

  // time to live (localhost=0, localnet=1, ...)
  uint8_t ttl8 = (uint8_t)ttl;
  result = ::setsockopt(this->handle, IPPROTO_IP, IP_MULTICAST_TTL, &ttl8, sizeof(ttl8));
  if( result < 0 ) {
    int code = errno;
    close();
    throw Error(this, "Failed to set multicast TTL (%d)", code);
  }

  // explicit or default nic?
  if( !this->local.is_blank_ip() ) {
    // explicit nic
    if( this->local.is_ip4() ) {
      in_addr* ptr = &this->local.addr4.sin_addr;
      result = ::setsockopt(this->handle, IPPROTO_IP, IP_MULTICAST_IF, ptr, sizeof(in_addr));
    }
    else {
      in6_addr* ptr = &this->local.addr6.sin6_addr;
      result = ::setsockopt(this->handle, IPPROTO_IP, IP_MULTICAST_IF, ptr, sizeof(in6_addr));
    }
    if( result < 0 ) {
      int code = errno;
      close();
      string ip = this->local.ip();
      throw Error(this, "Failed to set multicast local IP to %s (%d)", ip.c_str(), code);
    }
  }
}

void UdpSocket::bind_mcast(const char* group_ip, int local_port, const char* local_ip, bool reuse)
{
  // check group ip
  int ver = get_ipv(group_ip);

  // bind to port (ignore local_ip here)
  if( ver == 4 ) {
    bind(local_port, "0.0.0.0", reuse);
  }
  else {
    bind(local_port, "::", reuse);
  }

  this->remote.set(group_ip, 0);
  this->local.set(local_ip, local_port);

  if( this->remote.addr.sa_family != this->local.addr.sa_family ) {
    close();
    throw Error(this, "Mixed mulicast IPv4/IPv6 group/local address");
  }

  // join multicast group
  int result;
  if( ver == 4 ) {
    // TODO: MCAST_JOIN_GROUP (IPv4)
    ip_mreq mreq;
    bzero(&mreq, sizeof(struct ip_mreq));
    mreq.imr_multiaddr.s_addr = this->remote.addr4.sin_addr.s_addr;
    mreq.imr_interface.s_addr = this->local.addr4.sin_addr.s_addr;
    result = ::setsockopt(this->handle, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
  }
  else {
    // TODO: MCAST_JOIN_GROUP (IPv6)
    throw Error(this, "Not implemented");
  }
  if( result < 0 ) {
    int code = errno;
    close();
    string ip = remote.ip();
    throw Error(this, "Failed to join multicast group %s (%d)", ip.c_str(), code);
  }
}

//----------------------------------------------------------------

bool UdpSocket::try_send(const char* data, int bytes, PortIp& sendto, int wait_timeout)
{
  if( this->handle < 0 ) {
    throw Error(this, "UDP socket is closed");
  }

  if( wait_timeout > 0 ) {
    wait_to_send(wait_timeout);
  }

  // check
  if( this->handle < 0 ) {
    throw Error(this, "UDP socket is closed");
  }
  if( bytes > this->payload_max ) {
    string address = sendto.address();
    throw Error(this, "UDP payload too large (sending to %s)", address.c_str());
  }

  // sendto
  int sent = (int)::sendto(this->handle, data, bytes, 0, &sendto.addr, sizeof(sendto));
  if( sent < 0 ) {
    int code = errno;
    if( code==EAGAIN || code==EWOULDBLOCK ) {
      // send buffer is full (push back)
      return false;
    }
    else {
      // error
      close();
      string address = sendto.address();
      throw Error(this, "Failed to send UDP to %s (%d)", address.c_str(), code);
    }
  }
  if( sent != bytes ) {
    // unexpected error
    close();
    string address = sendto.address();
    throw Error(this, "Failed to send UDP to %s", address.c_str());
  }

  // packet sent (accepted for sending)
  this->last_sent = get_mono();
  this->bytes_sent += sent;
  return true;
}

//----------------------------------------------------------------

void UdpSocket::send(const char* data, int bytes, PortIp& sendto, int stall_timeout)
{
  // cap
  stall_timeout = std::max(stall_timeout, 100*MILLI);
  stall_timeout = std::min(stall_timeout, 1*MINUTE);

  // try send until accepted/stopped/stall/error
  int64_t start = get_mono();
  while( !try_send(data,bytes,sendto,50*MILLI) ) {
    // push back
    // this may not happen with UDP because the kernel has the right
    // to discard datagrams when the send buffer is full (!)
    // consider "rate limiting" to avoid packet loss
    app_check();
    int64_t elapsed = get_duration(start);
    if( elapsed > stall_timeout ) {
      // stalled
      close();
      string address = sendto.address();
      throw Error(this, "Stalled while sending UDP to %s", address.c_str());
    }
  }

  // packet sent (accepted for sending)
}

//----------------------------------------------------------------

bool UdpSocket::try_receive(Buffer* buffer, PortIp* remote)
{
  if( this->list != nullptr ) {
    if( !IpSockets::check(this->list,this->pos) ) {
      return false; // quick return
    }
  }

  assert( buffer != nullptr );

  if( this->handle < 0 ) {
    throw Error(this, "UDP socket is closed");
  }

  int space = buffer->space;
  if( space > UDP_PAYLOAD_MAX ) {
    space = UDP_PAYLOAD_MAX;
  }
  if( space <= 0 ) {
    throw Error(this, "UDP receive buffer with no space");
  }

  // recvfrom
  sockaddr* ptr = nullptr;
  socklen_t len = 0;
  if( remote != nullptr ) {
    remote->addr.sa_family = 0;
    ptr = &remote->addr;
    len = (socklen_t)sizeof(PortIp);
  }
  int bytes = (int)::recvfrom(this->handle, buffer->data, space, 0, ptr, &len);
  if( bytes < 0 ) {
    buffer->clear();
    int code = errno;
    if( code==EAGAIN ) {
      // nothing
      return false;
    }
    if( code==EWOULDBLOCK ) {
      // no complete packet available
      return false;
    }
    // error
    int port = get_local_port();
    throw Error(this, "Failed to receive UDP at port %d (%d)", port, code);
  }
  buffer->set(bytes);

  // received packet
  this->last_received = get_mono();
  this->bytes_received += bytes;
  return true;
}

//================================================================
// TcpSocket (IpSocket)
//----------------------------------------------------------------

void TcpSocket::init(const char* remote_ip, int port, int keep_alive_sec)
{
  close();

  // check remote port
  if( port == 0 ) {
    throw Error(this, "TCP stream with illegal port 0");
  }

  // check remote ip
  int ver = this->remote.set(remote_ip, port);

  // allocate socket
  int handle;
  if( ver == 4 ) {
    handle = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  }
  else {
    handle = ::socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
  }
  if( handle < 0 ) {
    int code = errno;
    string address = this->remote.address();
    throw Error(this, "Failed to allocate TCP socket to %s (%d)", address.c_str(), code);
  }
  set(handle, keep_alive_sec);
}

//----------------------------------------------------------------

bool TcpSocket::try_connect()
{
  if( this->handle < 0 ) {
    throw Error(this, "TCP socket is closed");
  }
  // TODO: research a better way to do non-blocking connect (getpeername?)
  int result = ::connect(this->handle, &this->remote.addr, sizeof(PortIp));
  int64_t now = get_mono();
  if( result >= 0 ) {
    // connected!
    this->last_sent = now;
    this->last_received = now;
    return true;
  }
  // check errno
  int code = errno;
  if( code==EISCONN ) {
    // connected!
    this->last_sent = now;
    this->last_received = now;
    return true;
  }
  else if( code==EAGAIN || code==EWOULDBLOCK || code==EINPROGRESS || code==EALREADY ) {
    // not yet
    return false;
  }
  else {
    // error
    close();
    string address = this->remote.address();
    throw Error(this, "Failed to connect to %s (%d)", address.c_str(), code);
  }
}

void TcpSocket::connect(int stall_timeout)
{
  // cap
  stall_timeout = std::max(stall_timeout, 100*MILLI);
  stall_timeout = std::min(stall_timeout, 1*MINUTE);

  // try connect until success/stop/stall/error
  int64_t start = get_mono();
  while( !try_connect() ) {
    // get some sleep and try again
    wait(50*MILLI); // TODO: verify connect produces input event
    app_check();
    int64_t duration = get_duration(start);
    if( duration > stall_timeout ) {
      // give up
      close();
      string address = this->remote.address();
      throw Error(this, "Could not connect to %s", address.c_str());
    }
  }
}

//----------------------------------------------------------------

int TcpSocket::try_send(const char* data, int allbytes, int wait_timeout)
{
  if( this->handle < 0 ) {
    throw Error(this, "No TCP connection");
  }

  if( wait_timeout > 0 ) {
    wait_to_send(wait_timeout);
  }

  // chunk cut off
  int trybytes = std::min(allbytes,this->chunk_max);

  // try send (non-blocking)
  if( trybytes <= 0 ) {
    // skip calling send here (send returning 0 is reserved for disconnect)
    return 0;
  }
  int sent = (int)::send(this->handle, data, trybytes, MSG_DONTWAIT|MSG_NOSIGNAL);
  if( sent == 0 ) {
    // DISCONNECTED!
    close();
    return -1;
    //string address = this->remote.address();
    //throw DisconnectException(this, "Disconnected from %s", address.c_str());
  }
  if( sent < 0 ) {
    int code = errno;
    if( code==EAGAIN || code==EWOULDBLOCK ) {
      // send buffer is full (push back)
      return 0;
    }
    else {
      // error
      close();
      string address = this->remote.address();
      throw Error(this, "Lost connection to %s (%d)", address.c_str(), code);
    }
  }
  // return bytes sent
  this->last_sent = get_mono();
  this->bytes_sent += sent;
  return sent;
}

void TcpSocket::send(const char* data, int bytes, int stall_timeout)
{
  if( bytes <= 0 ) {
    return; // quick return
  }

  // cap
  stall_timeout = std::max(stall_timeout, 100*MILLI);
  stall_timeout = std::min(stall_timeout, 1*MINUTE);

  // try send until all data accepted or stall or error
  int64_t start = get_mono();
  for(;;) {
    int sent = try_send(data, bytes, 50*MILLI);
    if( sent < 0 ) {
      string address = this->remote.address();
      throw Error(this, "Disconnected while sending to %s", address.c_str());
    }
    if( sent > 0 ) {
      // some or all data accepted
      data += sent;
      this->last_sent = get_mono();
      this->bytes_sent += sent;
      // restart stall check
      start = this->last_sent;
    }
    else {
      // nothing accepted this time
      int64_t duration = get_duration(start);
      if( duration > stall_timeout ) {
        // give up
        close();
        string address = this->remote.address();
        throw Error(this, "Stalled while sending TCP to %s", address.c_str());
      }
    }
    if( sent >= bytes ) {
      break; // sent all!
    }
    if( sent <= KIB ) {
      // just a small chunk accepted, so we can sleep a bit
      short_sleep(MILLI);
    }
    app_check();
  }
}

//----------------------------------------------------------------

int TcpSocket::try_receive()
{
  if( this->list != nullptr ) {
    if( !IpSockets::check(this->list,this->pos) ) {
      return false; // quick return
    }
  }

  if( this->handle < 0 ) {
    throw Error(this, "No TCP connection");
  }

  // cap
  int chunk = this->buffer.vacant();
  if( chunk > this->chunk_max ) {
    chunk = this->chunk_max;
  }

  // expand?
  if( chunk < chunk_max/4 ) {
    int expand = this->buffer.space + chunk_max/4;
    if( expand > this->buffer.limit ) {
      expand = this->buffer.limit;
    }
    this->buffer.reserve(expand);
    chunk = this->buffer.vacant();
    if( chunk > chunk_max ) {
      chunk = chunk_max;
    }
  }

  // check
  if( chunk <= 0 ) {
    throw Error(this, "TCP receive buffer with no space");
  }

  // receive chunk (non-blocking)
  char* end = this->buffer.end();
  int bytes = (int)::recv(this->handle, end, chunk, MSG_DONTWAIT);
  if( bytes == 0 ) {
    // DISCONNECTED!
    *end = '\0';
    close();
    return -1;
    //string address = this->remote.address();
    //throw DisconnectException(this, "Disconnected from %s", address.c_str());
  }
  if( bytes < 0 ) {
    *end = '\0';
    int code = errno;
    if( code==EAGAIN || code==EWOULDBLOCK ) {
      // no data
      return 0;
    }
    else {
      // error
      close();
      string address = this->remote.address();
      throw Error(this, "Lost connection to %s (%d)", address.c_str(), code);
    }
  }

  // data received
  this->last_received = get_mono();
  this->bytes_received += bytes;
  this->buffer.add(bytes);
  return bytes;
}

//================================================================
// TcpService (IpSocket)
//----------------------------------------------------------------

void TcpService::bind(int port, const char* local_ip, int queue_max, int keep_alive_sec)
{
  close();

  if( keep_alive_sec >= 0  ) {
    this->keep_alive_sec = keep_alive_sec;
  }

  // check port
  if( port == 0 ) {
    throw Error(this, "TCP service with illegal port 0");
  }

  local.set(local_ip, port);

  // allocate socket
  int handle;
  if( this->local.is_ip4() ) {
    handle = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  }
  else {
    handle = ::socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
  }
  if( handle < 0 ) {
    int code = errno;
    string address = this->local.address();
    throw Error(this, "Failed to allocate TCP socket for %s (%d)", address.c_str(), code);
  }
  set(handle);

  int result;

  result = ::bind(this->handle, &this->local.addr, sizeof(PortIp));
  if( result < 0 ) {
    int code = errno;
    close();
    string address = this->local.address();
    throw Error(this, "Failed to bind TCP service to %s (%d)", address.c_str(), code);
  }

  result = ::listen(this->handle, queue_max);
  if( result < 0 ) {
    int code = errno;
    close();
    string address = this->local.address();
    throw Error(this, "Failed to enable TCP service at %s (%d)", address.c_str(), code);
  }
}

//----------------------------------------------------------------

bool TcpService::try_accept(TcpSocket* callee)
{
  assert( callee != nullptr );
  if( this->handle < 0 ) {
    throw Error(this, "TCP service is closed");
  }

  callee->close();

  PortIp remote;
  socklen_t addr_space = (socklen_t)sizeof(remote);
  int callee_handle = ::accept(this->handle, &remote.addr, &addr_space);
  if ( callee_handle < 0 ) {
    // nothing or error
    return false;
  }

  callee->set(callee_handle, this->keep_alive_sec);
  callee->remote = remote;
  callee->local = this->local;

  // new connection
  return true;
}

//================================================================
