#include "dnsbar.h"
#include "dnsbar-module.h"
#include "repo.h"

/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

int main(int argc, char* argv[])
{
  if( argc < 1 ) {
    log_error("Missing initial argument");
    exit(2);
  }
  const char* arg = BLANK;
  if( argc == 2 ) {
    arg = argv[1];
  }
  if( argc>2 || str_equal(arg,"-h") || str_equal(arg,"--help") ) {
    notef("Usage: %s [<confdir>]", argv[0]);
    exit(0);
  }
  if( str_equal(arg,"-v") || str_equal(arg,"--version") ) {
    notef("dnsbar %s %d %s by %s", MODULE_VERSION, REPO_DATE, REPO_HASH, MODULE_OWNERS);
    exit(0);
  }
  string exe = abs_path(argv[0]);
  while( APP_EXIT == APP_NO_EXIT ) {
    app_start();
    try {
      notef("dnsbar %s %d %s", MODULE_VERSION, REPO_DATE, REPO_HASH);
      DnsBar app;
      app.run(exe.c_str(), arg);
    }
    catch(const StopException& stop) {
      log_note(stop.what());
    }
    catch(const Exception& e) {
      app_req_exit(APP_ERROR_EXIT);
      log_error(e);
    }
    catch(const std::exception& e) {
      app_req_exit(APP_ERROR_EXIT);
      log_error(e);
    }
    catch(...) {
      app_req_exit(APP_ERROR_EXIT);
      log_error("Exception!");
    }
    app_end();
    if( APP_EXIT!=APP_OK_EXIT && APP_INTERNAL_RESTART ) {
      if( APP_EXIT == APP_RESTART_EXIT ) {
        log_note("Internal restart...");
      }
      else {
        log_note("Internal error restart...");
        short_sleep(1*SECOND, true);
      }
      APP_EXIT = APP_NO_EXIT;
      short_sleep(1*SECOND, true);
      continue;
    }
    // exit
    break;
  }
  notef("exit(%d)", APP_EXIT);
  short_sleep(50*MILLI, true);
  return APP_EXIT;
}

//----------------------------------------------------------------
