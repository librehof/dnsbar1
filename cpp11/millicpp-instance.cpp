#include "millicpp.h"

/*
 * millicpp * Apache 2.0 (C) 2023 librehof.org
 */

//================================================================
// Instance
//----------------------------------------------------------------

Instance::~Instance() noexcept
{
  RESOURCE_MINUS;
}

string Instance::get_identifier() noexcept
{
  return stringf("Instance(0x%08llx)", (LLU)this);
}

int Instance::get_refs() noexcept
{
  return this->refs;
}

void Instance::enref()
{
  this->refs++;
}

void Instance::deref() noexcept
{
  // never fail
  int refs = --(this->refs);
  if( refs < 0 ) {
    ERROR_PLUS;
  }
  else if( refs == 0 ) {
    delete this; // self destruct!
  }
}

//================================================================
// Exceptions
//----------------------------------------------------------------

void Exception::init(const char* reason) noexcept
{
  if( str_blank(reason) ) {
    reason = "Exception";
  }
  this->message = reason;
}

void Exception::init(const char* format, va_list args) noexcept
{
  if( str_blank(format) ) {
    format = "Exception";
  }
  this->message = stringf(format, args);
}

void Exception::init(IInstance* source, const char* reason) noexcept
{
  if( source != nullptr ) {
    this->source = source->get_identifier();
  }
  if( str_blank(reason) ) {
    this->message = "Exception";
  }
  else {
    this->message = reason;
  }
}

void Exception::init(IInstance* source, const char* format, va_list args) noexcept
{
  if( source != nullptr ) {
    this->source = source->get_identifier();
  }
  if( str_blank(format) ) {
    this->message = "Exception";
  }
  else {
    this->message = stringf(format, args);
  }
}

Exception::~Exception() noexcept
{
}

const char* Exception::what() const noexcept
{
  return this->message.c_str();
}

//================================================================
// Buffer (Instance)
//----------------------------------------------------------------

int block_space_up(int sizenew, int limit)
{
  // initial
  assert( sizenew >= 0 );
  assert( limit >= 0 );
  if( limit > BLOCK_MAX ) {
    limit = BLOCK_MAX;
  }
  if( sizenew > limit ) {
    throw Error("Size out of limit");
  }
  int target = sizenew + (sizenew >> 2);
  if( target > limit ) {
    target = limit;
  }
  int spacenew = 64; // minimum
  if( target >= 0x00010000 ) {
    spacenew = 0x00010000;
  }
  // iterate
  while( spacenew < target ) {
    spacenew <<= 1;
  }
  return spacenew;
}

void* block_insert(void* endptr, void* subptr, int bytes)
{
  int tail = ptr_sub(endptr,subptr);
  if( tail < 0 ) {
    throw Error("Out of bound (insert)");
  }
  return memmove(ptr_add(subptr,bytes), subptr, tail);
}

void block_remove(void* endptr, void* subptr, int bytes)
{
  int tail = ptr_sub(endptr,subptr) - bytes;
  if( tail < 0 ) {
    throw Error("Out of bound (cut)");
  }
  if( tail != 0 ) {
    memmove(subptr, ptr_add(subptr,bytes), tail);
  }
}

//----------------------------------------------------------------

Buffer::Buffer()
{
  this->size = 0;
  this->data = BLANK;
  this->space = 0;
  this->limit = BLOCK_LIMIT;
}

Buffer::Buffer(int space, int limit)
{
  this->size = 0;
  this->data = BLANK;
  this->space = 0;
  if( limit < 0 ) {
    this->limit = space;
  }
  else {
    this->limit = limit;
  }
  optimize(space);
}

Buffer::~Buffer() noexcept
{
  if( this->space != 0 ) {
    free(this->data);
    RESOURCE_MINUS;
  }
}

//----------------------------------------------------------------

int Buffer::optimize(int spacemin)
{
  assert( this->limit >= 0 );
  assert( this->limit <= BLOCK_MAX );
  assert( spacemin >= 0 );
  assert( this->size <= this->space );
  if( spacemin < this->size ) {
    spacemin = this->size; // slim fit
  }
  if( spacemin > this->limit ) {
    throw Error(this, "Buffer space out of limit");
  }
  if( this->space == 0 ) {
    // is empty
    if( spacemin == 0 ) {
      // keep empty
      this->data = BLANK;
    }
    else {
      // malloc
      this->data = (char*)::malloc(spacemin+4);
      if( this->data == nullptr ) {
        // usually we get a pointer here even if oom, then
        // killed on first access if selected by "oom-killer"
        throw Error("Out of memory (malloc)");
      }
      this->data[0] = '\0'; // oom-kill may happen here
      RESOURCE_PLUS;
    }
  }
  else {
    // is malloced
    if( spacemin == 0 ) {
      // free+empty
      free(this->data);
      RESOURCE_MINUS;
      this->data = BLANK;
    }
    else if( this->size == 0 ) {
      // free+malloc
      free(this->data);
      RESOURCE_MINUS;
      this->data = (char*)::malloc(spacemin+4);
      if( this->data == nullptr ) {
        // usually we get a pointer here even if oom, then
        // killed on first access if selected by "oom-killer"
        this->space = 0;
        throw Error("Out of memory (malloc)");
      }
      this->data[0] = '\0'; // oom-kill may happen here
      RESOURCE_PLUS;
    }
    else {
      // realloc
      this->data = (char*)::realloc(this->data, spacemin+4);
      if( this->data == nullptr ) {
        // usually we get a pointer here even if oom, then
        // killed on first access if selected by "oom-killer"
        this->space = 0;
        throw Error("Out of memory (realloc)");
      }
    }
  }
  return (this->space = spacemin);
}

//----------------------------------------------------------------

char* Buffer::set(int bytes)
{
  assert( bytes >= 0 );
  if( bytes > this->space ) {
    optimize(bytes);
  }
  this->size = bytes;
  this->data[bytes] = '\0';
  return this->data;
}

char* Buffer::setf(const char* format, ...)
{
  // try I
  clear();
  va_list args;
  va_start(args, format);
  int virtsize = addf(this, format, args);
  va_end(args);
  if( virtsize > 0 ) {
    // try II
    optimize(virtsize+4);
    va_start(args, format);
    int virtsize = addf(this, format, args);
    va_end(args);
    if( virtsize > 0 ) {
      throw Error(this, "Formatter double fault (addf)");
    }
  }
  return this->data;
}

//----------------------------------------------------------------

char* Buffer::add(int bytes)
{
  assert( bytes >= 0 );
  int before = this->size;
  int after = before + bytes;
  if( after > this->space ) {
    int space = block_space_up(after, this->limit);
    optimize(space);
  }
  this->size = after;
  char* area = this->data + before;
  area[bytes] = '\0';
  return area;
}

void Buffer::addf(const char* format, ...)
{
  // try I
  va_list args;
  va_start(args, format);
  int virtsize = addf(this, format, args);
  va_end(args);
  if( virtsize > 0 ) {
    // try II
    int space = block_space_up(this->size+virtsize+4, this->limit);
    optimize(space);
    va_start(args, format);
    int virtsize = addf(this, format, args);
    va_end(args);
    if( virtsize > 0 ) {
      throw Error(this, "Formatter double fault (addf)");
    }
  }
}

void Buffer::addfn(const char* format, ...)
{
  // try I
  va_list args;
  va_start(args, format);
  int virtsize = addf(this, format, args);
  va_end(args);
  if( virtsize > 0 ) {
    // try II
    int space = block_space_up(this->size+virtsize+4, this->limit);
    optimize(space);
    va_start(args, format);
    int virtsize = addf(this, format, args);
    va_end(args);
    if( virtsize > 0 ) {
      throw Error(this, "Formatter double fault (addfn)");
    }
  }
  *add(1) = '\n';
}

int Buffer::addf(Buffer* buffer, const char* format, va_list args)
{
  // try I
  int space = buffer->space;
  int vacant = space - buffer->size;
  int virtsize = vsnprintf(buffer->end(), vacant+1, format, args);
  if( virtsize < 0 ) {
    throw Error(buffer, "Formatter failed (addf)");
  }
  if( virtsize > vacant ) {
    return virtsize;
  }
  else {
    buffer->size += virtsize;
    return 0;
  }
}

//----------------------------------------------------------------

char* Buffer::insert(int pos, int bytes)
{
  assert( pos >= 0 );
  add(bytes);
  return (char*)block_insert(this->data+this->size, this->data+pos, bytes);
}

void Buffer::remove(int pos, int bytes)
{
  assert( pos >= 0 );
  assert( bytes >= 0 );
  block_remove(this->data+this->size, this->data+pos, bytes);
  this->size -= bytes;
  this->data[this->size] = '\0';
}

//================================================================
