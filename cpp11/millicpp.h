#ifndef MILLICPP_H
#define MILLICPP_H

/*
 * millicpp | Copyright 2023 librehof.org
 *
 * License: Apache 2.0
 *
 * Dependencies:
 *
 *  GNU/Linux
 *  C++ >= 11 (using C++ 11 rvalue move, emplace, ...)
 *  sizeof(char) == 1
 *  sizeof(int) == 4
 *  sizeof(time_t) == 8
 *  sizeof(ptr) == 4 or 8
 *
 * Defines:
 *
 *  NDEBUG : disables asserts, resource counter and more
 *
 * Build:
 *
 *  g++ [-g] [-pthread] -std=c++11 -o <exe> cpp11/...
 *
 * Preferred simple datatypes:
 *
 *  bool    : true|false
 *  int     : 32-bit integer (up to some millions)
 *  int64_t : 64-bit integer
 *  float   : up to 5-6 digits float
 *  double  : ~15 digits float
 *  string  : text (utf8 unless otherwise stated)
 *
 * Time:
 *
 *  64-bit millisecond time (unless otherwise stated)
 *
 * Access semantics:
 *
 *  instance variables are generally not "protected",
 *  access can be marked as:
 *
 *  - pubrd : direct read ptr/content
 *  - pubrw : direct read/write
 *
 * Thread-safety:
 *
 *  global counters are thread-safe
 *  otherwise thread-safety must be explicitly added
 *  use c++11 std::thread (and Task/Hold/Release)
 */

//================================================================
// Common includes
//----------------------------------------------------------------

// util
#include <cstddef>
#include <cstdint>
#include <exception>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>

// file
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/types.h>

// net
#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <poll.h>

// time
#include <sys/time.h>
#include <time.h>

// special
#include <limits.h>
#include <signal.h>
#include <sched.h>

// std
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <memory>
#include <string>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <mutex>
#include <thread>

#ifndef NDEBUG
#define DEBUG
#endif

// check datatypes
static_assert(sizeof(char) == 1, "sizeof(char) != 1");
static_assert(sizeof(int) == 4, "sizeof(int) != 4");
static_assert(sizeof(time_t) == 8, "sizeof(time_t) != 8");
static_assert(int(true) == 1, "int(true) != 1");

// check ip address types
static_assert(sizeof(in_addr) == 4, "sizeof(in_addr) != 4");
static_assert(sizeof(in6_addr) == 16, "sizeof(in6_addr) != 16");

// byte units
#define KIB (1024)
#define MIB (1024*1024)

// time units (millisecond resolution)
#define MILLI (1)
#define SECOND (1000)
#define MINUTE (60*1000)
#define HOUR (60*60*1000)
#define DAY (24*60*60*1000)

// to/from external time resolution to milliseconds
#define TO_NANO(t) ((t)*1000000)
#define TO_MICRO(t) ((t)*1000)
#define TO_MILLI(t) (t)
#define TO_SECOND(t) ((t)/1000)
#define TO_MINUTE(t) ((t)/1000000)
#define FROM_NANO(t) ((t)/1000000)
#define FROM_MICRO(t) ((t)/1000)
#define FROM_MILLI(t) ((t))
#define FROM_SECOND(t) ((t)*1000)
#define FROM_MINUTE(t) ((t)*1000000)

// null values
#define NULL_POS (-1)
#define NULL_SIZE (-1)
#define NULL_TIME (-1)
#define NULL_HANDLE (-1)

// generic mutex stall timeout - capped within 0.1s:1d
#ifndef STALL_TIMEOUT
#define STALL_TIMEOUT (10*MINUTE)
#endif

// generic I/O stall timeout - capped within 0.1:60 [s]
#ifndef IO_TIMEOUT
#define IO_TIMEOUT (10*SECOND)
#endif

// standard buffer limit
#ifndef BLOCK_LIMIT
#define BLOCK_LIMIT (128*MIB)
#endif

// max buffer limit
#ifndef BLOCK_MAX
#define BLOCK_MAX (512*MIB)
#endif

// default max copy buffer
#ifndef FILE_CHUNK_MAX
#define FILE_CHUNK_MAX (4*MIB)
#endif

// default max try_send/receive per call
#ifndef TCP_CHUNK_MAX
#define TCP_CHUNK_MAX (1*MIB)
#endif

// string format buffer
#ifndef STRINGF_MAX
#define STRINGF_MAX (1000)
#endif

// blank null-terminated str (shared)
extern volatile int BLANK_INT;
#define BLANK ((char*)(&BLANK_INT))

// log levels
#define LOG_NONE (0)
#define LOG_ERROR (1)
#define LOG_WARNING (2)
#define LOG_NOTE (3)
#define LOG_DETAIL (4)

// default = LOG_NOTE
extern volatile int LOG_LEVEL;

// default = unlimited
extern volatile int LOG_LIMIT;

// path types (file system)
#define PATH_ANY (-1)
#define PATH_NONE (0)
#define PATH_DIR (0x01)
#define PATH_FILE (0x02)
#define PATH_LINK (0x04)
#define PATH_BLOCK (0x08)
#define PATH_SERIAL (0x10)
#define PATH_FIFO (0x20)
#define PATH_SOCKET (0x40)
#define PATH_SPECIAL (PATH_BLOCK|PATH_SERIAL|PATH_FIFO|PATH_SOCKET)
#define PATH_KNOWN (0x7F)
#define PATH_NODIR (PATH_KNOWN-PATH_DIR)
#define PATH_UNKNOWN (0x80)

//##############################################################//
//                                                              //
//                          B A S I C                           //
//                                                              //
//##############################################################//

//================================================================
// Global counters
//----------------------------------------------------------------

extern volatile int TOTAL_RESOURCES;

extern volatile int SILENT_WARNINGS;
extern volatile int SILENT_ERRORS;

extern volatile int LOGGED_DETAILS;
extern volatile int LOGGED_NOTES;
extern volatile int LOGGED_WARNINGS;
extern volatile int LOGGED_ERRORS;

#ifdef DEBUG
#define RESOURCE_PLUS __sync_fetch_and_add(&TOTAL_RESOURCES,1)
#define RESOURCE_MINUS __sync_fetch_and_sub(&TOTAL_RESOURCES,1)
#else
#define RESOURCE_PLUS
#define RESOURCE_MINUS
#endif

#define WARNING_PLUS __sync_fetch_and_add(&SILENT_WARNINGS,1)
#define ERROR_PLUS __sync_fetch_and_add(&SILENT_ERRORS,1)

void print_global_counters();

void clear_log_counters();

//================================================================
// Pointer offset
//----------------------------------------------------------------

// add byte offset (int) to void*
inline const void* ptr_add(const void* ptr, int offset) {
  return ((const char*)ptr) + offset;
}
inline void* ptr_add(void* ptr, int offset) {
  return ((char*)ptr) + offset;
}

// add byte offset (int) to char*
inline const char* ptr_add(const char* ptr, int offset) {
  return ptr + offset;
}
inline char* ptr_add(char* ptr, int offset) {
  return ptr + offset;
}

// subtract pointers and return byte offset (int>=0)
inline int ptr_sub(const void* end, const void* start) {
  return (int)((char*)end - (char*)start);
}

//================================================================
// Byte string (size based)
//----------------------------------------------------------------

inline bool data_equal(const void* left, int leftsize, const void* right, int rightsize)
{
  if( leftsize != rightsize ) { return false; }
  else { return ( memcmp(left,right,leftsize) == 0 ); }
}

// returns pos or nullpos if not found
int data_find(const void* data, int size, const void* subdata, int subsize, int nullpos=NULL_POS);
int data_find_byte(const void* data, int size, char byte, int nullpos=NULL_POS);
//int data_find_byteset(const void* data, int size, const void* oneof, int oneofsize, int nullpos=NULL_POS);

inline bool data_contains(const void* data, int size, const void* subdata, int subsize) {
  return ( data_find(data,size,subdata,subsize) >= 0 );
}

bool data_starts_with(const void* data, int size, const void* subdata, int subsize);
bool data_ends_with(const void* data, int size, const void* subdata, int subsize);

//================================================================
// Char string (zero terminated)
//----------------------------------------------------------------

// str (char*)

inline int str_size(const char* str) {
  return (int)strlen(str);
}

inline bool str_blank(const char* str) {
  return ( str==nullptr || *str=='\0' );
}

bool str_is_integer(const char* str);
bool str_is_number(const char* str);
bool str_is_label(const char* str, const char* special="_-.", bool start_with_digit=false);

inline bool str_equal(const char* left, const char* right) {
  return ( strcmp(left,right) == 0 );
}

bool str_equal(const char* left, int leftsize, const char* right);

// returns pos or NULL_POS if not found
int str_find(const char* str, const char* substr);
inline int str_find(const char* str, int size, const char* substr) {
  return data_find(str,size,substr,str_size(substr));
}

// returns pos or nullpos/NULL_POS if not found
int str_find_ascii(const char* str, char ascii);
inline int str_find_ascii(const char* str, int size, char ascii, int nullpos=NULL_POS) {
  return data_find_byte(str,size,ascii,nullpos);
}

// returns pos or nullpos/NULL_POS if not found
int str_find_asciiset(const char* str, const char* oneof);
int str_find_asciiset(const char* str, int size, const char* oneof, int nullpos=NULL_POS);

// returns pos after oneof or nullpos/NULL_POS if not found
int str_rfind_asciiset(const char* str, int size, const char* oneof, int nullpos=NULL_POS);

inline bool str_contains(const char* str, const char* substr) {
  return ( strstr(str,substr) != nullptr );
}
inline bool str_contains(const char* str, int size, const char* substr)
{
  return ( str_find(str,size,substr) >= 0 );
}

// returns 0 or next pos after "forward scaning" over continues ascii/oneof
int str_span_ascii(const char* str, char ascii);
int str_span_ascii(const char* str, int size, char ascii);
int str_span_asciiset(const char* str, const char* oneof);
int str_span_asciiset(const char* str, int size, const char* oneof);

// returns size or pos after "reverse scaning" over continues ascii/oneof
int str_rspan_ascii(const char* str, int size, char ascii);
int str_rspan_asciiset(const char* str, int size, const char* oneof);

// true/false
bool str_starts_with(const char* str, const char* substr);
bool str_starts_with(const char* str, int size, const char* substr);

// true/false
bool str_ends_with(const char* str, int size, const char* substr);
inline bool str_ends_with(const char* str, const char* substr) {
  return str_ends_with(str, str_size(str), substr);
}

// integer conversion (+/-n)
// optional label used in error message
int str_to_int(const char* str, const char* label=nullptr);
int64_t str_to_int64(const char* str, const char* label=nullptr);

// number conversion (+/-n.n, throws on 64-bit overflow)
// optional label used in error message
// TODO: handle numbers like 0.00000000000000123
double str_to_double(const char* str, const char* label=nullptr);

// utf8 codepoint count
int utf8_points(const char* utf8);

//----------------------------------------------------------------

// double compare

// relative 9-digit comparison
inline bool eq9(double a, double b)
{
  double delta = std::abs(a - b);
  double limit = std::max(std::abs(a),std::abs(b)) * 1e-9;
  return delta <= limit;
}

// relative 12-digit comparison
inline bool eq12(double a, double b)
{
  double delta = std::abs(a - b);
  double limit = std::max(std::abs(a),std::abs(b)) * 1e-12;
  return delta <= limit;
}

// relative 15-digit comparison
inline bool eq15(double a, double b)
{
  double delta = std::abs(a - b);
  double limit = std::max(std::abs(a),std::abs(b)) * 1e-15;
  return delta <= limit;
}

// n-decimal comparison (absolute precision)
inline bool eqd(double a, double b, int decimals) {
  double p = std::pow(10,decimals);
  return eq15(std::round(a*p), std::round(b*p));
}

//----------------------------------------------------------------

// string

typedef std::string string;
typedef std::stringstream stringstream;

// 64-bit cast when using %lld or %llu/llx
#define LLD long long int
#define LLU unsigned long long int

// get "int" size
#define intsize(x) ((int)((x).size()))

// printf => string (using minimal C-locale for numbers)
// truncated at STRINGF_MAX (ERROR_PLUS)
string stringf(const char* format, va_list args) noexcept;
string stringf(const char* format, ...) noexcept __attribute__((__format__(__printf__,1,2)));

//================================================================
// Value containers
//----------------------------------------------------------------

// ValVec<T:value> => ValVec[pos:int] = T

template<class T>
struct ValVec : std::vector<T>
{
  using std::vector<T>::vector;
  // size
  int size() { return (int)std::vector<T>::size(); }
  int space() { return (int)std::vector<T>::capacity(); }
  void reserve(int space) { std::vector<T>::reserve((unsigned)space); }
  // add
  void add(const T& value) { this->push_back(value); }
  void add(T&& value) { this->push_back(value); }
  template<class... Args> void add_new(Args&&... args) {
    this->emplace_back(args...);
  }
  // insert
  void insert(int pos, const T& value) {
    assert( pos>=0 && pos<=size() );
    std::vector<T>::insert(this->begin()+pos, value);
  }
  void insert(int pos, T&& value) {
    assert( pos>=0 && pos<=size() );
    std::vector<T>::insert(this->begin()+pos, value);
  }
  template<class... Args> void insert_new(int pos, Args&&... args) {
    assert( pos>=0 && pos<=size() );
    this->emplace(this->begin()+pos, args...);
  }
  // remove
  void remove(int pos) {
    assert( pos>=0 && pos<size() );
    this->erase(this->begin()+pos);
  }
  // at(pos) will always throw if out of range
  T& at(int pos) { return std::vector<T>::at((unsigned)pos); }
	const T& at(int pos) const { return std::vector<T>::at((unsigned)pos); }
  // [pos] will only assert range in debug
  T& operator[](int pos) {
    assert( pos>=0 && pos<size() );
    return std::vector<T>::operator[]((unsigned)pos);
  }
	const T& operator[](int pos) const {
    assert( pos>=0 && pos<size() );
    return std::vector<T>::operator[]((unsigned)pos);
  }
};

// ValSet<T:value[,C:less]> => ValSet[T]

template<class T, class C=std::less<T>>
struct ValSet : std::set<T,C>
{
  using std::set<T,C>::set;
  // size
  int size() { return (int)std::set<T,C>::size(); }
  // add (true if added)
  bool add(const T& value) { return this->insert(value).second; }
  bool add(T&& value) { return this->insert(value).second; }
  template<class... Args> bool add_new(Args&&... args) {
    return this->emplace(args...).second;
  }
  // remove (true if removed, use erase if iterator)
  bool remove(const T& value) {
    return ( this->erase(value) > 0 );
  }
  // contains (true if exists)
  bool contains(const T& value) const {
    return ( this->find(value) != this->end() );
  }
};

// ValMap<K:value,T:value[,C:less]> => ValMap[K] = T

template<class K, class T, class C=std::less<K>>
struct ValMap : std::map<K,T,C>
{
  using std::map<K,T,C>::map;
  // size
  int size() { return (int)std::map<K,T,C>::size(); }
  // add (true if new key)
  bool add(K key, const T& value) {
    bool newkey = ( this->erase(key) == 0 );
    this->emplace(key,value);
    return newkey;
  }
  bool add(K key, T&& value) {
    bool newkey = ( this->erase(key) == 0 );
    this->emplace(key,value);
    return newkey;
  }
  // add if key is missing (skip erase lookup)
  void add_missing(K key, const T& value) {
    this->emplace(key,value);
  }
  void add_missing(K key, T&& value) {
    this->emplace(key,value);
  }
  // remove (true if removed, use erase if iterator)
  bool remove(K key) {
    return ( this->erase(key) > 0 );
  }
  // contains (true if key exists)
  bool contains(K key) const {
    return ( this->find(key) != this->end() );
  }
  // get (use with copy-friendly T)
  T get(K key, const T& nullvalue) const {
    auto entry = this->find(key);
    if( entry == this->end() ) { return nullvalue; }
    return entry->second;
  }
  // at(key) will throw if not found
  // [key] will return added default T() if not found!!!
};

//================================================================
// Subref
//----------------------------------------------------------------

// parser helper
// use together with str_ and data_ functions

struct Subref
{
  const char* ptr; // pubrd
  int size;  // pubrd
  int space; // pubrw

  Subref() {
    this->ptr=nullptr; this->size=0; this->space=BLOCK_MAX;
  }
  Subref(const char* start, int size=0, int space=BLOCK_MAX) {
    this->ptr=start; this->size=size; this->space=space;
  }

  void init() {
    this->ptr=nullptr; this->size=0; this->space=BLOCK_MAX;
  }
  void init(const char* start, int size=0, int space=BLOCK_MAX) {
    this->ptr=start; this->size=size; this->space=space;
  }

  // step ahead n bytes after init/step/next (false if overflow)
  // assert size 0
  bool forward(int bytes) {
    assert( size==0 );
    this->ptr += bytes;
    this->space -= bytes;
    return ( this->space >= 0 );
  }

  // step ahead n bytes after span (false if above size)
  bool trim(int bytes) {
    this->ptr += bytes;
    this->size -= bytes;
    this->space -= bytes;
    return ( this->size >= 0 );
  }

  // set byte size (false if overflow)
  bool span(int size) {
    this->size = size;
    return ( this->size <= this->space );
  }

  // step over "size" bytes (false if overflow)
  bool step() {
    this->ptr += size;
    this->space -= size;
    this->size = 0;
    return ( this->size <= this->space );
  }

  // step over "size" bytes (false if no next)
  bool next() {
    this->ptr += size;
    this->space -= size;
    this->size = 0;
    return ( this->size < this->space );
  }

  operator bool() const { return ( this->size != 0 ); }
  bool operator==(const char* str) const { return str_equal(this->ptr,this->size,str); }
  bool operator==(const Subref& src) const { return data_equal(this->ptr,this->size,src.ptr,src.size); }
  bool operator!=(const char* str) const { return !str_equal(this->ptr,this->size,str); }
  bool operator!=(const Subref& src) const { return !data_equal(this->ptr,this->size,src.ptr,src.size); }

  // writable pointer
  char* write() const { return const_cast<char*>(this->ptr); }

  // end pointer
  const char* end() const { return this->ptr + this->size; }

  // 8-bit conversions
  int8_t int8(int pos=0) {
    assert( pos < this->size );
    return (int8_t)this->ptr[pos];
  }
  uint8_t uint8(int pos=0) {
    assert( pos < this->size );
    return (uint8_t)this->ptr[pos];
  }
  void int8(int pos, int8_t value) {
    assert( pos < this->size );
    write()[pos] = (char)value;
  }
  void uint8(int pos, uint8_t value) {
    assert( pos < this->size );
    write()[pos] = (char)value;
  }

  // 16-bit conversions
  uint16_t uint16le(int pos=0);
  int16_t int16le(int pos=0) { return (int16_t)uint16le(pos); }
  uint16_t uint16be(int pos=0);
  int16_t int16be(int pos=0) { return (int16_t)uint16be(pos); }
  void uint16le(int pos, uint16_t value);
  void int16le(int pos, int16_t value) { uint16le(pos,(uint16_t)value); }
  void uint16be(int pos, uint16_t value);
  void int16be(int pos, int16_t value) { uint16be(pos,(uint16_t)value); }

  // 32-bit conversions
  uint32_t uint32le(int pos=0);
  int32_t int32le(int pos=0) { return (int32_t)uint32le(pos); }
  uint32_t uint32be(int pos=0);
  int32_t int32be(int pos=0) { return (int32_t)uint32be(pos); }
  void uint32le(int pos, uint32_t value);
  void int32le(int pos, int32_t value) { uint32le(pos,(uint32_t)value); }
  void uint32be(int pos, uint32_t value);
  void int32be(int pos, int32_t value) { uint32be(pos,(uint32_t)value); }

  // 64-bit conversions
  uint64_t uint64le(int pos=0);
  int64_t int64le(int pos=0) { return (int64_t)uint64le(pos); }
  uint64_t uint64be(int pos=0);
  int64_t int64be(int pos=0) { return (int64_t)uint64be(pos); }
  void uint64le(int pos, uint64_t value);
  void int64le(int pos, int64_t value) { uint64le(pos,(uint64_t)value); }
  void uint64be(int pos, uint64_t value);
  void int64be(int pos, int64_t value) { uint64be(pos,(uint64_t)value); }

  // copy to string
  operator string() const { return move(string(this->ptr,this->size)); }
};

//================================================================
// Subterm
//----------------------------------------------------------------

// Subref with temporary null-termination, CAUTION!

struct Subterm : Subref
{
  char saved = '\0'; // pubrd

  using Subref::Subref;
  ~Subterm() noexcept { restore(); }

  void terminate() {
    assert( this->saved=='\0' );
    this->saved = this->ptr[this->size];
    char* ptr = const_cast<char*>(this->ptr);
    ptr[this->size] = '\0';
  }

  void restore() {
    if( saved != '\0' ) {
      char* ptr = const_cast<char*>(this->ptr);
      ptr[this->size] = this->saved;
      this->saved = '\0';
    }
  }

  bool forward(int bytes) {
    assert( this->saved=='\0' );
    return Subref::forward(bytes);
  }

  bool span(int size) {
    assert( this->saved=='\0' );
    return Subref::span(size);
  }

  bool step() {
    restore();
    return Subref::step();
  }

  bool next() {
    restore();
    return Subref::next();
  }
};

//##############################################################//
//                                                              //
//                       I N S T A N C E                        //
//                                                              //
//##############################################################//

//================================================================
// Instance (base)
//----------------------------------------------------------------

// base interface
// use with Ref<T> when object has been created with "new"

struct IInstance
{
  // get "Type(id)" representation (used by Error)
  virtual string get_identifier() noexcept = 0;

  // probe refs
  virtual int get_refs() noexcept = 0;

  // ref++ (managed by Ref<T>)
  virtual void enref() = 0;

  // ref-- (managed by Ref<T>)
  virtual void deref() noexcept = 0;
};

//----------------------------------------------------------------

// base object

// CAUTION:
// - exception in constructor => will skip associated destructor!
// - set defaults and "throw" at start of constructor,
//   or do proper cleanup before "throw"
// - avoid "throw" in destructor (soft fail with ERROR_PLUS)

struct Instance : virtual IInstance
{
  // construction (bottom to top)
  Instance() { RESOURCE_PLUS; }

  // destruction via vtable (top to bottom)
  virtual ~Instance() noexcept;

  // unclonable
  Instance(const Instance&) = delete;
  void operator=(const Instance&) = delete;

  // IInstance
  string get_identifier() noexcept override;
  int get_refs() noexcept override;
  void enref() override;
  void deref() noexcept override;

protected:
  int refs = 0;
};

//================================================================
// Ref (<T:IInstance>)
//----------------------------------------------------------------

// holder of "new" instance

template<class T>
struct Ref
{
  T* ptr; // pubrd

  // construct (enref)
  Ref() {
    ptr = nullptr;
  }
  Ref(const Ref<T>& enref) {
    T* enptr = enref.ptr;
    if( enptr != nullptr ) {
      enptr->enref();
    }
    this->ptr = enptr;
  }
  Ref(T* enptr) {
    if( enptr != nullptr ) {
      enptr->enref();
    }
    this->ptr = enptr;
  }

  // destruct (deref)
  ~Ref() {
    IInstance* deptr = this->ptr;
    if( deptr != nullptr ) {
      this->ptr = nullptr;
      deptr->deref();
    }
  }

  // assign ref (enref+deref)
  Ref<T>& operator=(const Ref<T>& enref) {
    IInstance* deptr = this->ptr;
    T* enptr = enref.ptr;
    if( enptr != nullptr ) {
      enptr->enref();
      this->ptr = enptr;
    }
    else {
      this->ptr = nullptr;
    }
    if( deptr != nullptr ) {
      deptr->deref();
    }
    return *this;
  };

  // assign ptr (enref+deref)
  Ref<T>& operator=(T* enptr) {
    IInstance* deptr = this->ptr;
    if( enptr != nullptr ) {
      enptr->enref();
      this->ptr = enptr;
    }
    else {
      this->ptr = nullptr;
    }
    if( deptr != nullptr ) {
      deptr->deref();
    }
    return *this;
  }

  // act on
  T* operator->() { return this->ptr; }

  // cast to pointer
  operator T*() { return this->ptr; }

  // compare
  operator bool() const { return ( this->ptr != nullptr ); }
  bool operator<(const Ref<T>& b) const { return ( this->ptr < b.ptr ); }
  bool operator==(const Ref<T>& b) const { return ( this->ptr == b.ptr ); }
  bool operator!=(const Ref<T>& b) const { return ( this->ptr != b.ptr ); }
};

//================================================================
// Instance containers
//----------------------------------------------------------------

// CAUTION:
//
// iteration pattern to use when removing entries inside loop...
//
// for(auto i=container.begin(); i!=container.end();) {
//   auto entry = *i++;
//   if( ... ) {
//     container.remove(entry);
//   }
// }

//----------------------------------------------------------------

// RefVec<T:IInstance> => RefVec[pos:int] = Ref<T>

template<class T>
struct RefVec : std::vector<Ref<T>>
{
  using std::vector<Ref<T>>::vector;
  // size
  int size() { return (int)std::vector<Ref<T>>::size(); }
  int space() { return (int)std::vector<Ref<T>>::capacity(); }
  void reserve(int space) { std::vector<Ref<T>>::reserve((unsigned)space); }
  // add
  void add(T* ptr) { this->emplace_back(ptr); }
  // insert
  void insert(int pos, T* ptr) {
    assert( pos>=0 && pos<=size() );
    this->emplace(this->begin()+pos, ptr);
  }
  // remove
  void remove(int pos) {
    assert( pos>=0 && pos<size() );
    this->erase(this->begin()+pos);
  }
  // at(pos) will always throw if out of range
  T* at(int pos) { return std::vector<Ref<T>>::at((unsigned)pos); }
  // [pos] will only assert range in debug
  T* operator[](int pos) {
    assert( pos>=0 && pos<size() );
    return std::vector<Ref<T>>::operator[]((unsigned)pos);
  }
};

// RefSet<T:IInstance[,C:less]> => RefSet[Ref<T>]
// add/remove nullptr => ignore

template<class T, class C=std::less<Ref<T>>>
struct RefSet : std::set<Ref<T>,C>
{
  using std::set<Ref<T>,C>::set;
  // size
  int size() { return (int)std::set<Ref<T>,C>::size(); }
  // add (true if added)
  bool add(T* ptr) {
    if( ptr == nullptr ) { return false; }
    return this->emplace(ptr).second;
  }
  // remove (true if removed, use erase if iterator)
  bool remove(T* ptr) {
    if( ptr == nullptr ) { return false; }
    return ( this->erase(ptr) > 0 );
  }
  // contains (true if exists)
  bool contains(T* ptr) const {
    return ( this->find(ptr) != this->end() );
  }
};

// RefMap<K:value,T:IInstance[,C:less]> => RefMap[K] = Ref<T>
// add nullptr => remove key if exists
// get none => nullptr

template<class K, class T, class C=std::less<K>>
struct RefMap : std::map<K,Ref<T>,C>
{
  using std::map<K,Ref<T>,C>::map;
  // size
  int size() { return (int)std::map<K,Ref<T>,C>::size(); }
  // add (true if new key)
  bool add(K key, T* ptr) {
    bool newkey = ( this->erase(key) == 0 );
    if( ptr == nullptr ) { return false; }
    this->emplace(key,ptr);
    return newkey;
  }
  // add if key is missing (skip erase lookup)
  bool add_missing(K key, T* ptr) {
    if( ptr == nullptr ) { return false; }
    this->emplace(key,ptr);
    return true;
  }
  // remove (true if removed, use erase if iterator)
  bool remove(K key) {
    return ( this->erase(key) > 0 );
  }
  // contains (true if key exists)
  bool contains(K key) const {
    return ( this->find(key) != this->end() );
  }
  // get (nullptr if not found)
  T* get(K key) const {
    auto entry = this->find(key);
    if( entry == this->end() ) { return nullptr; }
    return entry->second;
  }
  // at(key) will throw if not found
  // [key] will return added Ref<T>(nullptr) if not found!!!
};

//================================================================
// Exceptions
//----------------------------------------------------------------

// base exception with system message

struct Exception : std::exception
{
  // pubrd: source or blank
  string source;

  // pubrd: system message in english
  string message;

  Exception() { }
  Exception(const char* reason) { init(reason); }
  Exception(IInstance* source, const char* reason) { init(source, reason); }

  // constructor helpers
  void init(const char* reason) noexcept;
  void init(const char* format, va_list args) noexcept;
  void init(IInstance* source, const char* reason) noexcept;
  void init(IInstance* source, const char* format, va_list args) noexcept;

  // destruction via vtable
  virtual ~Exception() noexcept;

  // system message in english
  virtual const char* what() const noexcept override;
};

//----------------------------------------------------------------

// unintentional error

struct Error : Exception
{
  Error(const char* format, ...) noexcept
    __attribute__((__format__(__printf__,2,3)))
  {
    va_list args;
    va_start(args, format);
    init(format, args);
    va_end(args);
  }
  Error(IInstance* source, const char* format, ...) noexcept
    __attribute__((__format__(__printf__, 3, 4)))
  {
    va_list args;
    va_start(args, format);
    init(source, format, args);
    va_end(args);
  }
};

//----------------------------------------------------------------

// intentional exceptions

// peer disconnected (thrown by send/receive)
struct DisconnectException : Exception
{
  DisconnectException(const char* format, ...) noexcept
    __attribute__((__format__(__printf__,2,3)))
  {
    va_list args;
    va_start(args, format);
    init(format, args);
    va_end(args);
  }
  DisconnectException(IInstance* source, const char* format, ...) noexcept
    __attribute__((__format__(__printf__, 3, 4)))
  {
    va_list args;
    va_start(args, format);
    init(source, format, args);
    va_end(args);
  }
};

// exit has been requested
struct StopException : Exception
{
  StopException(const char* reason) { init(reason); }
};

//================================================================
// Buffer (Instance)
//----------------------------------------------------------------

// byte buffer (char*)
// ending with 1-byte zero terminator (not included in size)
// malloced if space > 0, otherwise BLANK

//----------------------------------------------------------------

// helper functions

// calculate new space, rounded up to 64,128,256,...,limit
int block_space_up(int sizenew, int limit);

// expand "bytes" at subptr, returns subptr (realloc befor call)
void* block_insert(void* endptr, void* subptr, int bytes);

// contract "bytes" at subptr, returns subptr (realloc after call)
void block_remove(void* endptr, void* subptr, int bytes);

//----------------------------------------------------------------

struct Buffer : Instance
{
  int   size;  // pubrd: occupied space [bytes]
  char* data;  // pubrd: malloced if space > 0, otherwise BLANK
  int   space; // pubrd: allocated space [bytes]
  int   limit; // pubrw: max space [bytes]

  // vacant space [bytes]
  int vacant() { return (this->space - this->size); }

  // *** life cycle ***

  // no pre-allocation, auto-expansion up to BLOCK_LIMIT
  Buffer();

  // pre-allocation, limit=NULL_SIZE => limit=space
  Buffer(int space, int limit=NULL_SIZE);

  // destruct
  ~Buffer() noexcept override;

  // *** space ***

  // ensure some minimum space (will not shrink), returns new space
  int reserve(int spacemin) {
    if( spacemin > this->space ) {
      return optimize(spacemin);
    }
    else {
      return this->space;
    }
  }

  // ensure optimal space (expand or shrink), returns new space
  int optimize(int spacemin=0);

  // *** set ***

  // set zero size with pre-allocation, returns new space
  int preset(int space) { set(0); return optimize(space); }

  // set zero size
  void clear() { set(0); }

  // set size, returns pointer to data
  char* set(int bytes);

  // set new, returns pointer to data
  char* set(const char* str) {
    return strcpy(set(str_size(str)), str);
  }
  char* set(const char* data, int bytes) {
    return (char*)memcpy(set(bytes), data, bytes);
  }
  char* set(const Buffer& buffer) {
    return (char*)memcpy(set(buffer.size), buffer.data, buffer.size);
  }
  char* setf(const char* format, ...) __attribute__((__format__(__printf__,2,3)));

  // *** add ***

  // add area at the end, returns pointer to area
  char* add(int bytes);

  // add at the end
  void add(const char* str) {
    strcpy(add(str_size(str)),str);
  }
  void addn() {
    *add(1) = '\n';
  }
  void addn(const char* str) {
    strcpy(add(str_size(str)+1),str);
    this->data[this->size-1] = '\n';
  }
  void add(const char* data, int bytes) {
    memcpy(add(bytes), data, bytes);
  }
  void add(const Buffer& buffer) {
    memcpy(add(buffer.size), buffer.data, buffer.size);
  }
  void add(const ValVec<string>& lines) {
    for(auto& line : lines) {
      addn(line.c_str());
    }
  }
  void add(const ValSet<string>& lines) {
    for(auto& line : lines) {
      addn(line.c_str());
    }
  }
  void addf(const char* format, ...) __attribute__((__format__(__printf__,2,3)));
  void addfn(const char* format, ...) __attribute__((__format__(__printf__,2,3)));

  // helper: returns 0 if success and "needed size" if overflow (expand and retry)
  static int addf(Buffer* buffer, const char* format, va_list args);

  // *** insert ***

  // insert area at pos, returns pointer to area
  char* insert(int pos, int bytes);

  // insert at pos
  void insert(int pos, const char* str) { strcpy(insert(pos,str_size(str)), str); }
  void insert(int pos, const char* data, int bytes) { memcpy(insert(pos,bytes), data, bytes); }
  void insert(int pos, const Buffer& buffer) { memcpy(insert(pos,buffer.size), buffer.data, buffer.size); }

  // *** remove ***

  // remove area at pos
  void remove(int pos, int bytes);

  // *** probe ***

  operator bool() const { return ( this->size != 0 ); }
  bool operator==(const char* str) const { return str_equal(this->data,this->size,str); }
  bool operator==(const Buffer& src) const { return data_equal(this->data,this->size,src.data,src.size); }
  bool operator!=(const char* str) const { return !str_equal(this->data,this->size,str); }
  bool operator!=(const Buffer& src) const { return !data_equal(this->data,this->size,src.data,src.size); }

  // end pointer
  char* end() const { return this->data + this->size; }

  // copy to string
  operator string() const { return move(string(this->data,this->size)); }
};

//##############################################################//
//                                                              //
//                         S Y S T E M                          //
//                                                              //
//##############################################################//

//================================================================
// Time functions
//----------------------------------------------------------------

// sleep [ms]

// short thread sleep for max 1 second (~1-50ms accuracy)
// capped within 0.5:1000 [ms]
// will sleep for "at least" n [ms]
// will return actual time slept (>0) [ms]
// will NOT throw StopException
int64_t short_sleep(int ms, bool yield=false);

// long thread sleep for max 1 minute (~100ms accuracy)
// capped within 0.1s:1d
// will return actual time slept (>0) [ms]
// may throw StopException
int64_t long_sleep(int ms);

//----------------------------------------------------------------

// coarse mono time [ms]

// some ~5ms accuracy (times 10 if thread preemption occurs)
// primarily used for stall-timeout calculations
// will NOT count CPU suspend time!

// get 64-bit coarse time (never NULL_TIME) [ms]
int64_t get_mono();

// get duration (>=0) "from" a coarse mono time [ms]
// will throw on negative duration
int64_t get_duration(int64_t coarse_from, int64_t coarse_now=NULL_TIME);

//----------------------------------------------------------------

// "proper" mono time [ms]

// use for "millisecond accurate" stop-watch measurements
// consider dedicated CPU core and/or FIFO/RR thread scheduling
// standard linux may "correct" mono time with max 0.5ms/sec
// will count CPU suspend time!

// get 64-bit proper mono time (never NULL_TIME) [ms]
int64_t get_proper_mono();

// get duration (>=0) "from" a proper mono time [ms]
// will throw runtime_error on negative duration
int64_t get_proper_duration(int64_t proper_from, int64_t proper_now=NULL_TIME);

// get strict duration "from" a proper+coarse time (>=0) [ms]
// will throw on negative duration
// will throw if CPU has been suspended
int64_t get_strict_duration(int64_t proper_from, int64_t coarse_from);

//----------------------------------------------------------------

// periodic timer

// will trigger (true) after period>=now-from or if from=NULL_TIME
// will update "from" when triggered
// will throw on negative duration
bool has_elapsed(int64_t period, int64_t* from, int64_t now);

//----------------------------------------------------------------

// "nano" mono time [ns]

// use for short "high-resolution" performance measurements
// consider filtering out "outlier" preemption events (>1ms)
// precision will be system dependent

// get short 64-bit nano time (never NULL_TIME) [ns]
int64_t get_nano_mono();

// get duration (>=0) "from" a nano time [ns]
// will throw on negative duration
int64_t get_nano_duration(int64_t nano_from, int64_t nano_now=NULL_TIME);

//----------------------------------------------------------------

// world clock [milliseconds since 1970]

// CAUTION: subject to leap-second/manual adjustments

// get 64-bit real-time clock in utc (never NULL_TIME) [ms]
int64_t get_utc();

// TODO: host_zone_sec()

// utc [ms] -> utc/zone time fields (resolution = seconds)
void get_time_fields(tm* fields, int64_t utc, int zone_sec=0);

// utc [ms] -> host time fields (resolution = seconds)
void get_host_time_fields(tm* fields, int64_t utc);

// utc(+zone) [ms] -> millisecond field 0-999 (zone independent)
int get_msec_field(int64_t utc_or_local);

// utc [ms] -> utc/zone time string (finest resolution = seconds)
string format_time(const char* format, int64_t utc, int zone_sec=0);

// utc [ms] -> host time string (finest resolution = seconds)
string format_host_time(const char* format, int64_t utc);

//================================================================
// Lifetime management
//----------------------------------------------------------------

// exit codes
#define APP_NO_EXIT (-1)
#define APP_OK_EXIT (0)
#define APP_RESTART_EXIT (1)
#define APP_ERROR_EXIT (2)

// app global exit state
extern volatile int APP_EXIT;
extern volatile bool APP_INTERNAL_RESTART;

// called by main
void app_start();

// request exit
void app_req_exit(int code) noexcept;

// exit check
void app_check();

// resource check on exit
bool app_end(int instances_max=0, int mallocs_max=0, int handles_max=0) noexcept;

//================================================================
// App logging
//----------------------------------------------------------------

// log interface

struct ILog : virtual IInstance
{
  virtual void detail(const char* line, const char* source=nullptr) noexcept = 0;
  virtual void note(const char* line, const char* source=nullptr) noexcept = 0;
  virtual void warning(const char* line, const char* source=nullptr) noexcept = 0;
  virtual void error(const char* line, const char* source=nullptr) noexcept = 0;
};

//----------------------------------------------------------------

// default log (printf)

struct Log : Instance, virtual ILog
{
  void detail(const char* line, const char* source=nullptr) noexcept override;
  void note(const char* line, const char* source=nullptr) noexcept override;
  void warning(const char* line, const char* source=nullptr) noexcept override;
  void error(const char* line, const char* source=nullptr) noexcept override;
};

extern Ref<ILog> APP_LOG;

//----------------------------------------------------------------

// detail
inline void log_detail(const char* line, const char* source=nullptr) noexcept {
  APP_LOG->detail(line, source);
}
void detailf(const char* format, va_list args, IInstance* source=nullptr) noexcept;
void detailf(const char* format, ...) noexcept __attribute__((__format__(__printf__,1,2)));
void detailf(IInstance* source, const char* format, ...) noexcept __attribute__((__format__(__printf__,2,3)));

// note
inline void log_note(const char* line, const char* source=nullptr) noexcept {
  APP_LOG->note(line, source);
}
void notef(const char* format, va_list args, IInstance* source=nullptr) noexcept;
void notef(const char* format, ...) noexcept __attribute__((__format__(__printf__,1,2)));
void notef(IInstance* source, const char* format, ...) noexcept __attribute__((__format__(__printf__,2,3)));

// warning
inline void log_warning(const char* line, const char* source=nullptr) noexcept {
  APP_LOG->warning(line, source);
}
void warningf(const char* format, va_list args, IInstance* source=nullptr) noexcept;
void warningf(const char* format, ...) noexcept __attribute__((__format__(__printf__,1,2)));
void warningf(IInstance* source, const char* format, ...) noexcept __attribute__((__format__(__printf__,2,3)));

// error
inline void log_error(const std::exception& e) noexcept {
  APP_LOG->error(e.what());
}
inline void log_error(const Exception& e) noexcept {
  APP_LOG->error(e.message.c_str(), e.source.c_str());
}
inline void log_error(const char* line, const char* source=nullptr) noexcept {
  APP_LOG->error(line, source);
}
void errorf(const char* format, va_list args, IInstance* source=nullptr) noexcept;
void errorf(const char* format, ...) noexcept __attribute__((__format__(__printf__,1,2)));
void errorf(IInstance* source, const char* format, ...) noexcept __attribute__((__format__(__printf__,2,3)));

//================================================================
// Handle (Instance)
//----------------------------------------------------------------

// base for file/socket classes

struct Handle : Instance
{
  int  handle; // pubrd: file/socket descriptor

  Handle() { this->handle = NULL_HANDLE; }
  ~Handle() noexcept override;

  string get_identifier() noexcept override;

  // take ownership of raw handle
  void set(int handle);

  // ensure closed
  virtual void close() noexcept;

  bool is_closed() { return ( this->handle < 0 ); }
};

//================================================================
// Thread (scope control)
//----------------------------------------------------------------

// holds mutex lock within scope

// will throw on exit request or timeout
// timeout capped within 0.1s:1d (default=STALL_TIMEOUT)

typedef std::timed_mutex timed_mutex;

struct Hold
{
  timed_mutex* locked = nullptr;
  Hold(timed_mutex* mutex, int stall_timeout=NULL_TIME);
  ~Hold() noexcept;
};

//----------------------------------------------------------------

// release mutex lock within scope

struct Release
{
  timed_mutex* unlocked;
  Release(timed_mutex* mutex);
  ~Release() noexcept; // may hang!
};

//----------------------------------------------------------------

// run thread within scope, ends with join()

struct Task
{
  std::thread& thread;
  volatile bool* stop;
  Task(std::thread& thread, volatile bool* stop=nullptr) : thread(thread) {
    this->stop = stop;
  }
  void end() noexcept {
    if( this->stop != nullptr ) {
      *this->stop = true;
    }
    this->thread.join(); // may hang
  }
  void complete() noexcept {
    this->thread.join(); // may hang
  }
  ~Task() noexcept {
    end(); // may hang
  }
};

//================================================================
// Shell
//----------------------------------------------------------------

// run shell command

// returns status code
int shell(const char* cmdline, ValVec<string>* outerr, bool add=false);

// returns stdout+stderr, throws if status >= err_min
// throw message: cmdline or last line if "last line err" status
ValVec<string> shell(const char* cmdline,
  int err_min=1, bool last_line_err_max=8, bool last_line_err_255=true);

//##############################################################//
//                                                              //
//                        N E T W O R K                         //
//                                                              //
//##############################################################//

//================================================================
// PortIp
//----------------------------------------------------------------

// IPv4/v6: ip (including blank ip) + port (including 0)

// default is blank IPv4 (0.0.0.0) and port 0
// uses copy semantics, no inheritance, no virtual

// string size to hold all possible ip:port representations
// IPv4: 0.0.0.0 | 0.0.0.0:port | n.n.n.n | n.n.n.n:port
// IPv6: :: | [::]:port | ip6 |[ip6]:port
#define IPSTR_SPACE 48

typedef uint32_t Ip4be;

// Ip4be std::set comparator
struct Ip4Less {
  bool operator()(Ip4be a, Ip4be b) const { return ( memcmp(&a,&b,4) < 0 ); }
};

extern Ip4be IP4BE_BLANK;
extern Ip4be IP4BE_LOOPBACK;

extern const uint64_t* IP6BE_BLANK;
extern const uint64_t* IP6BE_LOOPBACK;

// get ip version, returns 4 or 6 or throws error
int get_ipv(const char* ip);

// ip4 raw compare
inline bool ip4_equal(const in_addr& ip4be1, const in_addr& ip4be2) {
  return ( ip4be1.s_addr == ip4be2.s_addr );
}

// ip6 raw compare
inline bool ip6_equal(const uint64_t* ip6be1, const uint64_t* ip6be2) {
  return ( ip6be1[0]==ip6be2[0] && ip6be1[1]==ip6be2[1] );
}
inline bool ip6_equal(const in6_addr& ip6be1, const in6_addr& ip6be2) {
  return ip6_equal((const uint64_t*)&ip6be1, (const uint64_t*)&ip6be2);
}

//----------------------------------------------------------------

struct Ip6be
{
  uint64_t dual[2];

  // construct
  Ip6be() {
    this->dual[0]=0; this->dual[1]=0;
  }
  Ip6be(const Ip6be& source) {
    *this = source;
  }
  Ip6be(const uint64_t* ip6be) {
    *this = ip6be;
  }
  Ip6be(const in6_addr& ip6be) {
    *this = (const uint64_t*)&ip6be;
  }

  // assign
  void operator=(const Ip6be& source) {
    this->dual[0]=source.dual[0]; this->dual[1]=source.dual[1];
  }
  void operator=(const uint64_t* ip6be) {
    this->dual[0]=ip6be[0]; this->dual[1]=ip6be[1];
  }
  void operator=(const in6_addr& ip6be) {
    *this = (const uint64_t*)&ip6be;
  }

  // cast to pointer
  operator uint64_t*() { return this->dual; }

  // compare
  operator bool() const {
    return ( this->dual[0]!=0 || this->dual[1]!=0 );
  }
  bool operator<(const Ip6be& b) const {
    return ( memcmp(this, &b, 16) < 0 );
  }
  bool operator==(const Ip6be& b) const {
    return ip6_equal(this->dual, b.dual);
  }
  bool operator!=(const Ip6be& b) const {
    return !ip6_equal(this->dual, b.dual);
  }
};

//----------------------------------------------------------------

union PortIp
{
  // space large enough for both IPv4 and IPv6 raw ip:port
  sockaddr addr;
  sockaddr_in addr4;
  sockaddr_in6 addr6;

  // construct
  PortIp() {
    clear();
  }
  PortIp(const PortIp& source) {
    if( this != &source ) {
      set(source.addr);
    }
  }
  PortIp(const char* ip, int port, bool ip4only=false) {
    set(ip,port,ip4only);
  }
  PortIp(const sockaddr& rawportip) {
    set(rawportip);
  }
  PortIp(Ip4be ip4be, int port) {
    set4(ip4be,port);
  }
  PortIp(const uint64_t* ip6be, int port) {
    set6(ip6be,port);
  }
  PortIp(const in_addr& ip4be, int port) {
    set4((Ip4be)ip4be.s_addr,port);
  }
  PortIp(const in6_addr& ip6be, int port) {
    set6((const uint64_t*)&ip6be,port);
  }

  // assign
  void operator=(const PortIp& source) {
    if( this != &source ) {
      set(source.addr);
    }
  }

  // string representation of ip:port
  void address(char* buffer, int space) noexcept;
  string address() noexcept;

  // convert from raw
  int ipv() noexcept; // returns 4 or 6
  string ip() noexcept;
  void get_ip(char* buffer, int space) noexcept;
  int port() noexcept { return ::ntohs(addr4.sin_port); }

  // 0.0.0.0:0
  void clear() {
    bzero(this,sizeof(PortIp));
    this->addr.sa_family = AF_INET;
  }

  // returns ipv or throws error
  int set(const char* ip, int port, bool ip4only=false);
  int set(const sockaddr& rawportip);
  int set4(Ip4be ip4be, int port);
  int set6(const uint64_t* ip6be, int port);
  int set4(const in_addr& ip4be, int port) {
    return set4((Ip4be)ip4be.s_addr, port);
  }
  int set6(const in6_addr& ip6be, int port) {
    return set6((const uint64_t*)&ip6be, port);
  }

  // set individual fields
  void set_port(int port);
  int set_ip(const PortIp& source);
  int set_ip(const char* ip, bool ip4only=false); // returns 4 or 6
  int set_ip4(Ip4be ip4be);
  int set_ip6(const uint64_t* ip6be);
  int set_ip4(const in_addr& ip4be) {
    return set_ip4((Ip4be)ip4be.s_addr);
  }
  int set_ip6(const in6_addr& ip6be) {
    return set_ip6((const uint64_t*)&ip6be);
  }

  bool operator==(PortIp& src) noexcept;
  bool operator!=(PortIp& src) noexcept { return !(*this==src); }

  bool is_blank() noexcept { return ( is_blank_ip() && port()==0 ); }

  bool is_ip4() noexcept { return ( ipv() == 4 ); }
  bool is_ip6() noexcept { return ( ipv() == 6 ); }
  bool is_blank_ip() noexcept;

  bool equals_ip4(Ip4be ip4be) noexcept;
  bool equals_ip6(const uint64_t* ip6be) noexcept;
  bool equals_ip(const in_addr& ip4) noexcept {
    return equals_ip4((Ip4be)ip4.s_addr);
  }
  bool equals_ip(const in6_addr& ip6) noexcept {
    return equals_ip6((const uint64_t*)&ip6);
  }
};

//================================================================
// IpSocket (Handle)
//----------------------------------------------------------------

// socket for IP ucast|bcast|mcast

// kernel socket buffer "typical" sizes on linux:
//
//  udp - may require manual increase if high speed!
//   send: /proc/sys/net/core/wmem_max [max: 208K]
//   recv: /proc/sys/net/core/rmem_max [max: 208K]
//
//  tcp - kernel will auto tune
//   send: /proc/sys/net/ipv4/tcp_wmem [auto: 4K..4M]
//   recv: /proc/sys/net/ipv4/tcp_rmem [auto: 4K..6M]

struct IpSockets;

struct IpSocket : Handle
{
  PortIp local;  // pubrd: local port + optional local ip (local nic)
  PortIp remote; // pubrd: ucast address | bcast net | mcast group

  // optionally part of wait list
  IpSockets* list = nullptr;
  int pos = NULL_POS;

  int want_sbuf_size = NULL_SIZE;    // pubrd: default (auto) or fixed size
  int want_rbuf_size = NULL_SIZE;    // pubrd: default (auto) or fixed size

  int64_t last_sent = NULL_TIME;     // pubrd: coarse mono [ms]
  int64_t bytes_sent = 0;            // pubrw: send statistics

  int64_t last_received = NULL_TIME; // pubrd: coarse mono [ms]
  int64_t bytes_received = 0;        // pubrw: receive statistics

  void preset_sbuf_size(int size) { this->want_sbuf_size = size; }
  void preset_rbuf_size(int size) { this->want_rbuf_size = size; }

  // helper
  // take ownership of raw handle
  // will ensure non-blocking and establish send/receive buffer sizes
  void set(int handle);

  // ensure not part of any wait list
  bool unlist() noexcept;

  virtual void close() noexcept override;

  // get effective randomized (open) or selected (bind) local port
  int get_local_port();

  // get effective buffer size after set()
  int get_sbuf_size();
  int get_rbuf_size();

  // wait for input
  // combine with try_receive (non-blocking)
  // will return true immediately on input event
  // or false after short timeout (max 1 second)
  // timeout capped within 1:1000 [ms]
  bool wait(int wait_timeout);

  // remote.address() if not blank, otherwise local.address()
  string address() noexcept;

protected:
  // wait for output buffer to be ready (used by try_send)
  // timeout capped at 1000 [ms]
  bool wait_to_send(int wait_timeout);

  // internal helpers
  void enable_non_blocking();
  void set_tcp_keep_alive(int sec); // disable with 0
  void set_sbuf_size(int bytes); // request new size
  void set_rbuf_size(int bytes); // request new size
};

//================================================================
// IpSockets (container)
//----------------------------------------------------------------

// wait for multiple sockets simultaneously

// TODO: use linux epoll instead (more efficient)

struct IpSockets
{
  IpSockets() {}
  IpSockets(int space) { reserve(space); }
  ~IpSockets() noexcept;

  // will be unlisted first if necessary
  bool add(IpSocket* ipsocket);

  // remove closed sockets
  bool prune();

  // wait for input on listed sockets
  // will return true immediately on input event
  // or false after short timeout (max 1 second)
  // wait timeout capped within 1:1000 [ms]
  bool wait(int wait_timeout);

  // expose list
  int size() { return this->sockets.size(); }
  int space() { return this->sockets.space(); }
  void reserve(int space) {
    this->sockets.reserve(space);
    this->pfds.reserve(space);
  }
  RefVec<IpSocket>::iterator begin() { return sockets.begin(); }
  RefVec<IpSocket>::iterator end() { return sockets.end(); }

  // helper called by IpSocket::set()
  static void set(IpSockets* list, int pos, int handle);

  // helper called by IpSocket::unlist()
  static void unlist(IpSockets* list, int pos) noexcept;

  // helper called by Udp/Tcp try_receive()
  static bool check(IpSockets* list, int pos) {
    pollfd* poller = list->pfds.data() + pos;
    if( poller->revents > 0 ) {
      poller->revents = 0; // reset
      return true;
    }
    else {
      return false;
    }
  }

protected:
  // IpSocket array (pos)
  RefVec<IpSocket> sockets;

  // pollfd array (pos)
  ValVec<pollfd> pfds;
};

//================================================================
// Nic
//----------------------------------------------------------------

// TODO: Nic::find(label_or_ip)

//================================================================
// UdpSender (IpSocket)
//----------------------------------------------------------------

// datagram sender/receiver (ucast|bcast|mcast)
// atomic operation (one datagram or no datagram)
// send non-blocking or with stall timeout
// receive non-blocking (can be paired with short-timeout wait)
// be aware: datagrams can be silently lost (packet loss)

#define UDP_PAYLOAD_MAX (65504)

struct UdpSocket : IpSocket
{
  int payload_max; // pubrd: max packet payload size

  UdpSocket() { this->payload_max = UDP_PAYLOAD_MAX; }
  UdpSocket(int payload_max) { this->payload_max = payload_max; }

  void set_payload_max(int payload_max) { this->payload_max = payload_max; }

  // generic open/bind
  void set_local(const PortIp& local) { close(); this->local=local; }
  void set_remote(const PortIp& remote) { this->remote=remote; }
  void init(bool reuse=false);
  void init(const char* remote_ip, int remote_port, const char* local_ip, int local_port, bool reuse=false, bool ip4only=false);

  // open unicast (bind to random local port)
  void open(const char* remote_ip=nullptr, int remote_port=0) {
    init(remote_ip, remote_port, nullptr, 0);
  }

  // bind unicast (bind to fixed local port)
  void bind(int local_port, const char* local_ip=nullptr, bool reuse=false) {
    init(nullptr, 0, local_ip, local_port, reuse);
  }

  // open IPv4-broadcast sender (bind to random local port)
  void open_bcast(const char* bcast_ip4, int remote_port);

  // bind IPv4-broadcast receiver (bind to fixed local port)
  void bind_bcast(int local_port, const char* local_ip=nullptr, bool reuse=false) {
    init(nullptr, 0, local_ip, local_port, reuse, true);
  }

  // open multicast sender
  void open_mcast(const char* group_ip, int remote_port, const char* local_ip=nullptr, bool loop=false, int ttl=1);

  // bind multicast receiver
  void bind_mcast(const char* group_ip, int local_port, const char* local_ip=nullptr, bool reuse=false);

  // try send packet to ip:port, returns false if not ready to send
  // wait timeout capped at 1000 [ms]
  bool try_send(const char* data, int bytes, PortIp& sendto, int wait_timeout=0);
  bool try_send(const char* data, int bytes, int wait_timeout=0) {
    return try_send(data, bytes, this->remote, wait_timeout);
  }

  // send packet to ip:port with stall timeout
  // stall timeout capped within 0.1:60 [sec]
  void send(const char* data, int bytes, PortIp& sendto, int stall_timeout=NULL_TIME);
  void send(const char* data, int bytes, int stall_timeout=NULL_TIME) {
    send(data, bytes, this->remote, stall_timeout);
  }

  // receive into buffer (set)
  // returns true if packet was received (including empty payload)
  // will throw if buffer is too small for packet (packet will be lost)
  bool try_receive(Buffer* buffer, PortIp* remote_ptr=nullptr);
};

//================================================================
// TcpSocket (IpSocket)
//----------------------------------------------------------------

// IPv4/v6 TCP end-point (caller or callee)
// non-blocking with configurable send timeout

struct TcpSocket : IpSocket
{
  Buffer buffer; // pubrw: dedicated receive buffer
  int chunk_max; // pubrd: max try_send/receive per call

  TcpSocket() { this->chunk_max = TCP_CHUNK_MAX; }
  TcpSocket(int chunk_max) { this->chunk_max = chunk_max; }

  void set_chunk_max(int chunk_max) { this->chunk_max = chunk_max; }

  // used by init and TcpService::accept
  void set(int handle, int keep_alive_sec) {
    IpSocket::set(handle);
    set_tcp_keep_alive(keep_alive_sec);
  }

  // [caller]
  // make ready to connect (open without connect)
  void init(const char* remote_ip, int port, int keep_alive_sec=NULL_TIME);

  // [caller]
  // try to connect (non-blocking)
  // returns true if successful, false if in progress
  bool try_connect();

  // [caller]
  // connect succefully or fail with exception
  // stall timeout capped within 0.1:60 [sec]
  void connect(int stall_timeout=NULL_TIME);

  // [caller/callee]
  // send none/some/all (non-blocking)
  // chunk_max (1 MiB) per call (cutoff)
  // wait timeout capped at 1000 [ms]
  // returns bytes actually sent or -1 if peer disconnected!
  // if allbytes != returned:
  // => left to send = allbytes - returned
  // if min(allbytes,chunk_max) != returned:
  // => system's send buffer is full (push back)
  int try_send(const char* data, int allbytes, int wait_timeout=0);

  // [caller/callee]
  // send all bytes or fail with exception
  // stall timeout capped within 0.1:60 [sec]
  void send(const char* data, int bytes, int stall_timeout=NULL_TIME);

  // [caller/callee]
  // receive from port (non-blocking)
  // received data will be added to internal buffer (incoming stream)
  // cut off at chunk_max per call
  // returns bytes received, 0 if nothing or -1 if peer disconnected!
  // calling try_receive() is the way to detect a clean "disconnect"
  int try_receive();
};

//================================================================
// TcpService (IpSocket)
//----------------------------------------------------------------

// IPv4/v6 TCP connection manager
// non-blocking

struct TcpService : IpSocket
{
  int keep_alive_sec;

  TcpService() { keep_alive_sec = (IO_TIMEOUT*2)/SECOND; }
  //~TcpService() noexcept override;

  void set(int handle) {
    Handle::set(handle);
    enable_non_blocking();
  }

  // make ready to accept connections on port (open service)
  void bind(int port, const char* local_ip, int queue_max, int keep_alive_sec=NULL_TIME);

  // non-blocking, returns true if new connection (connected callee)
  // callee inherits the local address from service,
  // use callee->get_local_port() to get the actual local port
  bool try_accept(TcpSocket* callee);
};

//##############################################################//
//                                                              //
//                         S T O R A G E                        //
//                                                              //
//##############################################################//

//================================================================
// File system functions
//----------------------------------------------------------------

string get_current_dir();

void set_current_dir(const char* path);

// step thru path levels until zero termination
// initial "/" or "<proto>:/[/]" will count as an entry
// returns false when no more entries
bool step_path(const char* path, Subref& level);

// convert to absolute path if relative
string abs_path(const char* anypath, const char* current_dir=nullptr);

// convert to absolute "system" path (follow symlinks)
string true_path(const char* anypath);

//----------------------------------------------------------------

// add path to base path (linux path format)
string join_path(const char* base, const char* abs_or_rel);

//----------------------------------------------------------------

// extract parent from path (dirname)
string parent_path(const char* path);

// extract leaf from path (basename)
string path_leaf(const char* path);

// extract extension from path
string path_ext(const char* path, bool include_dot);

//----------------------------------------------------------------

// returns PATH_<type> if exists or PATH_NONE (0) if not
// will return PATH_UNKNOWN if error and throw_on_error=false
int get_path_type(const char* path, bool follow_links=true, bool throw_on_error=false);

// probe if path exists (and matches type), returns true/false
bool path_exists(const char* path, int path_type=PATH_ANY);

//----------------------------------------------------------------

// check if path exists and is a directory, throws error if not
void check_dir(const char* path);

// create directory if it does not exist, returns true if created
bool ensure_dir(const char* path);

// delete directory+content if it exists, returns true if deleted
bool ensure_no_dir(const char* path, int content_type=PATH_FILE);

// delete directory content if it exists, returns true if deleted
bool ensure_empty_dir(const char* path, int content_type=PATH_FILE);

//----------------------------------------------------------------

// create symlink, remove old symlink first if exists
// returns true if new or changed
bool ensure_link(const char* target_path, const char* path);

// remove symlink if it exists, returns true if removed
bool ensure_no_link(const char* path);

//----------------------------------------------------------------

// check if path exists and is a file (of type), throws error if not
void check_file(const char* path);

// delete file if it exists, returns true if deleted
bool ensure_no_file(const char* path);

// get file size [bytes]
int64_t get_file_size(const char* path);

// get last modified [milliseconds since 1970]
int64_t get_file_mtime(const char* path);

// load file into buffer, returns bytes loaded
int load_file(const char* path, Buffer* buffer, bool add=false);

// load first line of file (STRINGF_MAX)
string load_first_line(const char* path);

// load key=value file
// values can be quoted with "" or ''
// # starts comment
ValMap<string,string> load_property_file(const char* path);

// returns true if file was created, false if overwritten
bool save_file(const char* path, const char* data, int size);

void rename_file(const char* src, const char* dest);
void copy_file(const char* src, const char* dest);

//----------------------------------------------------------------

string get_current_user();

// 4 fields * 3 bits each (4+2+1)
// - Special: setuid (4) + setgid (2) + sticky (1)
// - Owner:   read (4) + write (2) + list/execute (1)
// - Group:   read (4) + write (2) + list/execute (1)
// - Others:  read (4) + write (2) + list/execute (1)
// (1) list permission if directory or execute permission if file
int get_path_mode(const char* path);

string get_path_owner(const char* path);
string get_path_group(const char* path);

//void change_path_mode(const char* path, int mode);
//void change_path_owner(const char* path, const char* owner);
//void change_path_group(const char* path, const char* group);

void get_disk_info(const char* mount_path, int64_t* free_bytes, int64_t* disk_bytes);

//================================================================
// File (Handle)
//----------------------------------------------------------------

struct File : Handle
{
  string path; // pubrd

  File() { }
  File(const char* path) { this->path = path; }
  //~File() noexcept override;

  void set(const char* path) {
    close();
    if( this->path != path ) {
      this->path = path;
    }
  }

  // open read-only
  // will throw if file is missing
  void open_r();
  void open_r(const char* path) {
    set(path);
    open_r();
  }

  // open for append (write only)
  void open_a(bool create=true);
  void open_a(const char* path, bool create=true) {
    set(path);
    open_a(create);
  }

  // open for write
  // create=true will truncate if file exists
  // create=false will throw if file is missing
  void open_w(bool create=true);
  void open_w(const char* path, bool create=true) {
    set(path);
    open_w(create);
  }
  void open_rw(bool create=true);
  void open_rw(const char* path, bool create=true) {
    set(path);
    open_rw(create);
  }

  // set byte position
  void seek(int64_t pos);

  // seek to end and return end position
  int64_t end();

  // returns byte size
  int read(char* data, int space);

  // returns bytes added
  int read(Buffer* buffer, bool add=false);

  // write bytes
  void write(const char* data, int size);

  // commit to disk
  void flush() {
    if( this->handle >= 0 ) {
      ::fsync(this->handle);
    }
  }
};

//================================================================
// DirWalker (Handle)
//----------------------------------------------------------------

struct DirWalker : Handle
{
  string path; // pubrd

  //DirWalker() { }
  //~DirWalker() noexcept override;

  void open(const char* path);
  virtual void close() noexcept override;

  // name never equals "." or ".."
  // close() + returns false if no more entries

  bool step(string* name_out, int type=PATH_ANY, int* is_type=nullptr) {
    return find(name_out, nullptr, type, is_type);
  }

  bool find(string* name_out, const char* contains, int type=PATH_ANY, int* is_type=nullptr);

protected:
  DIR* stream = nullptr;
  dirent* entry = nullptr; // pointing into stream
};

//##############################################################//

#endif // MILLICPP_H
