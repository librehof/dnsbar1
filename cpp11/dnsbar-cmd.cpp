#include "dnsbar.h"

/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

void DnsBar::run_script(const char* type, const char* script)
{
  if( str_blank(script) ) {
    return;
  }
  // execute
  detailf("> %s", script);
  try {
    // clear
    ensure_no_script_log(type);

    // run
    ValVec<string> outerr;
    int status = shell(script, &outerr);
    string name = type;
    if( status > 0 ) {
      log_error(script);
      this->main_error = script;
      name += ".err";
    }
    else {
      name += ".out";
    }

    // log
    if( !report_dir.empty() ) {
      this->report.clear();
      this->report.add(outerr);
      save_report(name.c_str());
    }
  }
  catch(const std::exception& e) {
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::log_list_diff(const char* havepath, const char* wantpath)
{
  if( this->log_dir.empty() ) {
    return;
  }
  if( this->diff_cmd.empty() ) {
    return;
  }
  if( !path_exists(havepath,PATH_FILE) ) {
    return;
  }
  if( !path_exists(wantpath,PATH_FILE) ) {
    return;
  }
  // cmd
  string quoted_have = stringf("'%s'", havepath);
  string quoted_want = stringf("'%s'", wantpath);
  string cmd = stringf(this->diff_cmd.c_str(), quoted_have.c_str(), quoted_want.c_str(),
    BLANK, BLANK, BLANK, BLANK, BLANK);
  // execute
  detailf("> %s", cmd.c_str());
  try {
    // run
    ValVec<string> outerr;
    int status = shell(cmd.c_str(), &outerr);
    if( status > 1 ) {
      // error
      log_error(cmd.c_str());
      this->sync_error = cmd.c_str();
    }
    else if( !outerr.empty() ) {
      // append diff to daily diff log
      Buffer output;
      int64_t now = get_utc();
      //string time = format_time("%Y-%m-%d %H:%M:%S UTC", now);
      //output.addn();
      //output.addfn("# %s", time.c_str());
      output.addn();
      output.add(outerr);
      string key = format_host_time("%m-%d", now);
      string name = stringf("dnsbar-%s.diff", key.c_str());
      string path = join_path(this->log_dir.c_str(), name.c_str());
      File logfile(path.c_str());
      logfile.open_a();
      logfile.write(output.data, output.size);
      // remote
      push_remote_log_file(path.c_str(), name.c_str());
    }
  }
  catch(const std::exception& e) {
    log_error(e);
  }
}

//----------------------------------------------------------------
/*
void DnsBar::nft_reload()
{
  if( this->nft.empty() ) {
    return;
  }
  if( this->nft_conf.empty() ) {
    return;
  }
  // cmd
  string cmd = stringf("%s -f %s", this->nft.c_str(), this->nft_conf.c_str());
  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str(), 1, 0);
  }
  catch(const std::exception& e) {
    log_error(e);
  }
}
*/
//----------------------------------------------------------------

void DnsBar::nft4_clear()
{
  if( this->nft_clear == 0 ) {
    return;
  }
  if( this->nft.empty() ) {
    return;
  }
  if( this->nft4_family.empty() ) {
    return;
  }
  if( this->nft4_table.empty() ) {
    return;
  }
  if( this->nft4_set.empty() ) {
    return;
  }
  // cmd
  string cmd = stringf("%s flush set %s %s %s", this->nft.c_str(),
    this->nft4_family.c_str(), this->nft4_table.c_str(), this->nft4_set.c_str());
  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str(), 1, 0);
  }
  catch(const std::exception& e) {
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::nft6_clear()
{
  if( this->nft_clear == 0 ) {
    return;
  }
  if( this->nft.empty() ) {
    return;
  }
  if( this->nft6_family.empty() ) {
    return;
  }
  if( this->nft6_table.empty() ) {
    return;
  }
  if( this->nft6_set.empty() ) {
    return;
  }
  // cmd
  string cmd = stringf("%s flush set %s %s %s", this->nft.c_str(),
    this->nft6_family.c_str(), this->nft6_table.c_str(), this->nft6_set.c_str());
  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str(), 1, 0);
  }
  catch(const std::exception& e) {
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::nft4_add(Ip4be ip4be)
{
  if( this->nft.empty() ) {
    return;
  }
  if( this->nft4_family.empty() ) {
    return;
  }
  if( this->nft4_table.empty() ) {
    return;
  }
  if( this->nft4_set.empty() ) {
    return;
  }
  if( this->nft_errors >= 100 ) {
    if( this->nft_errors == 100 ) {
      this->nft_errors++;
      log_error("Too many nft errors...");
    }
    return;
  }
  // cmd
  PortIp portip;
  portip.set_ip4(ip4be);
  char ipstr[IPSTR_SPACE];
  portip.get_ip(ipstr, IPSTR_SPACE);
  string cmd = stringf("%s add element %s %s %s \"{ %s }\"", this->nft.c_str(),
    this->nft4_family.c_str(), this->nft4_table.c_str(), this->nft4_set.c_str(), ipstr);
  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str(), 1, 0);
  }
  catch(const std::exception& e) {
    this->nft_errors++;
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::nft6_add(const uint64_t* ip6be)
{
  if( this->nft.empty() ) {
    return;
  }
  if( this->nft6_family.empty() ) {
    return;
  }
  if( this->nft6_table.empty() ) {
    return;
  }
  if( this->nft6_set.empty() ) {
    return;
  }
  if( this->nft_errors >= 100 ) {
    if( this->nft_errors == 100 ) {
      this->nft_errors++;
      log_error("Too many nft errors...");
    }
    return;
  }
  // cmd
  PortIp portip;
  portip.set_ip6(ip6be);
  char ipstr[IPSTR_SPACE];
  portip.get_ip(ipstr, IPSTR_SPACE);
  string cmd = stringf("%s add element %s %s %s \"{ %s }\"", this->nft.c_str(),
    this->nft6_family.c_str(), this->nft6_table.c_str(), this->nft6_set.c_str(), ipstr);
  // execute
  try {
    shell(cmd.c_str(), 1, 0);
  }
  catch(const std::exception& e) {
    this->nft_errors++;
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::ensure_remote_dir(const char* destpath)
{
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return;
  }
  if( this->remote_errors >= 100 ) {
    return;
  }

  // destpath
  if( str_blank(destpath) ) {
    this->remote_errors++;
    log_error("ensure_remote_dir with blank argument(s)");
    return;
  }

  // cmd
  string cmd = stringf("ssh '%s@%s' mkdir -p '%s'",
    this->remote_user.c_str(), this->remote_host.c_str(),
    destpath);

  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str());
  }
  catch(const std::exception& e) {
    this->remote_errors++;
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::push_remote_file(const char* srcpath, const char* destname)
{
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return;
  }
  if( this->remote_dir.empty() ) {
    return;
  }

  if( this->remote_errors >= 100 ) {
    if( this->remote_errors == 100 ) {
      this->remote_errors++;
      log_error("Too many remote errors...");
    }
    return;
  }

  // destpath
  string dest = path_leaf(destname);
  if( dest.empty() || str_blank(srcpath) ) {
    this->remote_errors++;
    log_error("push_remote_file with blank argument(s)");
    return;
  }
  string destpath = join_path(this->remote_dir.c_str(), dest.c_str());

  // cmd
  string cmd = stringf("scp '%s' '%s@%s:%s'",
    srcpath,
    this->remote_user.c_str(), this->remote_host.c_str(), destpath.c_str());

  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str());
  }
  catch(const std::exception& e) {
    this->remote_errors++;
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::push_remote_log_file(const char* srcpath, const char* destname)
{
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return;
  }
  if( this->remote_log_dir.empty() ) {
    return;
  }

  if( this->remote_errors >= 100 ) {
    if( this->remote_errors == 100 ) {
      this->remote_errors++;
      log_error("Too many remote errors...");
    }
    return;
  }

  // destpath
  string dest = path_leaf(destname);
  if( dest.empty() || str_blank(srcpath) ) {
    this->remote_errors++;
    log_error("push_remote_file with blank argument(s)");
    return;
  }
  string destpath = join_path(this->remote_log_dir.c_str(), dest.c_str());

  // cmd
  string cmd = stringf("scp '%s' '%s@%s:%s'",
    srcpath,
    this->remote_user.c_str(), this->remote_host.c_str(), destpath.c_str());

  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str());
  }
  catch(const std::exception& e) {
    this->remote_errors++;
    log_error(e);
  }
}

//----------------------------------------------------------------

void DnsBar::try_pull_remote_file(const char* srcname, const char* destpath)
{
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return;
  }
  if( this->remote_dir.empty() ) {
    return;
  }
  if( this->remote_errors >= 100 ) {
    return;
  }

  string src = path_leaf(srcname);
  if( src.empty() || str_blank(destpath) ) {
    this->remote_errors++;
    log_error("try_pull_remote_file with blank argument(s)");
    return;
  }
  string srcpath = join_path(this->remote_dir.c_str(), src.c_str());

  try {
    // cmd
    string cmd = stringf("scp '%s@%s:%s' '%s'",
      this->remote_user.c_str(), this->remote_host.c_str(), srcpath.c_str(),
      destpath);
    // execute
    detailf("> %s", cmd.c_str());
    shell(cmd.c_str());
  }
  catch(...) {
    // ignore
  }
}

//----------------------------------------------------------------

void DnsBar::try_move_remote_file(const char* srcname, const char* destname)
{
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return;
  }
  if( this->remote_dir.empty() ) {
    return;
  }
  if( this->remote_errors >= 100 ) {
    return;
  }

  string src = path_leaf(srcname);
  if( src.empty() ) {
    this->remote_errors++;
    log_error("try_move_remote_file with blank argument(s)");
    return;
  }
  string srcpath = join_path(this->remote_dir.c_str(), src.c_str());

  // destpath
  string dest = path_leaf(destname);
  if( dest.empty() ) {
    this->remote_errors++;
    log_error("try_move_remote_file with blank argument(s)");
    return;
  }
  string destpath = join_path(this->remote_dir.c_str(), dest.c_str());

  try {
    // cmd
    string cmd = stringf("ssh '%s@%s' mv '%s' '%s'",
      this->remote_user.c_str(), this->remote_host.c_str(),
      srcpath.c_str(),
      destpath.c_str());
    // execute
    detailf("> %s", cmd.c_str());
    shell(cmd.c_str());
  }
  catch(...) {
    // ignore
  }
}

//----------------------------------------------------------------

void DnsBar::try_delete_remote_file(const char* destname)
{
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return;
  }
  if( this->remote_dir.empty() ) {
    return;
  }
  if( this->remote_errors >= 100 ) {
    return;
  }

  // destpath
  string dest = path_leaf(destname);
  if( dest.empty() ) {
    this->remote_errors++;
    log_error("try_delete_remote_file with blank argument(s)");
    return;
  }
  string destpath = join_path(this->remote_dir.c_str(), dest.c_str());

  try {
    // cmd
    string cmd = stringf("ssh '%s@%s' rm -f '%s'",
      this->remote_user.c_str(), this->remote_host.c_str(),
      destpath.c_str());
    // execute
    detailf("> %s", cmd.c_str());
    shell(cmd.c_str());
  }
  catch(...) {
    // ignore
  }
}

//----------------------------------------------------------------

void DnsBar::touch_remote_file(const char* destname)
{
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return;
  }
  if( this->remote_dir.empty() ) {
    return;
  }
  if( this->remote_errors >= 100 ) {
    return;
  }

  // destpath
  string dest = path_leaf(destname);
  if( dest.empty() ) {
    this->remote_errors++;
    log_error("try_touch_remote_file with blank argument(s)");
    return;
  }
  string destpath = join_path(this->remote_dir.c_str(), dest.c_str());

  // cmd
  string cmd = stringf("ssh '%s@%s' touch '%s'",
    this->remote_user.c_str(), this->remote_host.c_str(),
    destpath.c_str());

  // execute
  detailf("> %s", cmd.c_str());
  try {
    shell(cmd.c_str());
  }
  catch(const std::exception& e) {
    this->remote_errors++;
    log_error(e);
  }
}

//----------------------------------------------------------------
