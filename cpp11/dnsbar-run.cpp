#include "dnsbar.h"
#include "dnsbar-module.h"
#include "repo.h"


/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

void DnsBar::load_conf(
  const ValMap<string,string>& conf,
  const char* default_data_dir,
  const char* default_log_dir)
{
  int number;
  string text;

  this->started = get_utc();

  // black
  number = str_to_int(conf.get("black","0").c_str());
  if( number > 0 ) {
    this->black = 1;
  }
  else {
    this->black = 0;
  }

  // verbose
  number = str_to_int(conf.get("verbose","0").c_str());
  number = std::max(number, 0);
  number = std::min(number, 2);
  this->verbose = number;
  if( this->verbose > 0 ) {
    LOG_LEVEL = LOG_DETAIL;
  }
  else {
    LOG_LEVEL = LOG_NOTE;
  }

  // test
  number = str_to_int(conf.get("test","0").c_str());
  number = std::max(number, 0);
  number = std::min(number, 2);
  this->test = number;

  // stat_sec and LOG_LIMIT
  number = str_to_int(conf.get("stat_sec","3600").c_str());
  number = std::max(number, 1);
  number = std::min(number, 24*60*60);
  this->stat_period = FROM_SECOND(number);
  number *= 2;
  if( number < 50 ) {
    number = 50;
  }
  if( number < 100 ) {
    number *= 2;
  }
  if( LOG_LEVEL >= LOG_DETAIL ) {
    number *= 10;
  }
  LOG_LIMIT = number;

  // sync_sec
  number = str_to_int(conf.get("sync_sec","45").c_str());
  number = std::max(number, 1);
  number = std::min(number, 24*60*60);
  this->sync_period = FROM_SECOND(number);

  // dirs
  this->var_dir = conf.get("var_dir", default_data_dir);
  this->var_dir = abs_path(this->var_dir.c_str());
  this->report_dir = conf.get("report_dir", default_data_dir);
  this->report_dir = abs_path(this->report_dir.c_str());
  this->log_dir = conf.get("log_dir", default_log_dir);
  this->log_dir = abs_path(this->log_dir.c_str());

  // incoming
  text = conf.get("incoming_ip","127.0.0.1");
  number = str_to_int(conf.get("incoming_port","53").c_str());
  this->incoming.set(text.c_str(), number);
  number = str_to_int(conf.get("incoming_max","1000").c_str());
  number = std::max(number, 10);
  number = std::min(number, 30000);
  this->incoming_max = number;

  // outgoing
  text = conf.get("outgoing_ip", "");
  number = str_to_int(conf.get("outgoing_port","53").c_str());
  this->outgoing.set(text.c_str(), number);

  // udp_buf
  number = str_to_int(conf.get("udp_buf","0").c_str());
  number = std::max(number, -1);
  number = std::min(number, 1000);
  this->udp_buf = number;

  // block_reports
  number = str_to_int(conf.get("block_reports","5").c_str());
  number = std::max(number, 0);
  number = std::min(number, 3600);
  this->block_reports = number;

  // ttl_min_sec
  number = str_to_int(conf.get("ttl_min_sec","1").c_str());
  number = std::max(number, 1);
  number = std::min(number, 10);
  this->ttl_min_sec = (uint32_t)number;

  // ttl_max_sec
  number = str_to_int(conf.get("ttl_max_sec","240").c_str());
  number = std::max(number, (int)this->ttl_min_sec);
  number = std::min(number, 600);
  this->ttl_max_sec = (uint32_t)number;

  // restart_hour
  number = str_to_int(conf.get("restart_hour","-1").c_str());
  number = std::max(number, -1);
  number = std::min(number, 23);
  this->restart_hour = number;

  // restart_internal
  number = str_to_int(conf.get("restart_internal","1").c_str());
  if( number > 0 ) {
    APP_INTERNAL_RESTART = true;
  }
  else {
    APP_INTERNAL_RESTART = false;
  }

  // event scripts
  this->start_script = conf.get("start_script", "");
  this->restart_script = conf.get("restart_script", "");
  this->error_restart_script = conf.get("error_restart_script", "");
  this->stop_script = conf.get("stop_script", "");

  // diff_cmd
  this->diff_cmd = conf.get("diff_cmd", BLANK);

  // nft
  this->nft = conf.get("nft", "/sbin/nft");

  // nft_clear
  number = str_to_int(conf.get("nft_clear","0").c_str());
  number = std::max(number, 0);
  number = std::min(number, 1);
  this->nft_clear = number;

  // nftables ip4 set
  this->nft4_family = conf.get("nft4_family", BLANK);
  this->nft4_table = conf.get("nft4_table", "filter");
  this->nft4_set = conf.get("nft4_set", "white4out");

  // nftables ip6 set
  this->nft6_family = conf.get("nft6_family", BLANK);
  this->nft6_table = conf.get("nft6_table", "filter");
  this->nft6_set = conf.get("nft6_set", "white6out");

  // remote list/report sync (ssh/scp)
  this->remote_user = conf.get("remote_user", "root");
  this->remote_host = conf.get("remote_host", "");
  this->remote_dir = conf.get("remote_dir", "/var/lib/dnsbar");
  this->remote_log_dir = conf.get("remote_log_dir", BLANK);

  // ip4nets: ip4,mask4,ip4,mask4,...
  for(auto entry : conf) {
    const string& key = entry.first;
    // white net
    if( str_starts_with(key.c_str(),"white_") && str_ends_with(key.c_str(),intsize(key),"_net") ) {
      if( this->black ) {
        errorf("Skipping white net: %s", key.c_str());
      }
      else {
        string mask_key(key.c_str(),key.size()-4);
        mask_key += "_mask";
        string net = entry.second;
        string mask = conf.get(mask_key.c_str(),"255.255.255.0");
        PortIp netip(net.c_str(), 0);
        PortIp netmask(mask.c_str(), 0);
        this->ip4nets.add(netip.addr4.sin_addr.s_addr);
        this->ip4nets.add(netmask.addr4.sin_addr.s_addr);
      }
    }
    // black net
    if( str_starts_with(key.c_str(),"black_") && str_ends_with(key.c_str(),intsize(key),"_net") ) {
      if( !this->black ) {
        errorf("Skipping black net: %s", key.c_str());
      }
      else {
        string mask_key(key.c_str(),key.size()-4);
        mask_key += "_mask";
        string net = entry.second;
        string mask = conf.get(mask_key.c_str(),"255.255.255.0");
        PortIp netip(net.c_str(), 0);
        PortIp netmask(mask.c_str(), 0);
        this->ip4nets.add(netip.addr4.sin_addr.s_addr);
        this->ip4nets.add(netmask.addr4.sin_addr.s_addr);
      }
    }
  }

  // ensure directories
  if( !this->var_dir.empty() ) {
    ensure_dir(this->var_dir.c_str());
  }
  if( !this->report_dir.empty() ) {
    ensure_dir(this->report_dir.c_str());
  }
  if( !this->log_dir.empty() ) {
    ensure_dir(this->log_dir.c_str());
  }
  if( !this->remote_dir.empty() ) {
    ensure_remote_dir(this->remote_dir.c_str());
  }
  if( !this->remote_log_dir.empty() ) {
    ensure_remote_dir(this->remote_log_dir.c_str());
  }

  // load static lists
  this->sync_error.clear();
  this->preload_lists(this->conf_dir.c_str());
  this->matchlist0 = move(this->matchlistN);
  this->wildlist0 = move(this->wildlistN);
  this->neglist0 = move(this->neglistN);
  this->ip4list0 = move(this->ip4listN);
  if( !this->sync_error.empty() ) {
    log_warning("Review static list(s)");
    this->main_error = this->sync_error;
    this->sync_error.clear();
  }
}

//----------------------------------------------------------------

void DnsBar::load_startup_lists()
{
  // load dynamic lists (apply even if error)
  this->sync_error.clear();
  try {
    preload_lists(this->var_dir.c_str());
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
  if( !this->sync_error.empty() ) {
    log_warning("Review dynamic list(s)");
    this->main_error = this->sync_error;
    this->sync_error.clear();
  }
}

//----------------------------------------------------------------

void DnsBar::main_loop()
{
  // ping dns
  try {
    string out_ip = this->outgoing.ip();
    string cmd = stringf("ping -q -w2 -c1 '%s'", out_ip.c_str());
    shell(cmd.c_str(), 1, false);
  }
  catch(const std::exception& e) {
    log_error(e);
  }

  // nftables clear
  try {
    nft4_clear();
    nft6_clear();
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }

  IpSockets sockets(1000);
  RefSet<UdpDnsCall> udps;
  //RefSet<TcpDnsCall> tcps;

  // register service!
  Ref<UdpSocket> udp_service = new UdpSocket;
  if( this->udp_buf == 0 ) {
    // auto adjust receive buffer
    if( this->incoming_max >= 1500 ) {
      udp_service->preset_rbuf_size(400*KIB);
    }
    else if( this->incoming_max >= 500 ) {
      udp_service->preset_rbuf_size(200*KIB);
    }
    else if( this->incoming_max >= 100 ) {
      udp_service->preset_rbuf_size(100*KIB);
    }
  }
  else if( this->udp_buf > 0 ) {
    // fixed size receive buffer
    udp_service->preset_rbuf_size(this->udp_buf*KIB);
  }
  udp_service->bind(this->incoming.port(), this->incoming.ip().c_str());
  sockets.add(udp_service);

  // shared
  Buffer dns_packet(UDP_PAYLOAD_MAX);
  PortIp dns_from;

  Ref<TcpService> tcp_service = new TcpService;
  tcp_service->bind(this->incoming.port(), this->incoming.ip().c_str(), 1); // not used!
  //tcp_service->bind(this->incoming.port(), this->incoming.ip().c_str(), this->incoming_max>>4);
  //sockets.add(tcp_service);

  // startup info
  int rbuf_size = udp_service->get_rbuf_size();
  notef("UDP total rbuf = %d [KiB]", rbuf_size/KIB);
  notef("Listening on %s", incoming.address().c_str());

  for(;;) {
    // stats
    this->sockets_high = std::max(this->sockets_high, (int)sockets.size());
    //this->udps_max = std::max(this->udps_max, (int)udps.size());
    //this->tcps_max = std::max(this->tcps_max, (int)tcps.size());

    app_check();
    sockets.prune();

    // wait for traffic

    //detailf("release");
    {
      Release release(&this->mutex);
      if( sockets.wait(500*MILLI) ) {
        detailf("receiving...");
      }
    }
    //detailf("hold");

    // udp service

    if( udp_service->try_receive(&dns_packet,&dns_from) ) {
      // incoming udp
      if( dns_from.is_ip6() ) {
        // expected to be IPv4 LAN hosts only => drop udp!
        this->drop_count++;
      }
      else if( udps.size() > this->incoming_max ) {
        // overloaded => drop udp!
        this->drop_count++;
      }
      else {
        // forward udp!
        this->call_count++;
        Ref<UdpDnsCall> udp = new UdpDnsCall;
        udp->incoming = dns_from;
        udp->outgoing = new UdpSocket;
        udp->outgoing->init();
        sockets.add(udp->outgoing);
        udps.add(udp);
        udp->outgoing->send(dns_packet.data, dns_packet.size, this->outgoing);
        short_sleep(0); // tiny sleep to avoid packet loss on other side
      }
    }

    // udp calls

    for(auto i=udps.begin(); i!=udps.end();) {
      auto udp = *i++;
      if( udp->outgoing->try_receive(&dns_packet,&dns_from) ) {
        // return udp!
        Ip4be caller_ip = (Ip4be)udp->incoming.addr4.sin_addr.s_addr;
        int form = 0;
        int psize = scan_response(caller_ip, dns_packet.data, dns_packet.size, &form);
        if( psize <= 0 ) {
          // drop!
          this->drop_count++;
          if( illegal_packet.size == 0 ) {
            illegal_packet.set(dns_packet);
          }
          detailf("udp illegal (will drop response)");
        }
        else {
          // return!
          udp_service->send(dns_packet.data, psize, udp->incoming);
          if( psize != dns_packet.size ) {
            this->block_count++;
            detailf("udp block: %s (0-answer)", this->query_domain.name);
          }
          else if( form==1 ) {
            this->pass_count++;
            detailf("udp pass: %s", this->query_domain.name);
          }
          else if( str_blank(this->query_domain.name) ) {
            detailf("udp passthrough (%d)", form);
          }
          else {
            detailf("udp passthrough (%d): %s", form, this->query_domain.name);
          }
        }
        // end call
        udp->outgoing->close();
        udps.erase(udp);
      }
      else {
        // check udp stall
        int64_t duration = get_duration(udp->outgoing->last_received);
        if( duration > 5*SECOND ) {
          // end call
          detailf("udp stalled (ending)");
          udp->outgoing->close();
          udps.remove(udp);
        }
      }
    }

    // start over
  }
}

//----------------------------------------------------------------

void DnsBar::background_loop()
{
  detailf("background start");
  try {
    int64_t stat_from = NULL_TIME;
    int64_t sync_from = NULL_TIME;
    tm fields;
    for(;;) {
      app_check();
      short_sleep(500*MILLI, true);
      int64_t now = get_mono();

      // *** daily ***

      int64_t utc = get_utc();
      get_host_time_fields(&fields, utc);
      int hour = fields.tm_hour;
      if( this->test == 1 ) {
        // use seconds instead to debug daily routine
        hour = fields.tm_sec;
        hour *= 24;
        hour /= 60;
      }

      // arm?
      int prev_hour = this->restart_hour - 1;
      if( prev_hour < 0 ) {
        prev_hour = 23;
      }
      if( prev_hour == hour && ! this->restart_armed ) {
        notef("Armed (hour=%02d)", hour);
        this->restart_armed = true;
      }

      // daily?
      if( this->restart_armed && this->restart_hour == hour ) {
        try {
          // daily report
          notef("Daily restart (hour=%02d)", hour);
          short_sleep(100*MILLI, true);
          this->restart_armed = false;
          disable_temp_lists(this->var_dir.c_str());
        }
        catch(const StopException& stop) {
        }
        catch(const std::exception& e) {
          log_error(e);
        }
        catch(...) {
          log_error("Exception!");
        }
        // restart!
        app_req_exit(APP_RESTART_EXIT);
      }

      // *** stat ***

      if( this->stat_period>=1000 && has_elapsed(this->stat_period,&stat_from,now) ) {
        short_sleep(100*MILLI, true);
        try {
          Hold hold(&this->mutex);
          int warnings = LOGGED_WARNINGS + SILENT_WARNINGS;
          int errors = LOGGED_ERRORS + SILENT_ERRORS;
          clear_log_counters();
          notef("stat: s=%d calls=%d pass=%d block=%d x=%d r=%d w=%d e=%d",
            this->sockets_high,
            //this->udps_max,
            //this->tcps_max,
            this->call_count,
            this->pass_count,
            this->block_count,
            this->drop_count,
            TOTAL_RESOURCES,
            warnings,
            errors);
          // start over
          this->sockets_high = 0;
          //this->udps_max = 0;
          //this->tcps_max = 0;
          this->call_count = 0;
          this->pass_count = 0;
          this->block_count = 0;
          this->drop_count = 0;
          this->nft_errors = 0;
          this->remote_errors = 0;
        }
        catch(const StopException& stop) {
        }
        catch(const std::exception& e) {
          log_error(e);
        }
        catch(...) {
          log_error("Exception!");
        }
        short_sleep(300*MILLI, true);
      }

      // *** sync ***

      if( this->sync_period>=1000 && has_elapsed(this->sync_period,&sync_from,now) )
      {
        const char* sync_ok = "dnsbar-sync.ok";
        const char* sync_err = "dnsbar-sync.err";
        const char* sync_done = "dnsbar-sync.done";

        this->sync_error.clear();

        // delete ok/done
        try {
          if( !this->report_dir.empty() ) {
            // ensure no ok file
            string path_ok = join_path(this->report_dir.c_str(), sync_ok);
            ensure_no_file(path_ok.c_str());
            try_delete_remote_file(sync_ok);
            // ensure no err file
            string path_err = join_path(this->report_dir.c_str(), sync_err);
            ensure_no_file(path_err.c_str());
            try_delete_remote_file(sync_err);
          }
        }
        catch(const StopException& stop) {
        }
        catch(const std::exception& e) {
          this->sync_error = e.what();
          log_error(e);
        }
        catch(...) {
          this->sync_error = "Exception!";
          log_error("Exception!");
        }
        short_sleep(50*MILLI, true);

        // ruleset report (conf+lists)
        try {
          {
            Hold hold(&this->mutex);
            fill_conf_report();
          }
          short_sleep(300*MILLI, true);
          save_report("dnsbar-ruleset.txt");

          // ip4 pass report
          short_sleep(50*MILLI, true);
          {
            Hold hold(&this->mutex);
            fill_ip4_report();
          }
          short_sleep(300*MILLI, true);
          save_report("dnsbar-passed.ip4");

          // ip6 pass report
          short_sleep(50*MILLI, true);
          {
            Hold hold(&this->mutex);
            fill_ip6_report();
          }
          short_sleep(300*MILLI, true);
          save_report("dnsbar-passed.ip6");

          // domain pass report
          short_sleep(50*MILLI, true);
          {
            Hold hold(&this->mutex);
            fill_pass_report();
          }
          short_sleep(300*MILLI, true);
          save_report("dnsbar-passed.txt");

          // caller block report
          short_sleep(50*MILLI, true);
          {
            Hold hold(&this->mutex);
            fill_caller_report();
          }
          short_sleep(300*MILLI, true);
          save_report("dnsbar-blocked.txt", this->block_reports);
        }
        catch(const StopException& stop) {
        }
        catch(const std::exception& e) {
          this->sync_error = e.what();
          log_error(e);
        }
        catch(...) {
          this->sync_error = "Exception!";
          log_error("Exception!");
        }
        short_sleep(50*MILLI, true);

        // update lists
        try {
          pull_list_updates(this->var_dir.c_str());
          apply_list_updates(this->var_dir.c_str());
          preload_lists(this->var_dir.c_str());
          short_sleep(50*MILLI, true);
          {
            Hold hold(&this->mutex);
            if( this->sync_error.empty() ) {
              apply_lists();
            }
            else {
              log_warning("Will not apply lists (sync error)");
            }
            save_illegal_packet();
            // start over
            this->caller_blocked.clear();
            this->nft_errors = 0;
          }
        }
        catch(const StopException& stop) {
        }
        catch(const std::exception& e) {
          this->sync_error = e.what();
          log_error(e);
        }
        catch(...) {
          this->sync_error = "Exception!";
          log_error("Exception!");
        }
        short_sleep(300*MILLI, true);

        // save ok/err/done
        try {
          if( !this->report_dir.empty() ) {
            int64_t now = get_utc();
            string timestr = format_time("%Y%m%dU%H%M%S", now);
            // ok/err
            if( this->sync_error.empty() ) {
              // ok
              this->report.clear();
              this->report.addfn("%s", timestr.c_str());
              string path_ok = join_path(this->report_dir.c_str(), sync_ok);
              save_file(path_ok.c_str(), this->report.data, this->report.size);
              touch_remote_file(sync_ok);
            }
            else {
              // err
              this->report.clear();
              this->report.addfn("%s: %s", timestr.c_str(), this->sync_error.c_str());
              string path_err = join_path(this->report_dir.c_str(), sync_err);
              save_file(path_err.c_str(), this->report.data, this->report.size);
              push_remote_file(path_err.c_str(), sync_err);
            }
            // done
            this->report.clear();
            this->report.addfn("%s", timestr.c_str());
            string path_done = join_path(this->report_dir.c_str(), sync_done);
            save_file(path_done.c_str(), this->report.data, this->report.size);
            touch_remote_file(sync_done);
          }
          this->sync_error.clear();
        }
        catch(const StopException& stop) {
        }
        catch(const std::exception& e) {
          log_error(e);
        }
        catch(...) {
          log_error("Exception!");
        }
        short_sleep(50*MILLI, true);
      }

      // start over
    }
  }
  catch(const StopException& stop) {
    detailf("stop");
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }
  detailf("background end");
}

//----------------------------------------------------------------

void DnsBar::run(const char* absexe, const char* arg)
{
  const char* conf_name = "dnsbar.conf";
  const char* sys_conf_dir = "/etc/dnsbar";
  const char* sys_data_dir = "/var/lib/dnsbar";
  const char* sys_log_dir  = "/var/log/dnsbar";
  const char* dev_conf_dir = "conf";
  const char* dev_data_dir = "data";

  LOG_LIMIT = NULL_SIZE;

  try {
    // get self_dir and its owner
    string self_file = abs_path(absexe);
    string self_bin = parent_path(self_file.c_str());
    string self_dir = parent_path(self_bin.c_str());
    string owner = get_path_owner(self_dir.c_str());

    // live/dev default
    string conf_dir;
    string data_dir;
    string log_dir;
    if( self_dir=="/" || owner=="root" ) {
      // live
      if( str_blank(arg) ) {
        conf_dir = sys_conf_dir;
      }
      else {
        conf_dir = abs_path(arg);
      }
      data_dir = sys_data_dir;
      log_dir = sys_log_dir;
    }
    else {
      // dev
      if( str_blank(arg) ) {
        conf_dir = abs_path(dev_conf_dir, self_dir.c_str());
      }
      else {
        conf_dir = abs_path(arg);
      }
      data_dir = abs_path(dev_data_dir, self_dir.c_str());
      log_dir = data_dir;
    }

    // conf_dir => current dir
    this->conf_dir = conf_dir;
    ensure_dir(conf_dir.c_str());
    set_current_dir(conf_dir.c_str());

    // load configuration (including static lists)
    string conf_path = join_path(conf_dir.c_str(), conf_name);
    check_file(conf_path.c_str());
    auto conf = load_property_file(conf_path.c_str());
    load_conf(conf, data_dir.c_str(), log_dir.c_str());
    const char* conf_mode;
    if( this->black ) {
      conf_mode = "black mode";
    }
    else {
      conf_mode = "white mode";
    }
    notef("Loaded: %s (%s)", conf_path.c_str(), conf_mode);

    fill_conf_report();
    save_report("dnsbar-conf.txt");

    // run start script
    try {
      run_script("start", this->start_script.c_str());
    }
    catch(const std::exception& e) {
      log_error(e);
    }
    catch(...) {
      log_error("Exception!");
    }

    // load initial dynamic list(s)
    load_startup_lists();
    apply_lists();

    notef("Loaded lists");

    // save initial report
    this->report.reserve(256*KIB);
    fill_conf_report();
    save_report("dnsbar-ruleset.txt");

    // *** loop ***

    // lock
    Hold hold(&this->mutex);

    // start background thread
    std::thread background(&DnsBar::background_loop, std::ref(*this));
    short_sleep(50*MILLI, true);

    // main loop
    detailf("main start");
    try {
      // will release lock during sockets.wait()
      // so background thread can step in (as short as possible)
      main_loop();
    }
    catch(const StopException& stop) {
      LOG_LIMIT = NULL_SIZE;
      log_note(stop.what());
    }
    catch(const Exception& e) {
      LOG_LIMIT = NULL_SIZE;
      app_req_exit(APP_ERROR_EXIT);
      log_error(e);
    }
    catch(const std::exception& e) {
      LOG_LIMIT = NULL_SIZE;
      app_req_exit(APP_ERROR_EXIT);
      log_error(e);
    }
    catch(...) {
      LOG_LIMIT = NULL_SIZE;
      app_req_exit(APP_ERROR_EXIT);
      log_error("Exception!");
    }
    detailf("main end");

    // wait for background thread to end
    {
      if( APP_EXIT < 0 ) {
        app_req_exit(APP_OK_EXIT);
      }
      Release release(&this->mutex);
      background.join();
    }
  }
  catch(const StopException& stop) {
    LOG_LIMIT = NULL_SIZE;
    log_note(stop.what());
  }
  catch(const Exception& e) {
    LOG_LIMIT = NULL_SIZE;
    app_req_exit(APP_ERROR_EXIT);
    log_error(e);
  }
  catch(const std::exception& e) {
    LOG_LIMIT = NULL_SIZE;
    app_req_exit(APP_ERROR_EXIT);
    log_error(e);
  }
  catch(...) {
    LOG_LIMIT = NULL_SIZE;
    app_req_exit(APP_ERROR_EXIT);
    log_error("Exception!");
  }
  LOG_LIMIT = NULL_SIZE;
  if( APP_EXIT < 0 ) {
    app_req_exit(APP_OK_EXIT);
  }

  // write final reports
  try {
    fill_day_report();
    save_day_report();
    fill_conf_report();
    save_report("dnsbar-ruleset.txt");
    fill_pass_report();
    save_report("dnsbar-passed.txt");
    fill_caller_report(); // no rotation
    save_report("dnsbar-blocked.txt");
  }
  catch(const Exception& e) { log_error(e); }
  catch(const std::exception& e) { log_error(e); }
  catch(...) { log_error("Exception!"); }

  // run stop/error/restart script
  try {
    if( APP_EXIT == APP_OK_EXIT ) {
      run_script("stop", this->stop_script.c_str());
    }
    else if( APP_EXIT == APP_RESTART_EXIT ) {
      run_script("stop", this->restart_script.c_str());
    }
    else {
      run_script("stop", this->error_restart_script.c_str());
    }
  }
  catch(const Exception& e) { log_error(e); }
  catch(const std::exception& e) { log_error(e); }
  catch(...) { log_error("Exception!"); }
}

//----------------------------------------------------------------
