#include "millicpp.h"
#include <clocale>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

/*
 * millicpp * Apache 2.0 (C) 2023 librehof.org
 */

//================================================================
// Time functions
//----------------------------------------------------------------

int64_t short_sleep(int ms, bool yield)
{
  // cap
  ms = std::max(ms, 0);
  ms = std::min(ms, SECOND-1);

  int64_t start = get_mono();
  int64_t elapsed;
  int64_t left = ms;

  // yield first? (force yield if many ms)
  if( yield || left>=50*MILLI ) {
    sched_yield();
    elapsed = get_duration(start);
    left -= elapsed;
    if( left <= 0 ) {
      return elapsed;
    }
  }

  // minimum 0.5 ms
  long nsec = (long)(TO_NANO(left));
  if( nsec <= 0 ) {
    nsec = 500*1000;
  }

  // short sleep
  timespec ns;
  ns.tv_sec = 0;
  ns.tv_nsec = nsec;
  nanosleep(&ns, nullptr);

  elapsed = get_duration(start);
  if( elapsed <= 0 ) {
    elapsed = 1;
  }
  return elapsed;
}

int64_t long_sleep(int ms)
{
  // cap
  if( ms < 100*MILLI ) { ms = 100*MILLI; }
  else if( ms > DAY ) { ms = DAY; };
  // sleep loop
  int64_t start = get_mono();
  int64_t elapsed;
  int resolution = 50;
  if( ms >= MINUTE ) {
    resolution = 200;
  }
  for(;;) {
    app_check();
    short_sleep(resolution, true);
    elapsed = get_duration(start);
    if( elapsed >= ms ) {
      return elapsed;
    }
  }
}

//----------------------------------------------------------------

int64_t get_mono()
{
  // get coarse mono as sec+nsec
  timespec ns;
  int result = clock_gettime(CLOCK_MONOTONIC_COARSE, &ns);
  if( result < 0 ) {
    throw Error("Failed to get monotonic time (COARSE)");
  }
  // sec+nsec => ms
  int64_t coarse_now = FROM_SECOND((int64_t)ns.tv_sec);
  coarse_now += FROM_NANO((int64_t)ns.tv_nsec);
  if( coarse_now == NULL_TIME ) {
    coarse_now = 0; // never NULL_TIME
  }
  return coarse_now;
}

int64_t get_duration(int64_t coarse_from, int64_t coarse_now)
{
  if( coarse_now == NULL_TIME ) {
    coarse_now = get_mono();
  }
  int64_t elapsed = coarse_now - coarse_from;
  if( elapsed < 0 ) {
    if( elapsed < -2*SECOND ) {
      throw Error("Negative time (get_duration)");
    }
    elapsed = 0;
  }
  return elapsed;
}

//----------------------------------------------------------------

int64_t get_proper_mono()
{
  // get boot mono as sec+nsec
  timespec ns;
  int result = clock_gettime(CLOCK_BOOTTIME, &ns);
  if( result < 0 ) {
    throw Error("Failed to get monotonic time (BOOTTIME)");
  }
  // sec+nsec => ms
  int64_t proper_now = FROM_SECOND((int64_t)ns.tv_sec);
  proper_now += FROM_NANO((int64_t)ns.tv_nsec);
  if( proper_now == NULL_TIME ) {
    proper_now = 0; // never NULL_TIME
  }
  return proper_now;
}

int64_t get_proper_duration(int64_t proper_from, int64_t proper_now)
{
  if( proper_now == NULL_TIME ) {
    proper_now = get_proper_mono();
  }
  int64_t proper = proper_now - proper_from;
  if( proper < 0 ) {
    if( proper < -100*MILLI ) {
      throw Error("Negative time (get_proper_duration)");
    }
    proper = 0;
  }
  return proper;
}

int64_t get_strict_duration(int64_t proper_from, int64_t coarse_from)
{
  int64_t proper = get_proper_duration(proper_from);
  int64_t coarse = get_duration(coarse_from);
  int64_t delta = abs(proper-coarse);
  if( delta > SECOND ) {
    throw Error("Pause detected during time measurement");
  }
  return proper;
}

//----------------------------------------------------------------

bool has_elapsed(int64_t period, int64_t* from, int64_t now)
{
  if( *from == NULL_TIME ) {
    // first time trigger
    *from = now;
    return true;
  }
  int64_t elapsed = now - *from;
  if( elapsed < 0 ) {
    if( elapsed < -2*SECOND ) {
      throw Error("Negative time (has_elapsed)");
    }
    elapsed = 0;
  }
  if( elapsed >= period ) {
    // periodic trigger
    *from = now;
    return true;
  }
  return false;
}

//----------------------------------------------------------------

int64_t get_nano_mono()
{
  // get mono as sec+nsec
  timespec ns;
  int result = clock_gettime(CLOCK_MONOTONIC, &ns);
  if( result < 0 ) {
    throw Error("Failed to get monotonic time (MONOTONIC)");
  }
  // sec+nsec => ns
  int64_t now = ((int64_t)(ns.tv_sec)) * 1000000000LL;
  now += (int64_t)ns.tv_nsec;
  if( now == NULL_TIME ) {
    now = 0; // never NULL_TIME
  }
  return now;
}

int64_t get_nano_duration(int64_t nano_from, int64_t nano_now)
{
  if( nano_now == NULL_TIME ) {
    nano_now = get_nano_mono();
  }
  int64_t elapsed = nano_now - nano_from;
  if( elapsed < 0 ) {
    if( elapsed < -1000000 ) {
      throw Error("Negative time (get_nano_duration)");
    }
    elapsed = 0;
  }
  return elapsed;
}

//----------------------------------------------------------------

int64_t get_utc()
{
  // get utc in sec+nsec
  timespec ns;
  int result = clock_gettime(CLOCK_REALTIME, &ns);
  if( result < 0 ) {
    throw Error("Failed to get clock (REALTIME)");
  }
  // sec+nsec => ms
  int64_t ms = FROM_SECOND((int64_t)ns.tv_sec);
  ms += FROM_NANO((int64_t)ns.tv_nsec);
  if( ms == NULL_TIME ) {
    ms = 0; // never NULL_TIME
  }
  return ms;
}

void get_time_fields(tm* fields, int64_t utc, int zone_sec)
{
  // utc ms + zone sec => time sec
  time_t time_sec = (time_t)(TO_SECOND(utc));
  time_sec += zone_sec;
  // convert into time fields
  auto ptr = gmtime_r(&time_sec, fields);
  if( ptr == nullptr ) {
    throw Error("Failed to get time fields (gmtime)");
  }
}

void get_host_time_fields(tm* fields, int64_t utc)
{
  // utc ms => sec
  time_t utc_sec = (time_t)(TO_SECOND(utc));
  // convert into host time (local time) fields
  auto ptr = localtime_r(&utc_sec, fields);
  if( ptr == nullptr ) {
    throw Error("Failed to get time fields (localtime)");
  }
}

int get_msec_field(int64_t utc_or_local)
{
  int msec = (int)(utc_or_local % SECOND);
  return msec;
}

string format_time(const char* format, int64_t utc, int zone_sec)
{
  // utc ms + zone sec => time sec
  time_t time_sec = (time_t)(TO_SECOND(utc));
  time_sec += zone_sec;

  // convert into time fields
  tm fields;
  auto ptr = gmtime_r(&time_sec, &fields);
  if( ptr == nullptr ) {
    throw Error("Failed to get time fields (gmtime)");
  }

  // format with STRINGF_MAX cap and return as string
  char buffer[STRINGF_MAX+4]; // stack memory
  int total = (int)strftime(buffer, STRINGF_MAX+1, format, &fields);
  if( total<0 || total>STRINGF_MAX ) {
    throw Error("Failed to format time");
  }
  string result = buffer;
  return result;
}

string format_host_time(const char* format, int64_t utc)
{
  // utc ms => sec
  time_t utc_sec = (time_t)(TO_SECOND(utc));

  // convert into host time fields
  tm fields;
  auto ptr = localtime_r(&utc_sec, &fields);
  if( ptr == nullptr ) {
    throw Error("Failed to get time fields (localtime)");
  }

  // format with STRINGF_MAX cap and return as string
  char buffer[STRINGF_MAX+4]; // stack memory
  int total = (int)strftime(buffer, STRINGF_MAX+1, format, &fields);
  if( total<0 || total>STRINGF_MAX ) {
    throw Error("Failed to format host time");
  }
  string result = buffer;
  return result;
}

//================================================================
// Logging
//----------------------------------------------------------------

void Log::detail(const char* line, const char* source) noexcept
{
  if( LOG_LEVEL < LOG_DETAIL ) {
    return; // skip
  }
  int limit = LOG_LIMIT;
  int count = LOGGED_DETAILS;
  if( limit>=0 && count>limit ) {
    return; // skip
  }
  // increment
  if( count < (INT_MAX>>1) ) {
    count = __sync_fetch_and_add(&LOGGED_DETAILS,1);
    if( count == limit ) {
      printf("suppressing details (limit=%d) ...\n", count);
      fflush(stdout);
      return; // skip
    }
  }
  // print
  if( LOG_LEVEL>=LOG_DETAIL && !str_blank(source) ) {
    printf("%s: %s\n", source, line);
  }
  else {
    printf("%s\n", line);
  }
  if( (count&0x7) == 0 ) {
    fflush(stdout);
  }
}

void Log::note(const char* line, const char* source) noexcept
{
  if( LOG_LEVEL < LOG_NOTE ) {
    return; // skip
  }
  int limit = LOG_LIMIT;
  int count = LOGGED_NOTES;
  if( limit>=0 && count>limit ) {
    return; // skip
  }
  // increment
  if( count < (INT_MAX>>1) ) {
    count = __sync_fetch_and_add(&LOGGED_NOTES,1);
    if( count == limit ) {
      printf("Suppressing notes (limit=%d) ...\n", count);
      fflush(stdout);
      return; // skip
    }
  }
  // print
  if( LOG_LEVEL>=LOG_DETAIL && !str_blank(source) ) {
    printf("%s: %s\n", source, line);
  }
  else {
    printf("%s\n", line);
  }
  fflush(stdout);
}

void Log::warning(const char* line, const char* source) noexcept
{
  if( LOG_LEVEL < LOG_WARNING ) {
    return; // skip
  }
  int limit = LOG_LIMIT;
  int count = LOGGED_WARNINGS;
  if( limit>=0 && count>limit ) {
    return; // skip
  }
  // increment
  if( count < (INT_MAX>>1) ) {
    count = __sync_fetch_and_add(&LOGGED_WARNINGS,1);
    if( count == limit ) {
      printf("Suppressing warnings (limit=%d) ...\n", count);
      fflush(stdout);
      return; // skip
    }
  }
  // print
  if( LOG_LEVEL>=LOG_DETAIL && !str_blank(source) ) {
    printf("WARNING: %s: %s\n", source, line);
  }
  else {
    printf("WARNING: %s\n", line);
  }
  fflush(stdout);
}

void Log::error(const char* line, const char* source) noexcept
{
  if( LOG_LEVEL < LOG_ERROR ) {
    return; // skip
  }
  int limit = LOG_LIMIT;
  int count = LOGGED_ERRORS;
  if( limit>=0 && count>limit ) {
    return; // skip
  }
  // increment
  if( count < (INT_MAX>>1) ) {
    count = __sync_fetch_and_add(&LOGGED_ERRORS,1);
    if( count == limit ) {
      printf("Suppressing errors (limit=%d) ...\n", count);
      fflush(stdout);
      return; // skip
    }
  }
  // print
  if( LOG_LEVEL>=LOG_DETAIL && !str_blank(source) ) {
    printf("ERROR: %s: %s\n", source, line);
  }
  else {
    printf("ERROR: %s\n", line);
  }
  fflush(stdout);
}

Ref<ILog> APP_LOG = new Log();

//----------------------------------------------------------------

void detailf(const char* format, va_list args, IInstance* source) noexcept
{
  if( LOG_LEVEL >= LOG_DETAIL ) {
    string line = stringf(format, args);
    if( source != nullptr ) {
      log_detail(line.c_str(), source->get_identifier().c_str());
    }
    else {
      log_detail(line.c_str());
    }
  }
}

void detailf(const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  detailf(format, args);
  va_end(args);
}

void detailf(IInstance* source, const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  detailf(format, args, source);
  va_end(args);
}

//----------------------------------------------------------------

void notef(const char* format, va_list args, IInstance* source) noexcept
{
  if( LOG_LEVEL >= LOG_NOTE ) {
    string line = stringf(format, args);
    if( source != nullptr ) {
      log_note(line.c_str(), source->get_identifier().c_str());
    }
    else {
      log_note(line.c_str());
    }
  }
}

void notef(const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  notef(format, args);
  va_end(args);
}

void notef(IInstance* source, const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  notef(format, args, source);
  va_end(args);
}

//----------------------------------------------------------------

void warningf(const char* format, va_list args, IInstance* source) noexcept
{
  if( LOG_LEVEL >= LOG_WARNING ) {
    string line = stringf(format, args);
    if( source != nullptr ) {
      log_warning(line.c_str(), source->get_identifier().c_str());
    }
    else {
      log_warning(line.c_str());
    }
  }
}

void warningf(const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  warningf(format, args);
  va_end(args);
}

void warningf(IInstance* source, const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  warningf(format, args, source);
  va_end(args);
}

//----------------------------------------------------------------

void errorf(const char* format, va_list args, IInstance* source) noexcept
{
  if( LOG_LEVEL >= LOG_ERROR ) {
    string line = stringf(format, args);
    if( source != nullptr ) {
      log_error(line.c_str(), source->get_identifier().c_str());
    }
    else {
      log_error(line.c_str());
    }
  }
}
void errorf(const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  errorf(format, args);
  va_end(args);
}

void errorf(IInstance* source, const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  errorf(format, args, source);
  va_end(args);
}

//================================================================
// Lifetime management
//----------------------------------------------------------------

volatile int APP_EXIT = APP_NO_EXIT;
volatile bool APP_INTERNAL_RESTART = false;

//----------------------------------------------------------------

// request exit
void app_req_exit(int code) noexcept
{
  if( code == APP_OK_EXIT ) {
    // higher prio
    APP_EXIT = code;
    detailf("app_req_exit(%d)", code);
  }
  else if( APP_EXIT != APP_OK_EXIT ) {
    // lower prio
    APP_EXIT = code;
    detailf("app_req_exit(%d)", code);
  }
  else {
    detailf("ongoing exit, ignoring app_req_exit(%d)", code);
  }
  APP_EXIT = code;
}

void app_exit_handler(int dummy)
{
  app_req_exit(APP_OK_EXIT);
}

// internal
int APPSTART_RESOURCES = 0;

void app_start()
{
  // set minimal "C" locale globally
  setlocale(LC_ALL, "C");

  // avoid getting SIGPIPE on ordinary file/socket operations
  signal(SIGPIPE, SIG_IGN);

  // ctrl+c (controlled shutdown)
  signal(SIGINT, app_exit_handler);

  // terminate (controlled shutdown)
  signal(SIGTERM, app_exit_handler);

  APPSTART_RESOURCES = TOTAL_RESOURCES;
}

//----------------------------------------------------------------

void app_check()
{
  // app exit check
  if( APP_EXIT >= 0 ) {
    // exit requested
    if( APP_EXIT == APP_OK_EXIT ) {
      throw StopException("Shutdown");
    }
    else if( APP_EXIT == APP_RESTART_EXIT ) {
      if( APP_INTERNAL_RESTART ) {
        throw StopException("Restart (exit)");
      }
      else {
        throw StopException("Restart exit");
      }
    }
    else {
      if( APP_INTERNAL_RESTART ) {
        throw StopException("Error (exit)");
      }
      else {
        throw StopException("Error exit");
      }
    }
  }
}

//----------------------------------------------------------------

bool app_end(int instances_max, int mallocs_max, int handles_max) noexcept
{
  bool ok = true;

  LOG_LIMIT = NULL_SIZE;

  if( APP_EXIT < 0 ) {
    app_req_exit(APP_OK_EXIT);
  }

  if( BLANK_INT != 0 ) {
    log_error("Blank string is dirty (not 0)");
    ok = false;
  }

  // TODO: end all threads but main thread

  TOTAL_RESOURCES -= APPSTART_RESOURCES;
  if( TOTAL_RESOURCES || SILENT_WARNINGS || SILENT_ERRORS ) {
    print_global_counters();
    ok = false;
  }

  if( !ok ) {
    short_sleep(SECOND);
  }

  log_detail("app_end");
  fflush(stdout);
  fflush(stderr);
  return ok;
}

//================================================================
// Handle (Instance)
//----------------------------------------------------------------

Handle::~Handle() noexcept
{
  close();
}

//----------------------------------------------------------------

string Handle::get_identifier() noexcept
{
  if( this->handle >= 0 ) {
    return stringf("Handle(%d)", this->handle);
  }
  else {
    return stringf("Handle(0x%08llx)", (LLU)this);
  }
}

//----------------------------------------------------------------

void Handle::set(int handle)
{
  if( this->handle >= 0 ) {
    // close old (ignore errors)
    close();
  }
  if( handle >= 0 ) {
    // register new
    RESOURCE_PLUS;
    this->handle = handle;
  }
}

void Handle::close() noexcept
{
  if( this->handle >= 0 ) {
    // ignore errors
    ::close(this->handle);
    this->handle = NULL_HANDLE;
    RESOURCE_MINUS;
  }
}

//================================================================
// Mutex
//----------------------------------------------------------------

Hold::Hold(timed_mutex* mutex, int stall_timeout)
{
  // cap
  if( stall_timeout == NULL_TIME ) {
    stall_timeout = STALL_TIMEOUT;
  }
  stall_timeout = std::max(stall_timeout, 100*MILLI);
  stall_timeout = std::min(stall_timeout, 1*DAY);

  // lock loop
  int64_t start = get_mono();
  int64_t elapsed;
  int resolution = 50;
  if( stall_timeout >= MINUTE ) {
    resolution = 200;
  }
  for(;;) {
    app_check();
    if( mutex->try_lock_for(std::chrono::milliseconds(resolution)) ) {
      break;
    }
    elapsed = get_duration(start);
    if( elapsed >= stall_timeout ) {
      throw Error("Lock failed (stalled)");
    }
  }
  // success
  this->locked = mutex;
}

Hold::~Hold() noexcept
{
  if( this->locked != nullptr ) {
    this->locked->unlock();
  }
}

//----------------------------------------------------------------

Release::Release(timed_mutex* mutex)
{
  mutex->unlock();
  this->unlocked = mutex;
}

Release::~Release() noexcept
{
  this->unlocked->lock();
}

//================================================================
// Shell
//----------------------------------------------------------------

int shell(const char* cmdline, ValVec<string>* outerr, bool add)
{
  if( !add && outerr!=nullptr && !outerr->empty() ) {
    outerr->clear();
  }
  Buffer buffer(STRINGF_MAX<<3);
  // run command line
  string cmd21 = cmdline;
  cmd21 += " 2>&1";
  FILE* fp = popen(cmd21.c_str(), "r");
  if (fp == NULL) {
    throw Error("Command failed: %s", cmdline);
  }
  // get stdout+stderr
  bool error = false;
  try {
    for(;;) {
      char* result = fgets(buffer.data, buffer.space, fp);
      if( result == NULL ) {
        break;
      }
      if( outerr != nullptr ) {
        int size = str_size(buffer.data);
        size = str_rspan_asciiset(buffer.data, size, "\r\n");
        outerr->add_new(buffer.data, size);
      }
    }
  }
  catch(...) {
    error = true;
  }
  // close + get status code
  int status = pclose(fp);
  if( error || !WIFEXITED(status) ) {
    return 127;
  }
  else {
    return WEXITSTATUS(status);
  }
}

//----------------------------------------------------------------

ValVec<string> shell(const char* cmdline,
  int err_min, bool last_line_err_max, bool last_line_err_255)
{
  ValVec<string> outerr;
  int status = shell(cmdline, &outerr);
  if( status >= err_min ) {
    if( last_line_err_255 && last_line_err_max>0 && status==255 ) {
      last_line_err_max = 255;
    }
    if( outerr.empty() || status>last_line_err_max ) {
      throw Error("Command failed: %s (%d)", cmdline, status);
    }
    else {
      string last = outerr.back();
      if( last.empty() ) {
        throw Error("Command failed: %s (%d)", cmdline, status);
      }
      else {
        throw Error("%s", last.c_str());
      }
    }
  }
  return outerr;
}

//================================================================
