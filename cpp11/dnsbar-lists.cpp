#include "dnsbar.h"

/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

string DnsBar::parse_list(Buffer* buffer, bool addN)
{
  // parse list
  string error;
  try {
    int start;
    int end;
    bool beginmark = false;
    bool endmark = false;
    Subref line(buffer->data, 0, buffer->size);
    for(;;) {
      // line
      end = str_find_asciiset(line.ptr, "\r\n");
      if( end == NULL_POS ) {
        end = line.space;
      }
      line.span(end);
      if( line.space <= 0 ) {
        break; // complete
      }
      // entry
      Subref entry(line.ptr, line.size, line.size);
      start = str_span_asciiset(entry.ptr, " \t");
      entry.trim(start);
      end = str_rspan_asciiset(entry.ptr, entry.size, " \t");
      entry.span(end);
      bool eqcomment = ( *entry.ptr == '#' );
      bool eqbegin = str_equal(entry.ptr, entry.size, "BEGIN");
      if( eqbegin ) {
        beginmark = true;
      }
      bool eqend = str_equal(entry.ptr, entry.size, "END");
      if( eqend ) {
        endmark = true;
      }
      else if( endmark && entry.size>0 ) {
        throw Error("Unexpected end of list file");
      }
      if( entry.size>0 && !eqcomment && !eqbegin && !eqend ) {
        // instruction entry
        end = str_find_asciiset(entry.ptr, entry.size, " \t", entry.size);
        entry.span(end);
        end += str_span_asciiset(entry.ptr+end, entry.space-end, " \t");
        if( entry.size>1 && entry.size<3 ) {
          string illegal_entry(entry.ptr, entry.size);
          error = stringf("Entry too short: %s", illegal_entry.c_str());
        }
        else if( entry.ptr[end]!='#' && entry.ptr[end]!='\r' && entry.ptr[end]!='\n' && entry.ptr[end]!='\0' ) {
          string illegal_entry(entry.ptr, entry.size);
          error = stringf("Illegal entry ending: %s", illegal_entry.c_str());
        }
        else if( entry>0 && !str_contains(entry.ptr, entry.size, ".") ) {
          // no dots
          string illegal_entry(entry.ptr, entry.size);
          error = stringf("Illegal entry format: %s", illegal_entry.c_str());
        }
        else if( entry>0 && addN ) {
          // found entry
          char first = entry.ptr[0];
          char second = entry.ptr[1];
          char last = entry.ptr[entry.size-1];
          if( first == '*' ) {
            // wildcard entry
            entry.trim(1);
            string wild(entry.ptr, entry.size);
            this->wildlistN.add(wild);
            if( second == '.' ) {
              // include base as well (implicit)
              entry.trim(1);
              string domain(entry.ptr, entry.size);
              this->matchlistN.add(domain);
            }
          }
          else if( first == '!' ) {
            // wildcard exclusion
            entry.trim(1);
            string neg(entry.ptr, entry.size);
            this->neglistN.add(neg);
            if( this->matchlistN.contains(neg) ) {
              this->matchlistN.remove(neg);
            }
          }
          else if( first>='0' && first<='9' && last>='0' && last<='9' ) {
            // ip4 entry
            string ip4str(entry.ptr, entry.size);
            PortIp ip4(ip4str.c_str(), 0);
            Ip4be ip4be = ip4.addr4.sin_addr.s_addr;
            this->ip4listN.add(ip4be);
          }
          else {
            // domain entry
            string domain(entry.ptr, entry.size);
            this->matchlistN.add(domain);
          }
        }
      }
      // next line
      line.step();
      if( str_starts_with(line.ptr,"\r\n") ) {
        start = 2;
      }
      else if( str_starts_with(line.ptr,"\n\r") ) {
        start = 2;
      }
      else {
        start = 1;
      }
      line.forward(start);
    }
    if( beginmark && !endmark ) {
      error = "Missing END mark in list file";
    }
  }
  catch(const std::exception& e) {
    error = e.what();
  }
  catch(...) {
    error = "Exception!";
  }
  return error;
}

//----------------------------------------------------------------

void DnsBar::disable_temp_list(const char* list_dir, const char* name)
{
  string temp_path = join_path(list_dir, name);
  string zero_path = temp_path + '0';
  try {
    notef("Disabling: %s", temp_path.c_str());
    // load old
    Buffer buffer;
    load_file(temp_path.c_str(), &buffer);
    Subref line(buffer.data, 0, buffer.size);
    // old -> new
    ensure_no_file(zero_path.c_str());
    File writer(zero_path.c_str());
    writer.open_w();
    int start;
    int end;
    bool beginmark = false;
    bool endmark = false;
    for(;;) {
      // line
      end = str_find_asciiset(line.ptr, "\r\n");
      if( end == NULL_POS ) {
        end = line.space;
      }
      line.span(end);
      if( line.space <= 0 ) {
        break; // complete
      }
      Subref entry(line.ptr, line.size, line.size);
      start = str_span_asciiset(entry.ptr, " \t");
      entry.trim(start);
      end = str_rspan_asciiset(entry.ptr, entry.size, " \t");
      entry.span(end);
      bool eqcomment = ( *entry.ptr == '#' );
      bool eqbegin = str_equal(entry.ptr, entry.size, "BEGIN");
      if( eqbegin ) {
        beginmark = true;
      }
      bool eqend = str_equal(entry.ptr, entry.size, "END");
      if( eqend ) {
        endmark = true;
      }
      else if( endmark && entry.size>0 ) {
        throw Error("Unexpected end of file: %s", temp_path.c_str());
      }
      if( entry.size>0 && !eqcomment && !eqbegin && !eqend ) {
        // comment out
        writer.write("#", 1);
        writer.write(line.ptr, line.size);
        writer.write("\n", 1);
      }
      else {
        // leave as is
        writer.write(line.ptr, line.size);
        writer.write("\n", 1);
      }
      // next line
      line.step();
      if( str_starts_with(line.ptr,"\r\n") ) {
        start = 2;
      }
      else if( str_starts_with(line.ptr,"\n\r") ) {
        start = 2;
      }
      else {
        start = 1;
      }
      line.forward(start);
    }
    if( beginmark && !endmark ) {
      throw Error("Missing END mark: %s", temp_path.c_str());
    }
    ensure_no_file(temp_path.c_str());
    rename_file(zero_path.c_str(), temp_path.c_str());
    // remote
    push_remote_file(temp_path.c_str(), name);
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }
}

void DnsBar::disable_temp_lists(const char* list_dir)
{
  if( str_blank(list_dir) ) {
    return; // disabled
  }
  // comment out temps
  try {
    DirWalker walker;
    walker.open(list_dir);
    string name;
    while( walker.step(&name,PATH_FILE) ) {
      if( str_ends_with(name.c_str(),intsize(name),".temp") )
      {
        // *.temp
        short_sleep(100*MILLI, true);
        // disable
        disable_temp_list(list_dir, name.c_str());
      }
      else if( str_ends_with(name.c_str(),intsize(name),".temp+") )
      {
        // *.temp+
        short_sleep(100*MILLI, true);
        // delete
        string want_path = join_path(list_dir, name.c_str());
        ensure_no_file(want_path.c_str());
        // remote
        try_delete_remote_file(name.c_str());
      }
    }
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }
  short_sleep(100*MILLI, true);
}

//----------------------------------------------------------------

void DnsBar::pull_list_update(const char* list_dir, const char* want)
{
  try {
    // remote pull list+ if exists
    string want_path = join_path(list_dir, want);
    try_pull_remote_file(want, want_path.c_str());
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
}

void DnsBar::pull_list_updates(const char* list_dir)
{
  if( str_blank(list_dir) ) {
    return; // disabled
  }
  if( this->remote_user.empty() || this->remote_host.empty() ) {
    return; // disabled
  }
  try {
    DirWalker walker;
    walker.open(list_dir);
    string name;
    while( walker.step(&name,PATH_FILE) ) {
      if( str_ends_with(name.c_str(),intsize(name),".list") ||
          str_ends_with(name.c_str(),intsize(name),".temp") )
      {
        // *.list or *.temp
        short_sleep(100*MILLI, true);
        string want = name;
        want += '+';
        // remote
        pull_list_update(list_dir, want.c_str());
      }
    }
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
  short_sleep(100*MILLI, true);
}

//----------------------------------------------------------------

void DnsBar::apply_list_update(
  const char* list_dir, const char* have, const char* want)
{
  string have_path = join_path(list_dir, have);
  string old_have = have;
  old_have += ".old";
  string old_path = join_path(list_dir, old_have.c_str());
  string want_path = join_path(list_dir, want);
  try {
    bool replace = true;
    if( path_exists(have_path.c_str(),PATH_FILE) ) {
      Buffer have_buf;
      load_file(have_path.c_str(), &have_buf);
      Buffer want_buf;
      load_file(want_path.c_str(), &want_buf);
      string error = parse_list(&want_buf, false);
      if( !error.empty() ) {
        // do not apply if error!
        throw Exception(error.c_str());
      }
      if( have_buf != want_buf ) {
        // changed: have -> old
        log_list_diff(have_path.c_str(), want_path.c_str());
        ensure_no_file(old_path.c_str());
        rename_file(have_path.c_str(), old_path.c_str());
        // remote
        try {
          push_remote_file(old_path.c_str(), old_have.c_str());
        }
        catch(const std::exception& e) {
          this->sync_error = e.what();
          log_error(e);
        }
        catch(...) {
          this->sync_error = "Exception!";
          log_error("Exception!");
        }
      }
      else {
        // equal: keep have
        replace = false;
      }
    }
    if( replace ) {
      // changed: want -> have
      ensure_no_file(have_path.c_str());
      rename_file(want_path.c_str(), have_path.c_str());
      notef("Updated: %s", have_path.c_str());
    }
    else {
      // equal: discard want
      notef("Equal: %s", have_path.c_str());
    }
    ensure_no_file(want_path.c_str());
    // remote
    try_delete_remote_file(want);
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
}

void DnsBar::apply_list_updates(const char* list_dir)
{
  if( str_blank(list_dir) ) {
    return; // disabled
  }
  try {
    DirWalker walker;
    walker.open(list_dir);
    string name;
    while( walker.step(&name,PATH_FILE) ) {
      if( str_ends_with(name.c_str(),intsize(name),".list+") ||
          str_ends_with(name.c_str(),intsize(name),".temp+") )
      {
        // *.list+ or *.temp+
        short_sleep(100*MILLI, true);
        string have(name.c_str(), name.size()-1);
        apply_list_update(list_dir, have.c_str(), name.c_str());
      }
    }
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
  short_sleep(100*MILLI, true);
}

//----------------------------------------------------------------

void DnsBar::preload_list(const char* list_dir, const char* name)
{
  // load list
  try {
    string list_path = join_path(list_dir, name);
    detailf("list: %s", list_path.c_str());
    Buffer buffer;
    load_file(list_path.c_str(), &buffer);
    string error = parse_list(&buffer, true);
    if( !error.empty() ) {
      this->sync_error = error;
      log_error(error.c_str());
    }
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
}

void DnsBar::preload_lists(const char* list_dir)
{
  if( str_blank(list_dir) ) {
    return; // disabled
  }

  // 0-lists => N-lists
  matchlistN = matchlist0;
  wildlistN  = wildlist0;
  neglistN   = neglist0;
  ip4listN   = ip4list0;

  // fill N-lists from list files
  DirWalker walker;
  walker.open(list_dir);
  string name;
  while( walker.step(&name,PATH_FILE) ) {
    if( str_ends_with(name.c_str(),intsize(name),".list") ||
        str_ends_with(name.c_str(),intsize(name),".temp") )
    {
      // *.list or *.temp
      preload_list(list_dir, name.c_str());
    }
  }

  // push remote
  try {
    DirWalker walker;
    walker.open(list_dir);
    string name;
    while( walker.step(&name,PATH_FILE) ) {
      if( str_ends_with(name.c_str(),intsize(name),".list") ||
          str_ends_with(name.c_str(),intsize(name),".temp") )
      {
        // *.list or *.temp
        string list_path = join_path(list_dir, name.c_str());
        push_remote_file(list_path.c_str(), name.c_str());
      }
    }
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::apply_lists()
{
  if( !this->sync_error.empty() ) {
    log_warning("Will not apply lists (sync error)");
    return;
  }
  // switch-in updated lists (super fast)
  detailf("applying lists");
  matchlist = move(matchlistN);
  wildlist  = move(wildlistN);
  neglist   = move(neglistN);
  ip4list   = move(ip4listN);
  if( neglist.size() > 0 ) {
    detailf("applied: match=%d, wild=%d(-%d), ip4=%d",
      (int)matchlist.size(), (int)wildlist.size(), (int)neglist.size(), (int)ip4list.size());
  }
  else {
    detailf("applied: match=%d, wild=%d, ip4=%d",
      (int)matchlist.size(), (int)wildlist.size(), (int)ip4list.size());
  }
}

//----------------------------------------------------------------

bool DnsBar::check_domain(const string& domain)
{
  if( this->neglist.contains(domain) ) {
    return false; // wild exclude
  }
  if( this->matchlist.contains(domain) ) {
    return true; // exact match
  }
  for(const auto& wild : this->wildlist) {
    if( data_ends_with(domain.c_str(),intsize(domain),wild.c_str(),intsize(wild)) ) {
      return true; // wild match
    }
  }
  // no match
  return false;
}

//----------------------------------------------------------------
