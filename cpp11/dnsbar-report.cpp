#include "dnsbar.h"
#include "dnsbar-module.h"
#include "repo.h"

/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

void DnsBar::ensure_no_script_log(const char* type)
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }
  try {
    // out
    string out = type;
    out += ".out";
    string outpath = join_path(this->report_dir.c_str(), out.c_str());
    ensure_no_file(outpath.c_str());
    try_delete_remote_file(out.c_str());

    // err
    string err = type;
    err += ".err";
    string errpath = join_path(this->report_dir.c_str(), err.c_str());
    ensure_no_file(errpath.c_str());
    try_delete_remote_file(err.c_str());
  }
  catch(const std::exception& e) {
    this->sync_error = e.what();
    log_error(e);
  }
  catch(...) {
    this->sync_error = "Exception!";
    log_error("Exception!");
  }
  short_sleep(100*MILLI, true);
}

//----------------------------------------------------------------

void DnsBar::fill_conf_report()
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }
  try {
    PortIp portip;

    // header
    this->report.clear();
    int64_t now = get_utc();
    string startstr = format_time("%Y-%m-%d %H:%M:%S UTC", this->started);
    string timestr = format_time("%Y-%m-%d %H:%M:%S UTC", now);
    this->report.addn();
    this->report.addfn("# %s %s %d %s", MODULE_LABEL, MODULE_VERSION, REPO_DATE, REPO_HASH);
    this->report.addfn("# started: %s", startstr.c_str());
    this->report.addfn("# generated: %s", timestr.c_str());

    this->report.addn();
    this->report.addfn("black=%d", black);
    this->report.addfn("verbose=%d", this->verbose);
    if( this->test > 0 ) {
      this->report.addfn("test=%d", this->test);
    }

    this->report.addn();
    this->report.addfn("var_dir='%s'", this->var_dir.c_str());
    this->report.addfn("report_dir='%s'", this->report_dir.c_str());
    this->report.addfn("log_dir='%s'", this->log_dir.c_str());

    this->report.addn();
    this->report.addfn("stat_sec=%d", TO_SECOND(this->stat_period));
    this->report.addfn("sync_sec=%d", TO_SECOND(this->sync_period));

    this->report.addn();
    this->report.addfn("incoming_ip='%s'", this->incoming.ip().c_str());
    this->report.addfn("incoming_port=%d", this->incoming.port());
    this->report.addfn("incoming_max=%d", this->incoming_max);
    this->report.addfn("outgoing_ip='%s'", this->outgoing.ip().c_str());
    this->report.addfn("outgoing_port=%d", this->outgoing.port());
    this->report.addfn("udp_buf=%d", this->udp_buf);

    this->report.addn();
    this->report.addfn("block_reports=%d", this->block_reports);

    this->report.addn();
    this->report.addfn("ttl_min_sec=%d", (int)this->ttl_min_sec);
    this->report.addfn("ttl_max_sec=%d", (int)this->ttl_max_sec);

    this->report.addn();
    if( restart_hour < 0 ) {
      this->report.addn("restart_hour=-1");
    }
    else {
      this->report.addfn("restart_hour=%02d", this->restart_hour);
    }
    if( APP_INTERNAL_RESTART ) {
      this->report.addn("restart_internal=1");
    }
    else {
      this->report.addn("restart_internal=0");
    }
    this->report.addfn("restart_armed=%d", this->restart_armed);

    this->report.addn();
    this->report.addfn("start_script='%s'", this->start_script.c_str());
    this->report.addfn("restart_script='%s'", this->restart_script.c_str());
    this->report.addfn("error_restart_script='%s'", this->error_restart_script.c_str());
    this->report.addfn("stop_script='%s'", this->stop_script.c_str());

    this->report.addn();
    this->report.addfn("diff_cmd='%s'", this->diff_cmd.c_str());

    this->report.addn();
    this->report.addfn("nft='%s'", this->nft.c_str());
    if( this->nft_clear ) {
      this->report.addn("nft_clear=1");
    }
    else {
      this->report.addn("nft_clear=0");
    }
    this->report.addfn("nft4_family='%s'", this->nft4_family.c_str());
    this->report.addfn("nft4_table='%s'", this->nft4_table.c_str());
    this->report.addfn("nft4_set='%s'", this->nft4_set.c_str());
    this->report.addfn("nft6_family='%s'", this->nft6_family.c_str());
    this->report.addfn("nft6_table='%s'", this->nft6_table.c_str());
    this->report.addfn("nft6_set='%s'", this->nft6_set.c_str());

    if( this->nft_errors > 0 ) {
      this->report.addn();
      this->report.addfn("nft_errors=%d", this->nft_errors);
    }

    this->report.addn();
    this->report.addfn("remote_user='%s'", this->remote_user.c_str());
    this->report.addfn("remote_host='%s'", this->remote_host.c_str());
    this->report.addfn("remote_dir='%s'", this->remote_dir.c_str());
    this->report.addfn("remote_log_dir='%s'", this->remote_log_dir.c_str());

    if( this->remote_errors > 0 ) {
      this->report.addn();
      this->report.addfn("remote_errors=%d", this->remote_errors);
    }

    // ip4nets
    {
      this->report.addn();
      this->report.addn("[ip4nets]");
      int count = (int)this->ip4nets.size();
      Ip4be ip4be;
      for(int pos=0; pos<count; pos+=2) {
        ip4be = this->ip4nets[pos];
        PortIp net(ip4be, 0);
        ip4be = this->ip4nets[pos+1];
        PortIp mask(ip4be, 0);
        this->report.addfn("%s/%s", net.ip().c_str(), mask.ip().c_str());
      }
    }

    // ip4list
    if( !this->ip4list.empty() ) {
      this->report.addn();
      this->report.addn("[ip4list]");
      for(auto entry : this->ip4list) {
        Ip4be ip4be = entry;
        PortIp portip;
        portip.set_ip4(ip4be);
        this->report.addn(portip.ip().c_str());
      }
    }

    // matchlist
    if( !this->matchlist.empty() ) {
      this->report.addn();
      this->report.addn("[matchlist]");
      for(auto entry : this->matchlist) {
        this->report.addn(entry.c_str());
      }
    }

    // wildlist
    if( !this->wildlist.empty() || !this->neglist.empty()  ) {
      this->report.addn();
      this->report.addn("[wildlist]");
      for(auto entry : this->wildlist) {
        this->report.addfn("*%s", entry.c_str());
      }
      for(auto entry : this->neglist) {
        this->report.addfn("!%s", entry.c_str());
      }
    }

    // startup error
    if( !this->main_error.empty() ) {
      this->report.addn();
      this->report.addfn("ERROR: %s", this->main_error.c_str());
    }
  }
  catch(const std::exception& e) {
    this->report.addn();
    this->report.addfn("ERROR: %s", e.what());
  }
  catch(...) {
    this->report.addn();
    this->report.addn("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::fill_ip4_report()
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }
  try {
    PortIp portip;
    char ipstr[IPSTR_SPACE];
    this->report.clear();
    for(auto ip4be : pset_ip4) {
      portip.set_ip4(ip4be);
      portip.get_ip(ipstr, IPSTR_SPACE);
      this->report.addn(ipstr);
    }
  }
  catch(const std::exception& error) {
    this->report.addfn("ERROR: %s", error.what());
  }
  catch(...) {
    this->report.addn("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::fill_ip6_report()
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }
  try {
    PortIp portip;
    char ipstr[IPSTR_SPACE];
    this->report.clear();
    for(auto entry : pset_ip6) {
      portip.set_ip6((uint64_t*)entry);
      portip.get_ip(ipstr, IPSTR_SPACE);
      this->report.addn(ipstr);
    }
  }
  catch(const std::exception& error) {
    this->report.addfn("ERROR: %s", error.what());
  }
  catch(...) {
    this->report.addn("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::fill_pass_report(bool add)
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }
  try {
    PortIp portip;
    char ipstr[IPSTR_SPACE];

    if( !add ) {
      // header
      this->report.clear();
      int64_t now = get_utc();
      string time = format_time("%Y-%m-%d %H:%M:%S UTC", now);
      this->report.addn();
      this->report.addfn("# %s", time.c_str());
    }

    // *** ip4 ***

    this->report.addn();
    this->report.addn("# *** passed ip4 ***");
    for(auto entry : pdomain_ip4) {
      string domain = entry.first;
      this->report.addn();
      this->report.addfn("[%s]", domain.c_str());
      for(auto& ip4be : entry.second) {
        portip.set_ip4(ip4be);
        portip.get_ip(ipstr,IPSTR_SPACE);
        this->report.addn(ipstr);
      }
    }

    // *** ip6 ***

    if( pdomain_ip6.size() > 0 ) {
      this->report.addn();
      this->report.addn("# *** passed ip6 ***");
      for(auto entry : pdomain_ip6) {
        string domain = entry.first;
        this->report.addn();
        this->report.addfn("[%s]", domain.c_str());
        for(auto& ip6be : entry.second) {
          portip.set_ip6(ip6be);
          portip.get_ip(ipstr, IPSTR_SPACE);
          this->report.addn(ipstr);
        }
      }
    }
  }
  catch(const std::exception& error) {
    this->report.addfn("ERROR: %s", error.what());
  }
  catch(...) {
    this->report.addn("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::fill_caller_report()
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }
  try {
    PortIp portip;
    char ipstr[IPSTR_SPACE];

    // header
    this->report.clear();
    int64_t now = get_utc();
    string time = format_time("%Y-%m-%d %H:%M:%S UTC", now);
    this->report.addfn("# %s", time.c_str());

    // blocked domains per ip4-caller
    this->report.addn();
    this->report.addn("# *** blocked per host ***");
    for(auto entry : this->caller_blocked) {
      Ip4be ip4be = entry.first;
      portip.set_ip4(ip4be);
      portip.get_ip(ipstr, sizeof(ipstr));
      this->report.addf("\n");
      this->report.addf("[%s]\n", ipstr);
      if( entry.second.empty() ) {
        this->report.addf("-\n");
      }
      else {
        for(const auto& domain : entry.second) {
          this->report.addf("%s\n", domain.c_str());
        }
      }
    }
  }
  catch(const std::exception& e) {
    log_error(e);
    this->report.addfn("ERROR: %s", e.what());
  }
  catch(...) {
    log_error("Exception!");
    this->report.addn("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::fill_day_report()
{
  if( this->log_dir.empty() ) {
    return; // disabled
  }
  try {
    fill_conf_report();
    fill_pass_report(true);
    this->report.addn();
    this->report.addn("# *** blocked ***");
    this->report.addn();
    for(auto domain : this->day_blocked) {
      this->report.addf("%s\n", domain.c_str());
    }
  }
  catch(const std::exception& e) {
    log_error(e);
    this->report.addfn("ERROR: %s", e.what());
  }
  catch(...) {
    log_error("Exception!");
    this->report.addn("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::save_illegal_packet()
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }
  try {
    if( this->illegal_packet.size > 0 ) {
      detailf("saving illegal packet");
      string filepath = join_path(this->report_dir.c_str(), "dnsbar-illegal.packet");
      save_file(filepath.c_str(), this->illegal_packet.data, this->illegal_packet.size);
      this->illegal_packet.clear();
    }
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::save_report(const char* name, int copies)
{
  if( this->report_dir.empty() ) {
    return; // disabled
  }

  // rotate?
  if( copies==0 || copies>1 ) {
    try {
      detailf("rotating: %s", name);
      // get highest number
      DirWalker walker;
      walker.open(this->report_dir.c_str());
      string match = name;
      match += '.';
      string entry;
      int max = 0;
      while( walker.step(&entry,PATH_FILE) ) {
        if( str_starts_with(entry.c_str(),match.c_str()) ) {
          string end(entry.c_str()+match.size());
          if( str_is_integer(end.c_str()) ) {
            int number = str_to_int(end.c_str());
            max = std::max(number, max);
          }
        }
      }
      // rotate from highest to lowest
      string entry_path;
      for(int i=max; i>=0; i--) {
        string entry;
        if( i == 0 ) {
          // name
          entry = name;
        }
        else {
          // name.i
          entry = match + std::to_string(i);
        }
        entry_path = join_path(this->report_dir.c_str(), entry.c_str());
        if( path_exists(entry_path.c_str(),PATH_FILE) ) {
          if( i >= copies ) {
            // delete!
            ensure_no_file(entry_path.c_str());
            // remote
            try_delete_remote_file(entry.c_str());
          }
          else {
            // move!
            string dest = match + std::to_string(i+1);
            string dest_path = join_path(this->report_dir.c_str(), dest.c_str());
            rename_file(entry_path.c_str(), dest_path.c_str());
            // remote
            try_move_remote_file(entry.c_str(), dest.c_str());
          }
        }
      }
    }
    catch(const std::exception& e) {
      log_error(e);
    }
    catch(...) {
      log_error("Exception!");
    }
  }

  // disabled?
  if( copies <= 0 ) {
    return;
  }

  try {
    // save report
    string filepath = join_path(this->report_dir.c_str(), name);
    detailf("saving report: %s", filepath.c_str());
    save_file(filepath.c_str(), this->report.data, this->report.size);
    // remote
    push_remote_file(filepath.c_str(), name);
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }
}

//----------------------------------------------------------------

void DnsBar::save_day_report()
{
  if( this->log_dir.empty() ) {
    return; // disabled
  }
  try {
    int64_t now = get_utc();
    string key = format_host_time("%m-%d", now);
    string name = stringf("dnsbar-%s.txt", key.c_str());
    string filepath = join_path(this->log_dir.c_str(), name.c_str());
    detailf("saving log report: %s", filepath.c_str());
    save_file(filepath.c_str(), this->report.data, this->report.size);
    // remote
    push_remote_log_file(filepath.c_str(), name.c_str());
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }
}

//----------------------------------------------------------------
