#define REPO_LABEL "dnsbar"
#define REPO_VERSION "1.5"
#define REPO_DATE 20240412
#ifdef NDEBUG
#define REPO_HASH "0a763a001e4653a1eb69697866dc68c7"
#else
#define REPO_HASH "debug"
#endif
