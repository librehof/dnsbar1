#include "millicpp.h"
#include "millicpp-module.h"
#include "repo.h"

/*
 * test
 */

#define ASSERT(x) if( !(x) ) throw Error("Test failed at line %d", __LINE__)

//----------------------------------------------------------------

extern int i1;
extern int i2;
int i1 = 0;
int i2 = 0;

void test_basic()
{
  {
    // ptr
    i1 = 0x00111100;
    i2 = 0x00222200;
    int offset = ptr_sub(&i2, &i1);
    int* ip = (int*)ptr_add(&i1, offset);
    ASSERT( *ip == 0x00222200 );

    // data
    ASSERT( !data_equal(&i1, sizeof(i1), &i2, sizeof(i2)) );
    i2 = 0x00111100;
    ASSERT( data_equal(&i1, sizeof(i1), &i2, sizeof(i2)) );
    ASSERT( data_find(&i1, sizeof(i1), ptr_add(&i1,1), 2) == 1 );
    ASSERT( data_contains(&i1, sizeof(i1), ptr_add(&i1,1), 2) );
    i2 = 0x00222200;
    ASSERT( data_find(&i1, sizeof(i1), ptr_add(&i2,1), 2) == NULL_POS );
    ASSERT( !data_contains(&i1, sizeof(i1), ptr_add(&i2,1), 2) );
    ASSERT( data_find(&i1, sizeof(i1), ptr_add(&i2,1), 2, -2) == -2 );
    char data[] = "abcdef";
    ASSERT( data_starts_with(data, str_size(data), "ab", 2) );
    ASSERT( !data_starts_with(data, str_size(data), "bc", 2) );
    ASSERT( data_ends_with(data, str_size(data), "ef", 2) );
    ASSERT( !data_ends_with(data, str_size(data), "de", 2) );

    // str
    ASSERT( str_size(data) == 6 );
    ASSERT( str_size(BLANK) == 0 );
    ASSERT( !str_blank(data) );
    ASSERT( str_blank(BLANK) );
    ASSERT( str_is_number("+1.9") );
    ASSERT( str_is_number("+1,9") );
    ASSERT( !str_is_number("+1,9.9") );
    ASSERT( !str_is_number("+1.9.9") );
    ASSERT( str_is_number("1234567890.") );
    ASSERT( str_is_number("01234567890") );
    ASSERT( !str_is_number("+1.9.") );
    ASSERT( str_is_number("-1.999999999") );
    ASSERT( str_is_number("+1.9") );
    ASSERT( !str_is_integer("+1.9") );
    ASSERT( str_is_integer("+9") );
    ASSERT( str_is_integer("-01234567890") );
    ASSERT( str_is_integer("1234567890") );
    ASSERT( !str_is_label("1234567890") );
    ASSERT( str_is_label("1234567890",BLANK,true) );
    ASSERT( str_is_label("_1234567-.azAz") );
    ASSERT( !str_is_label("$_1234567-.azAz") );
    ASSERT( str_is_label("$_1234567-.azAz", "_-.$") );
    ASSERT( str_equal(BLANK, BLANK) );
    ASSERT( str_equal(BLANK, "") );
    ASSERT( !str_equal(BLANK, "a") );
    ASSERT( str_equal("a1", "a1") );
    ASSERT( !str_equal("a1", "a11") );
    ASSERT( !str_equal("a11", "a1") );
    ASSERT( str_find("abc", "cd") == NULL_POS );
    ASSERT( str_find("abc", 2, "bc") == NULL_POS );
    ASSERT( str_find("abc", 3, "bc") == 1 );
    ASSERT( !str_contains("abc", "cd") );
    ASSERT( str_starts_with(data, "ab") );
    ASSERT( !str_starts_with(data, "bc") );
    ASSERT( str_ends_with(data, 6, "ef") );
    ASSERT( !str_ends_with(data, "de") );
    ASSERT( !str_ends_with(data, BLANK) );
    ASSERT( str_to_int("+2147483647") == INT32_MAX );
    ASSERT( str_to_int("-2147483648") == INT32_MIN );
    ASSERT( str_to_int64("+9223372036854775807") == INT64_MAX );
    ASSERT( str_to_int64("9223372036854775807") == 9223372036854775807LL );
    ASSERT( str_to_int64("+0") == 0 );
    ASSERT( str_to_int64("0") == 0 );
    ASSERT( str_to_int64("-0") == 0 );
    ASSERT( str_to_int64("-9223372036854775807") == -9223372036854775807LL );
    ASSERT( eq15(str_to_double("-9223372036.85477"),-9223372036.85477) );
    ASSERT( utf8_points("abc") == 3 );
    ASSERT( utf8_points(BLANK) == 0 );

    // double
    ASSERT( eq9(1.23456789,1.234567891) );
    ASSERT( !eq9(1.2345678,1.23456781) );
    ASSERT( eqd(1.23456789,1.24,1) );
    ASSERT( !eqd(1.23456789,1.24,2) );
  }

  {
    ValVec<string> a;
    char str[] = "abc";
    a.add("a");
    a.add_new(str,2);
    a.insert(1,"b");
    a.insert_new(a.size(), str+1, 2);
    ValVec<string> b;
    b.add("a");
    b.add("b");
    b.add("ab");
    b.add("bc");
    ASSERT( a == b );
    b.add("c");
    ASSERT( a != b );
    b.remove(4);
    ASSERT( a == b );
    a.remove(0);
    b.remove(0);
    ASSERT( a == b );
  }

  {
    ValSet<string> a;
    char str[] = "abc";
    a.add("a");
    a.add_new(str,2);
    a.add_new(str,2);
    ASSERT( a.size() == 2 );
    ASSERT( a.contains("a") );
    ASSERT( a.contains("ab") );
    ASSERT( a.remove("a") );
    ASSERT( a.size() == 1 );
    ASSERT( !a.remove("a") );
    ASSERT( a.size() == 1 );
    ValSet<string> b;
    ASSERT( a != b );
    b.add("ab");
    ASSERT( a == b );
  }

  {
    ValMap<string,string> a;
    char str[] = "abc";
    a.add("1","a");
    a.add("2", str);
    a.add("2", "b");
    a.add_missing("2", str);
    ASSERT( a.size() == 2 );
    ASSERT( a.contains("1") );
    ASSERT( a.contains("2") );
    ASSERT( a.remove("1") );
    ASSERT( a.size() == 1 );
    ASSERT( !a.remove("1") );
    ASSERT( a.size() == 1 );
    ValMap<string,string> b;
    ASSERT( a != b );
    b.add("2", "b");
    ASSERT( a == b );
    b.add("1", "a");
    ASSERT( a != b );
    ASSERT( a.get("2","") == "b" );
    ASSERT( a.get("1","") == "" );
  }
}

//----------------------------------------------------------------

void test_instance()
{
  {
    RefVec<Instance> v1;
    Ref<Instance> a = new Buffer;
    Ref<Buffer> b = new Buffer;
    b->add("hello");
    v1.reserve(100);
    int space = v1.space();
    ASSERT( space >= 100 );
    v1.add(a);
    v1.insert(0, b);
    ASSERT( v1.size() == 2 );
    v1.remove(0);
    ASSERT( v1.size() == 1 );
    RefVec<Instance> v2;
    v2.insert(0, a);
    ASSERT( v1 == v2 );
    v2.add(b);
    ASSERT( v1 != v2 );
    Instance* p = v2.at(1);
    Buffer* pp = dynamic_cast<Buffer*>(p);
    ASSERT( *pp == "hello" );
  }
  {
    Ref<Instance> a = new Buffer;
    Ref<Instance> b = new Buffer;
    RefSet<Instance> s1;
    s1.add(a);
    s1.add(a);
    s1.add(b);
    ASSERT( s1.contains(a) );
    ASSERT( s1.contains(b.ptr) );
    ASSERT( s1.size() == 2 );
    s1.remove(b);
    ASSERT( !s1.contains(b.ptr) );
    ASSERT( s1.size() == 1 );
    RefSet<Instance> s2;
    s2.add(a);
    ASSERT( s1 == s2 );
  }
  {
    Ref<Instance> a = new Buffer;
    Ref<Instance> b = new Buffer;
    RefMap<int,Instance> m1;
    m1.add(1, a);
    m1.add(1, a);
    m1.add(2, b);
    ASSERT( m1.contains(1) );
    ASSERT( m1.contains(2) );
    ASSERT( m1.size() == 2 );
    m1.remove(2);
    ASSERT( !m1.contains(2) );
    ASSERT( m1.size() == 1 );
    RefMap<int,Instance> m2;
    m2.add(1, a);
    ASSERT( m1 == m2 );
    m2.add(1, b);
    ASSERT( m1 != m2 );
    m2.add(1, a);
    ASSERT( m1 == m2 );
    m2.add_missing(1, b);
    ASSERT( m1 == m2 );
  }
}

//----------------------------------------------------------------

void test_system()
{
  int64_t coarse = get_mono();
  int64_t proper = get_proper_mono();
  short_sleep(500*MILLI);
  int64_t duration = get_strict_duration(proper, coarse);
  int delta = abs((int)(duration-500*MILLI));
  ASSERT( delta < 100*MILLI );
}

//----------------------------------------------------------------

char TMPDIR[] = "/tmp/millitest";

void test_storage()
{
  ensure_no_dir(TMPDIR, PATH_ANY);
  ensure_dir(TMPDIR);
  set_current_dir(TMPDIR);
  string tmpdir = get_current_dir();
  ASSERT( tmpdir == TMPDIR );
  string a1 = abs_path("a");
  string a2 = join_path(TMPDIR, "a");
  ASSERT( a1 == a2 );
  ASSERT( str_starts_with(a1.c_str(),TMPDIR) );
  ASSERT( str_ends_with(a1.c_str(),"a") );
  string p = parent_path(a1.c_str());
  ASSERT( p == TMPDIR );
  string leaf = path_leaf(p.c_str());
  ASSERT( leaf == "millitest" );
  string ext = path_ext(p.c_str(),false);
  ASSERT( ext == BLANK );
  ASSERT( path_ext("a.b.c",false) == "c" );
  ASSERT( path_ext("a.b.c",true) == ".c" );
  ASSERT( path_ext(".c",true) == ".c" );
  int t = get_path_type(TMPDIR);
  ASSERT( t == PATH_DIR );
  ensure_dir(a2.c_str());
  string b = join_path(TMPDIR, "b");
  ensure_link("a", b.c_str());
  ASSERT( get_path_type(b.c_str(),false) == PATH_LINK );
  ASSERT( get_path_type(b.c_str()) == PATH_DIR );
  ensure_no_link(b.c_str());
  ASSERT( get_path_type(b.c_str()) == PATH_NONE );

  ensure_no_dir(TMPDIR, PATH_ANY);
  ensure_dir(TMPDIR);
  set_current_dir(TMPDIR);
  save_file(a1.c_str(), "a\nb\n", 4);
  Buffer buffer;
  load_file(a1.c_str(), &buffer);
  ASSERT( buffer == "a\nb\n" );
  ASSERT( get_file_size(a1.c_str()) == str_size("a\nb\n") );
  string line = load_first_line(a1.c_str());
  ASSERT( line == "a" );

  buffer.clear();
  rename_file("a", "b");
  copy_file("b", "c");
  string c_path = join_path(TMPDIR, "c");
  load_file(c_path.c_str(), &buffer);
  ASSERT( buffer == "a\nb\n" );

  {
    File w;
    w.set("w");
    w.open_rw();
    w.write("abc", 3);
    w.seek(1);
    char buf[10];
    buf[1] = '\0';
    w.read(buf,1);
    ASSERT( str_equal(buf,"b") );
    w.seek(1);
    w.write("0", 1);

    File a(w.path.c_str());
    a.open_a();
    a.write("d", 1);

    File r(w.path.c_str());
    r.open_r();
    r.read(buf,4);
    buf[4] = '\0';
    ASSERT( str_equal(buf,"a0cd") );
  }

  notef("file: %s", c_path.c_str());
  int mode = get_path_mode(c_path.c_str());
  notef("mode: %04X", mode);
  string owner = get_path_owner(c_path.c_str());
  string user = get_current_user();
  ASSERT( user == owner );
  string group = get_path_group(c_path.c_str());
  notef("owner: %s:%s", owner.c_str(), group.c_str());

  ensure_no_dir(TMPDIR, PATH_ANY);

  int64_t free_bytes;
  int64_t disk_bytes;
  get_disk_info("/", &free_bytes, &disk_bytes);
}

//----------------------------------------------------------------

void test(int argc, char* argv[])
{
  //LOG_LEVEL = LOG_DETAIL;
  notef("millitest %s (%s.%d)", MODULE_VERSION, REPO_VERSION, REPO_DATE);
  test_basic();
  test_instance();
  // TODO: network
  test_system();
  test_storage();
  log_note("OK");
}

//----------------------------------------------------------------

int main(int argc, char* argv[])
{
  app_start();
  try {
    test(argc, argv);
  }
  catch(const StopException& e) {
    log_note(e.what());
  }
  catch(const Exception& e) {
    log_error(e);
  }
  catch(const std::exception& e) {
    log_error(e);
  }
  catch(...) {
    log_error("Exception!");
  }
  app_end();
  return APP_EXIT;
}

//----------------------------------------------------------------
