#include "millicpp.h"

/*
 * millicpp * Apache 2.0 (C) 2023 librehof.org
 */

//================================================================
// Global counters
//----------------------------------------------------------------

volatile int TOTAL_RESOURCES = 0;

volatile int SILENT_WARNINGS = 0;
volatile int SILENT_ERRORS = 0;

volatile int LOG_LEVEL = LOG_NOTE;
volatile int LOG_LIMIT = NULL_SIZE;
volatile int LOGGED_DETAILS = 0;
volatile int LOGGED_NOTES = 0;
volatile int LOGGED_WARNINGS = 0;
volatile int LOGGED_ERRORS = 0;

void print_global_counters()
{
#ifdef DEBUG
  notef("resources=%d, warnings=%d, errors=%d",
    TOTAL_RESOURCES, SILENT_WARNINGS, SILENT_ERRORS);
#else
  notef("warnings=%d, errors=%d\n",
    SILENT_WARNINGS, SILENT_ERRORS);
#endif
}

void clear_log_counters()
{
  LOGGED_DETAILS = 0;
  LOGGED_NOTES = 0;
  LOGGED_WARNINGS = 0;
  LOGGED_ERRORS = 0;
  if( LOG_LIMIT > 0 ) {
    detailf("cleared log counters (limit=%d)", LOG_LIMIT);
  }
}

//================================================================
// Byte string (size based)
//----------------------------------------------------------------

int data_find(const void* data, int size, const void* subdata, int subsize, int nullpos)
{
  if( subsize <= 0 ) {
    return nullpos;
  }
  int pos = 0;
  int remaining = size;
  const char* ptr;
  char byte = *(const char*)subdata;
  while( remaining >= subsize ) {
    ptr = (const char*)data + pos;
    if( memcmp(ptr, subdata, subsize) == 0 ) {
      return pos; // found subdata
    }
    // step one
    ptr++;
    remaining--;
    // step many
    ptr = (const char*)memchr(ptr, byte, remaining);
    if( ptr == nullptr ) {
      return nullpos; // end of data
    }
    pos = ptr_sub(ptr, data);
    remaining = size - pos;
  }
  return nullpos; // less then subdata remaining
}

int data_find_byte(const void* data, int size, char byte, int nullpos)
{
  const void* ptr = memchr(data, byte, size);
  if( ptr == nullptr ) {
    return nullpos;
  }
  else {
    return ptr_sub(ptr, data);
  }
}

bool data_starts_with(const void* data, int size, const void* subdata, int subsize)
{
  if( subsize <= 0 ) {
    return false;
  }
  if( subsize > size ) {
    return false;
  }
  return ( memcmp(data,subdata,subsize) == 0 );
}

bool data_ends_with(const void* data, int size, const void* subdata, int subsize)
{
  if( subsize <= 0 ) {
    return false;
  }
  if( subsize > size ) {
    return false;
  }
  const void* ptr = ptr_add(data, size-subsize);
  return ( memcmp(ptr,subdata,subsize) == 0 );
}

//================================================================
// Char string (zero terminated)
//----------------------------------------------------------------

volatile int BLANK_INT = 0;

//----------------------------------------------------------------

bool str_is_integer(const char* str)
{
  if( str_blank(str) ) {
    return false;
  }
  int pos = 0;
  for(;;) {
    char ch = str[pos++];
    if( ch == '\0' ) {
      break; // done
    }
    if( ch>='0' && ch<='9' ) {
      continue;
    }
    if( pos==1 && (ch=='+'||ch=='-') ) {
      continue;
    }
    // illegal
    return false;
  }
  // passed
  return true;
}

bool str_is_number(const char* str)
{
  if( str_blank(str) ) {
    return false;
  }
  int pos = 0;
  bool dot = false;
  for(;;) {
    char ch = str[pos++];
    if( ch == '\0' ) {
      break; // done
    }
    if( ch>='0' && ch<='9' ) {
      continue;
    }
    if( !dot && (ch=='.'||ch==',') ) {
      dot = true;
      continue;
    }
    if( pos==1 && (ch=='+'||ch=='-') ) {
      continue;
    }
    // illegal
    return false;
  }
  // passed
  return true;
}

bool str_is_label(const char* str, const char* special, bool start_with_digit)
{
  if( str_blank(str) ) {
    return false;
  }
  if( special == nullptr ) {
    special = BLANK;
  }
  int pos = 0;
  const char* special_ptr;
  for(;;) {
    char ch = str[pos++];
    if( ch == '\0' ) {
      break; // done
    }
    if( ch>='0' && ch<='9' ) {
      if( start_with_digit==false && pos==1 ) {
        return false; // starts with digit
      }
      continue;
    }
    if( ch>='A' && ch<='Z' ) {
      continue;
    }
    if( ch>='a' && ch<='z' ) {
      continue;
    }
    special_ptr = special;
    while( *special_ptr != '\0' ) {
      if( *special_ptr == ch ) {
        break; // ok
      }
      special_ptr++;
    }
    if( *special_ptr == '\0' ) {
      return false; // illegal
    }
  }
  // passed
  return true;
}

//----------------------------------------------------------------

bool str_equal(const char* left, int leftsize, const char* right)
{
  if( str_starts_with(left,leftsize,right) ) {
    if( right[leftsize] == '\0' ) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

//----------------------------------------------------------------

int str_find(const char* str, const char* substr)
{
  if( str_blank(substr) ) {
    return NULL_POS;
  }
  const char* subptr = strstr(str, substr);
  if( subptr == nullptr ) {
    return NULL_POS;
  }
  else {
    return ptr_sub(subptr,str);
  }
}

int str_find_ascii(const char* str, char ascii)
{
  const char* subptr = strchr(str, ascii);
  if( subptr == nullptr ) {
    return NULL_POS;
  }
  else {
    return ptr_sub(subptr,str);
  }
}

int str_find_asciiset(const char* str, const char* oneof)
{
  int pos = (int)strcspn(str, oneof);
  if( str[pos] == '\0' ) {
    return NULL_POS;
  }
  else {
    return pos;
  }
}

int str_find_asciiset(const char* str, int size, const char* oneof, int nullpos)
{
  int pos = 0;
  const char* ptr;
  char next;
  while( pos < size ) {
    next = str[pos];
    ptr = oneof;
    while( *ptr != '\0' ) {
      if( *ptr == next ) {
        return pos; // found
      }
      ptr++;
    }
    pos++;
  }
  return nullpos;
}

int str_rfind_asciiset(const char* str, int size, const char* oneof, int nullpos)
{
  int pos = size;
  const char* ptr;
  char next;
  while( pos > 0 ) {
    next = str[pos];
    ptr = oneof;
    while( *ptr != '\0' ) {
      if( *ptr == next ) {
        return pos; // found
      }
      ptr++;
    }
    pos--;
  }
  return nullpos;
}

//----------------------------------------------------------------

int str_span_ascii(const char* str, char ascii)
{
  // TODO: would strspn be faster
  assert( ascii != '\0' );
  int pos = 0;
  while( str[pos]==ascii ) {
    pos++;
  }
  return pos;
}

int str_span_ascii(const char* str, int size, char ascii)
{
  int pos = 0;
  while( pos<size && str[pos]==ascii ) {
    pos++;
  }
  return pos;
}

int str_span_asciiset(const char* str, const char* oneof)
{
  return (int)strspn(str, oneof);
}

int str_span_asciiset(const char* str, int size, const char* oneof)
{
  int pos = 0;
  const char* ptr;
  char next;
  while( pos < size ) {
    next = str[pos];
    ptr = oneof;
    for(;;) {
      if( *ptr == '\0' ) {
        return pos; // not found
      }
      if( *ptr == next ) {
        break; // step forward
      }
      ptr++;
    }
    pos++;
  }
  return pos; // size = all found
}

int str_rspan_ascii(const char* str, int size, char ascii)
{
  int pos = size;
  while( pos>0 && str[pos-1]==ascii ) {
    pos--;
  }
  return pos;
}

int str_rspan_asciiset(const char* str, int size, const char* oneof)
{
  int pos = size;
  const char* ptr;
  char next;
  while( pos > 0 ) {
    next = str[pos-1];
    ptr = oneof;
    for(;;) {
      if( *ptr == '\0' ) {
        return pos; // not found
      }
      if( *ptr == next ) {
        break; // step backward
      }
      ptr++;
    }
    pos--;
  }
  return pos; // 0 = all found
}

//----------------------------------------------------------------

bool str_starts_with(const char* str, const char* substr)
{
  int subsize = str_size(substr);
  if( subsize <= 0 ) {
    return false;
  }
  return ( strncmp(str,substr,subsize) == 0 );
}

bool str_starts_with(const char* str, int size, const char* substr)
{
  int subsize = str_size(substr);
  if( subsize <= 0 ) {
    return false;
  }
  if( subsize > size ) {
    return false;
  }
  return ( memcmp(str,substr,subsize) == 0 );
}

bool str_ends_with(const char* str, int size, const char* substr)
{
  int subsize = str_size(substr);
  if( subsize <= 0 ) {
    return false;
  }
  if( subsize > size ) {
    return false;
  }
  const char* ptr = ptr_add(str, size-subsize);
  return ( memcmp(ptr,substr,subsize) == 0 );
}

//----------------------------------------------------------------

// internal: 0=ok, 1=blank, 2=overflow, 3=illegal
int str_to_number(const char* str, int64_t& value, int64_t* div=nullptr)
{
  bool neg = false;
  if( *str=='+' ) {
    str++;
  }
  else if( *str=='-' ) {
    str++;
    neg = true;
  }
  int size = str_size(str);
  if( size == 0 ) {
    return 1; // blank
  }
  int pos = size;
  int64_t base = 1;
  int64_t next;
  value = 0;
  if( div != nullptr ) {
    *div = 0;
  }
  while( pos > 0 ) {
    pos--;
    char ch = str[pos];
    if( ch>='0' && ch<='9') {
      // digit
      ch -= '0';
      if( ch != '\0' ) {
        next = (int64_t)(ch) * base;
        if( next < base ) {
          return 2; // overflow
        }
        value += next;
        if( value < next ) {
          return 2; // overflow
        }
      }
      if( pos == 0 ) {
        break; // ok
      }
      next = base * 10;
      if( next < base ) {
        return 2; // overflow
      }
      base = next;
    }
    else if( ch=='.' || ch==',' ) {
      // decimal point
      if( div==nullptr || *div!=0 || base==1 ) {
        return 3; // illegal
      }
      *div = base;
    }
    else {
      return 3; // illegal
    }
  }
  if( neg ) {
    value = -value;
    if( value > 0 ) {
      return 2; // overflow
    }
  }
  return 0;
}

int str_to_int(const char* str, const char* label)
{
  if( str_blank(label) ) {
    label = "int";
  }
  int64_t value = str_to_int64(str, label);
  if( value < INT_MIN || value > INT_MAX ) {
    throw Error("Too large %s (%s)", label, str);
  }
  // success
  return (int)value;
}

int64_t str_to_int64(const char* str, const char* label)
{
  if( str_blank(label) ) {
    label = "int64";
  }
  int64_t value;
  int result = str_to_number(str, value, nullptr);
  if( result == 0 ) {
    // success
    return value;
  }
  else if( result == 1 ) {
    throw Error("Blank %s", label);
  }
  else if( result == 2 ) {
    throw Error("Too large %s (%s)", label, str);
  }
  else {
    throw Error("Illegal %s (%s)", label, str);
  }
  return 0;
}

double str_to_double(const char* str, const char* label)
{
  if( str_blank(label) ) {
    label = "number";
  }
  int64_t value;
  int64_t div;
  int result = str_to_number(str, value, &div);
  if( result == 0 ) {
    // success
    if( div == 0 ) {
      return (double)value;
    }
    else {
      return ((double)value) / (double)div;
    }
  }
  else if( result == 1 ) {
    throw Error("Blank %s", label);
  }
  else if( result == 2 ) {
    throw Error("Too large %s (%s)", label, str);
  }
  else {
    throw Error("Illegal %s (%s)", label, str);
  }
  return 0;
}

//----------------------------------------------------------------

int utf8_points(const char* str)
{
  int count = 0;
  for(;;) {
    char ch = *str;
    if( ch == '\0' ) {
      break;
    }
    // count start bytes (not 10xxxxxx)
    count += ((ch & 0xc0) != 0x80);
    str++;
  }
  return count;
}

//----------------------------------------------------------------

string stringf(const char* format, va_list args) noexcept
{
  char buffer[STRINGF_MAX+4]; // stack memory
  int total = vsnprintf(buffer, STRINGF_MAX+1, format, args);
  if( total<0 || total>STRINGF_MAX ) {
    // error
    buffer[STRINGF_MAX] = '\0';
    ERROR_PLUS;
  }
  string result = buffer;
  return result;
}

string stringf(const char* format, ...) noexcept
{
  va_list args;
  va_start(args, format);
  string result = stringf(format, args);
  va_end(args);
  return result;
}

//================================================================
// Subref
//----------------------------------------------------------------

uint16_t Subref::uint16le(int pos)
{
  assert( pos+1 < this->size );
  const uint8_t* ptr8 = (const uint8_t*)this->ptr + pos;
  uint32_t value;
  value =  (uint32_t)*ptr8++ << 0;
  value |= (uint32_t)*ptr8++ << 8;
  return (uint16_t)value;
}

uint16_t Subref::uint16be(int pos)
{
  assert( pos+1 < this->size );
  const uint8_t* ptr8 = (const uint8_t*)this->ptr + pos;
  uint32_t value;
  value =  (uint32_t)*ptr8++ << 8;
  value |= (uint32_t)*ptr8++ << 0;
  return (uint16_t)value;
}

void Subref::uint16le(int pos, uint16_t value)
{
  uint8_t* ptr8 = (uint8_t*)write() + pos;
  *ptr8++ = (uint8_t)(value >> 0);
  *ptr8++ = (uint8_t)(value >> 8);
}

void Subref::uint16be(int pos, uint16_t value)
{
  uint8_t* ptr8 = (uint8_t*)write() + pos;
  *ptr8++ = (uint8_t)(value >> 8);
  *ptr8++ = (uint8_t)(value >> 0);
}

//----------------------------------------------------------------

uint32_t Subref::uint32le(int pos)
{
  assert( pos+3 < this->size );
  const uint8_t* ptr8 = (const uint8_t*)this->ptr + pos;
  uint32_t value;
  value =  (uint32_t)*ptr8++ << 0;
  value |= (uint32_t)*ptr8++ << 8;
  value |= (uint32_t)*ptr8++ << 16;
  value |= (uint32_t)*ptr8++ << 24;
  return value;
}

uint32_t Subref::uint32be(int pos)
{
  assert( pos+3 < this->size );
  const uint8_t* ptr8 = (const uint8_t*)this->ptr + pos;
  uint32_t value;
  value =  (uint32_t)*ptr8++ << 24;
  value |= (uint32_t)*ptr8++ << 16;
  value |= (uint32_t)*ptr8++ << 8;
  value |= (uint32_t)*ptr8++ << 0;
  return value;
}

void Subref::uint32le(int pos, uint32_t value)
{
  uint8_t* ptr8 = (uint8_t*)write() + pos;
  *ptr8++ = (uint8_t)(value >> 0);
  *ptr8++ = (uint8_t)(value >> 8);
  *ptr8++ = (uint8_t)(value >> 16);
  *ptr8++ = (uint8_t)(value >> 24);
}

void Subref::uint32be(int pos, uint32_t value)
{
  uint8_t* ptr8 = (uint8_t*)write() + pos;
  *ptr8++ = (uint8_t)(value >> 24);
  *ptr8++ = (uint8_t)(value >> 16);
  *ptr8++ = (uint8_t)(value >> 8);
  *ptr8++ = (uint8_t)(value >> 0);
}

//----------------------------------------------------------------

uint64_t Subref::uint64le(int pos)
{
  assert( pos+3 < this->size );
  const uint8_t* ptr8 = (const uint8_t*)this->ptr + pos;
  uint64_t value;
  value =  (uint64_t)*ptr8++ << 0;
  value |= (uint64_t)*ptr8++ << 8;
  value |= (uint64_t)*ptr8++ << 16;
  value |= (uint64_t)*ptr8++ << 24;
  value |= (uint64_t)*ptr8++ << 32;
  value |= (uint64_t)*ptr8++ << 40;
  value |= (uint64_t)*ptr8++ << 48;
  value |= (uint64_t)*ptr8++ << 56;
  return value;
}

uint64_t Subref::uint64be(int pos)
{
  assert( pos+3 < this->size );
  const uint8_t* ptr8 = (const uint8_t*)this->ptr + pos;
  uint64_t value;
  value =  (uint64_t)*ptr8++ << 56;
  value |= (uint64_t)*ptr8++ << 48;
  value |= (uint64_t)*ptr8++ << 40;
  value |= (uint64_t)*ptr8++ << 32;
  value |= (uint64_t)*ptr8++ << 24;
  value |= (uint64_t)*ptr8++ << 16;
  value |= (uint64_t)*ptr8++ << 8;
  value |= (uint64_t)*ptr8++ << 0;
  return value;
}

void Subref::uint64le(int pos, uint64_t value)
{
  uint8_t* ptr8 = (uint8_t*)write() + pos;
  *ptr8++ = (uint8_t)(value >> 0);
  *ptr8++ = (uint8_t)(value >> 8);
  *ptr8++ = (uint8_t)(value >> 16);
  *ptr8++ = (uint8_t)(value >> 24);
  *ptr8++ = (uint8_t)(value >> 32);
  *ptr8++ = (uint8_t)(value >> 40);
  *ptr8++ = (uint8_t)(value >> 48);
  *ptr8++ = (uint8_t)(value >> 56);
}

void Subref::uint64be(int pos, uint64_t value)
{
  uint8_t* ptr8 = (uint8_t*)write() + pos;
  *ptr8++ = (uint8_t)(value >> 56);
  *ptr8++ = (uint8_t)(value >> 48);
  *ptr8++ = (uint8_t)(value >> 40);
  *ptr8++ = (uint8_t)(value >> 32);
  *ptr8++ = (uint8_t)(value >> 24);
  *ptr8++ = (uint8_t)(value >> 16);
  *ptr8++ = (uint8_t)(value >> 8);
  *ptr8++ = (uint8_t)(value >> 0);
}

//================================================================
