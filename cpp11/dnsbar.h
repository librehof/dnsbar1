#include "millicpp.h"

/*
 * dnsbar * AGPL (C) 2024 librehof.org
 */

//----------------------------------------------------------------

// Notes:

// dns over tcp (SoA message only?):
//  16-bit byte size is sent before packet (not if udp)

// non-root debugging:
//  sudo sysctl net.ipv4.ip_unprivileged_port_start=0

//----------------------------------------------------------------

struct Domain
{
  int  level;
  int  size;
  char name[260+4];

  void clear();
  bool scan(const char* packet, int psize, Subref& subdata, bool capture);

protected:
  bool subscan(const char* packet, int psize, Subref& subdata, bool capture);
};

//----------------------------------------------------------------

struct UdpDnsCall : Instance
{
  // will use global packet buffer
  PortIp incoming;
  Ref<UdpSocket> outgoing;
};

/*
struct TcpDnsCall : Instance
{
  Ref<TcpSocket> incoming;
  Ref<TcpSocket> outgoing;
};
*/

//----------------------------------------------------------------

struct DnsBar
{
  // *** conf ***

  int black = 0;
  int verbose = 0;
  int test = 0;

  // period for stat output
  int stat_period = 3600*SECOND; // [ms]

  // period for dynamic list re-load and intra-day reports
  int sync_period = 30*SECOND; // [ms]

  string conf_dir;   // static lists
  string var_dir;    // dynamic lists
  string report_dir; // intra-day reports
  string log_dir;    // daily report log

  // this service address
  PortIp incoming;

  // max ongoing calls
  int incoming_max = 1000;

  // actual "dns" service address
  PortIp outgoing;

  // udp 1/2 buffer size (-1=default,0=auto) [KiB]
  int udp_buf = 0;

  // max number of intra-day block reports
  int block_reports = 5;

  // A/AAAA ttl cap
  uint32_t ttl_min_sec = 1;
  uint32_t ttl_max_sec = 240;

  // daily restart:
  // - daily report is put in log_dir
  // - temp lists are "commented out"
  // - will clear nft4_set and nft6_set if nft_clear=1
  // - will run configured scripts
  int restart_hour = 04;
  // APP_INTERNAL_RESTART = restart_internal

  // event scrips
  string start_script = "";
  string restart_script = "";
  string error_restart_script = "";
  string stop_script = "";

  // list changes => log dir
  string diff_cmd = "";

  // nftables command path
  string nft = "/sbin/nft";

  // clear nft4_set and nft6_set on [re]start
  int nft_clear = 1;

  // nftables ip4 set
  string nft4_family; // "ip"
  string nft4_table;
  string nft4_set;

  // nftables ip6 set
  string nft6_family; // "ip6"
  string nft6_table;
  string nft6_set;

  // remote list/report sync
  string remote_user;
  string remote_host;
  string remote_dir;
  string remote_log_dir;

  // ip4,mask4,ip4,mask4,...
  ValVec<Ip4be> ip4nets;

  // *** lists ***

  // exact domains
  ValSet<string> matchlist;  // move(matchlistN)
  ValSet<string> matchlist0; // conf
  ValSet<string> matchlistN; // conf+var

  // wildcard domains (base names)
  ValSet<string> wildlist;   // move(wildlistN)
  ValSet<string> wildlist0;  // conf
  ValSet<string> wildlistN;  // conf+var

  // exclude from wildcard domains
  ValSet<string> neglist;    // move(neglistN)
  ValSet<string> neglist0;   // conf
  ValSet<string> neglistN;   // conf+var

  // any domain pointing to ip4
  ValSet<Ip4be,Ip4Less> ip4list;  // move(ip4listN)
  ValSet<Ip4be,Ip4Less> ip4list0; // conf
  ValSet<Ip4be,Ip4Less> ip4listN; // conf+var

  // *** state ***

  // utc
  int64_t started = NULL_TIME;

  // armed for restart
  int restart_armed = 0;

  // used for all udp packets
  Buffer udp_packet;
  PortIp udp_from;

  // saved in report dir
  Buffer illegal_packet;

  // hour statistics
  int sockets_high = 0;
  //int udps_max = 0;
  //int tcps_max = 0;
  int call_count = 0;
  int pass_count = 0;
  int block_count = 0;
  int drop_count = 0;

  // cmdline of failed script
  string main_error;

  // last sync error
  string sync_error;

  // nft errors => rate-limited
  int nft_errors = 0;

  // remote errors => rate-limited
  int remote_errors = 0;

  // passed domains (=> nftables)
  // cleared once per day
  ValSet<Ip4be,Ip4Less> pset_ip4;
  ValSet<Ip6be> pset_ip6;
  // domain => ip4/ip6 array
  ValMap<string,ValVec<Ip4be>> pdomain_ip4;
  ValMap<string,ValVec<Ip6be>> pdomain_ip6;

  // unmatched domains (per caller)
  // caller_ip => domain set
  // cleared after every sync period
  ValMap<Ip4be,ValSet<string>,Ip4Less> caller_blocked;

  // cleared once per day
  ValSet<string> day_blocked;

  // temporary buffers used by scan
  Domain query_domain;
  Domain answer_domain;

  // fill-and-save buffer
  Buffer report;

  // main/background exclusive lock
  timed_mutex mutex;

  // *** dnsbar ***

  // load initial conf
  void load_conf(
    const ValMap<string,string>& conf,
    const char* default_data_dir,
    const char* default_log_dir);

  // load initial dynamic list(s)
  void load_startup_lists();

  // load conf and run loops
  void run(const char* absexe, const char* arg);

  // main thread
  // releases mutex during sockets.wait()
  void main_loop();

  // background thread
  // holds mutex during periodic sync
  void background_loop();

  // *** lists ***

  // background
  // returns error or empty string on success
  string parse_list(Buffer* buffer, bool addN);

  // background
  void disable_temp_list(const char* list_dir, const char* name);
  void disable_temp_lists(const char* list_dir);

  // background
  void pull_list_update(const char* list_dir, const char* want);
  void pull_list_updates(const char* list_dir);
  void apply_list_update(const char* list_dir, const char* have, const char* want);
  void apply_list_updates(const char* list_dir);
  void preload_list(const char* list_dir, const char* name);
  void preload_lists(const char* list_dir);

  // exclusive
  void apply_lists();

  // check domain against matchlist and wildlist/neglist
  bool check_domain(const string& domain);

  // *** reports ***

  // clear any script log
  void ensure_no_script_log(const char* type);

  // exclusive
  void fill_conf_report(); // conf + lists
  void fill_ip4_report();
  void fill_ip6_report();
  void fill_pass_report(bool add=false); // passed ip4/ip6
  void fill_caller_report(); // caller_blocked
  void fill_day_report(); // conf + pass + day_blocked

  // exclusive
  void save_illegal_packet();

  // background
  void save_report(const char* name, int copies=1);
  void save_day_report();

  // *** cmd ***

  // run event script
  void run_script(const char* type, const char* script);

  void log_list_diff(const char* havepath, const char* wantpath);

  // clear nftables ip4/ip6 whitelist
  void nft4_clear();
  void nft6_clear();

  // add ip to nftables ip4/ip6 whitelist
  void nft4_add(Ip4be ip4be);
  void nft6_add(const uint64_t* ip6be);

  // push/pull
  void ensure_remote_dir(const char* destpath);
  void push_remote_file(const char* srcpath, const char* destname);
  void push_remote_log_file(const char* srcpath, const char* destname);
  void try_pull_remote_file(const char* srcname, const char* destpath);
  void try_move_remote_file(const char* srcname, const char* destname);
  void try_delete_remote_file(const char* destname);
  void touch_remote_file(const char* destname);

  // *** scan ***

  // scan DNS-response packet, return:
  //  0 => throw away (illegal)
  //  psize => send packet as is (passed/passthrough)
  //  bellow psize => send doctored 0-answer packet (blocked)
  int scan_response(Ip4be caller_ip, char* packet, int psize, int* form);

  // *** white ***

  // evaluate white domain=ip4 (A)
  bool white_ip4(
    const string& domain, Ip4be caller_ip, Ip4be answer_ip4);

  // evaluate white domain=ip6 (AAAA)
  bool white_ip6(
    const string& domain, Ip4be caller_ip, const uint64_t* answer_ip6);

  // *** black ***

  // evaluate black domain=ip4 (A)
  bool black_ip4(
    const string& domain, Ip4be caller_ip, Ip4be answer_ip4);

  // evaluate black domain=ip6 (AAAA)
  bool black_ip6(
    const string& domain, Ip4be caller_ip, const uint64_t* answer_ip6);
};

//----------------------------------------------------------------
