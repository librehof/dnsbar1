#include "millicpp.h"
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>

/*
 * millicpp * Apache 2.0 (C) 2023 librehof.org
 */

//================================================================
// File system functions
//----------------------------------------------------------------

string get_current_dir()
{
  char buffer[STRINGF_MAX+4];
  char* ptr = getcwd(buffer, STRINGF_MAX);
  if( ptr == nullptr ) {
    int code = errno;
    if( errno == 2 ) {
      throw Error("No current directory (deleted?)");
    }
    else {
      throw Error("Could not get current directory (%d)", code);
    }
  }
  string result = buffer;
  return result;
}

void set_current_dir(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (set_current_dir)");
  }
  int result = chdir(path);
  if( result != 0 ) {
    int code = errno;
    throw Error("Could not change current directory to %s (%d)", path, code);
  }
}

//----------------------------------------------------------------

bool step_path(const char* path, Subref& level)
{
  assert( path != nullptr );
  if( level.ptr == nullptr ) {
    level.init(path);
  }

  // next?
  if( level.size != 0 ) {
    level.next();
    int span = str_span_ascii(level.ptr, '/');
    level.forward(span);
  }

  // starts with '/'?
  if( path==level.ptr && *path=='/' ) {
    level.size = 1;
    return true;
  }

  // end?
  if( *level.ptr == '\0' ) {
    return false;
  }

  // find '/'
  int size = str_find_ascii(level.ptr, '/');
  if( size >= 0 ) {
    // first/next
    if( path==level.ptr && level.ptr[size-1]==':' ) {
      // <prot>:/[/]
      size++;
      if( level.ptr[size] == '/' ) {
        size++;
      }
    }
    level.size = size;
    return true;
  }
  else {
    // last
    level.size = str_size(level.ptr);
  }
  return true;
}

//----------------------------------------------------------------

string abs_path(const char* anypath, const char* current_dir)
{
  if( str_blank(anypath) ) {
    throw Error("Blank path (abs_path)");
  }

  // first entry
  Subref entry;
  if( !step_path(anypath,entry) ) {
    throw Error("Unknown path (abs_path)");
  }
  string first = entry;

  // make absolute
  string abspath;
  if( str_starts_with(first.c_str(),"/") ) {
    // absolute path
    abspath = anypath;
  }
  else if( str_ends_with(first.c_str(),"/") ) {
    // absolute protocol path (proto//)
    abspath = anypath;
  }
  else {
    // relative path
    if( anypath[0]=='.' && anypath[1]=='/' ) {
      // explicit relative (./)
      anypath += 2;
    }
    if( str_blank(current_dir) ) {
      string current = get_current_dir();
      abspath = join_path(current.c_str(), anypath);
    }
    else {
      abspath = join_path(current_dir, anypath);
    }
  }

  // quick I
  if( abspath == "/" ) {
    return abspath;
  }

  // first absolute entry
  entry.init();
  const char* absptr = abspath.c_str();
  if( !step_path(absptr,entry) ) {
    throw Error("Unexpected path (abs_path)");
  }

  // quick II
  if( !str_contains(absptr, "/..") ) {
    if( !str_contains(entry.end(), "//") ) {
      return abspath;
    }
  }

  // resolve all ".."
  string finalpath = entry;
  ValVec<string> entries;
  while( step_path(absptr,entry) ) {
    if( entry.size==2 && entry.ptr[0]=='.' && entry.ptr[1]=='.' ) {
      if( entries.empty() ) {
        throw Error("Could not resolve path: %s", absptr);
      }
      entries.erase(entries.end());
    }
    else if( entry.size > 0 ) {
      entries.add_new(entry.ptr, entry.size);
    }
  }
  // rebuild
  for(int i=0; i<entries.size(); i++) {
    if( !str_ends_with(finalpath.c_str(),intsize(finalpath),"/") ) {
      finalpath += "/";
    }
    finalpath.append(entries[i]);
  }
  return finalpath;
}

string true_path(const char* anypath)
{
  if( str_blank(anypath) ) {
    throw Error("Blank path (true_path)");
  }
  char buffer[PATH_MAX+1];
  char* ptr = realpath(anypath, buffer);
  if( ptr == nullptr ) {
    return abs_path(anypath);
  }
  string result = buffer;
  return result;
}

//----------------------------------------------------------------

string join_path(const char* base, const char* abs_or_rel)
{
  string result;
  // absoulute or no base?
  int baselen = str_size(base);
  if( abs_or_rel[0] == '/' || baselen == 0 ) {
    // return abs_or_rel as is
    result = abs_or_rel;
    return result;
  }
  // relative (base+child)
  result = base;
  if( abs_or_rel[0] == '\0' ) {
    // skip child
    return result;
  }
  if( base[baselen-1] == '/' ) {
    // remove base's end "/"
    result = result.substr(0, baselen-1);
  }
  if( str_starts_with(abs_or_rel,"./") ) {
    // remove child's start "./"
    abs_or_rel += 2;
  }
  // add
  result += "/";
  result += abs_or_rel;
  return result;
}

//----------------------------------------------------------------

string parent_path(const char* path)
{
  int endpos = str_size(path);
  if( endpos <= 1 ) {
    // empty
    string result = "";
    return result;
  }
  char last = path[endpos-1];
  if( last=='/' && str_ends_with(path,endpos,"://") ) {
    // skip http
    endpos -= 3;
  }
  else if( last=='/' || last=='\\' ) {
    // skip last
    endpos -= 1;
  }
  while( endpos > 0 ) {
    // find slash
    endpos--;
    char ch = path[endpos];
    if( ch=='/' || ch=='\\' ) {
      break; // found slash
    }
  }
  char first = path[0];
  if( endpos==0 && (first=='/'||first=='\\') ) {
    // root
    string result = "";
    result += first;
    return result;
  }
  else {
    string result = path;
    result = result.substr(0,endpos);
    if( str_ends_with(result.c_str(), ":/") ) {
      // http
      result += "/";
    }
    return result;
  }
}

string path_leaf(const char* path)
{
  int pos = str_size(path);
  while( pos > 0 ) {
    pos--;
    if( path[pos]=='/' || path[pos]=='\\' || path[pos]==':' ) {
      // extract leaf
      string result = path;
      return move(result.substr(pos+1));
    }
  }
  // everything is a leaf
  string result = path;
  return result;
}

string path_ext(const char* path, bool include_dot)
{
  int len = str_size(path);
  int pos = len;
  while( pos > 0 ) {
    pos--;
    if( path[pos]=='.' ) {
      // extract extension
      if( !include_dot ) {
        pos++;
      }
      string result = path;
      return move(result.substr(pos,len-pos));
    }
    if( path[pos]=='/' || path[pos]=='\\' || path[pos]==':' ) {
      break; // stop search
    }
  }
  // no extension
  string result = "";
  return result;
}

//----------------------------------------------------------------

int get_path_type(const char* path, bool follow_links, bool throw_on_error)
{
  if( str_blank(path) ) {
    return PATH_NONE;
  }

  // stat
  struct stat info;
  int result;
  if( follow_links ) {
    result = ::stat(path, &info);
  }
  else {
    result = ::lstat(path, &info);
  }
  if( result != 0 ) {
    int code = errno;
    if( code == ENOENT ) {
      return PATH_NONE;
    }
    if( !throw_on_error ) {
      return PATH_UNKNOWN;
    }
    string abspath = abs_path(path);
    throw Error("Inaccessible: %s (%d)", abspath.c_str(), code);
  }

  // check type
  if( S_ISLNK(info.st_mode) ) return PATH_LINK;
  if( S_ISDIR(info.st_mode) ) return PATH_DIR;
  if( S_ISREG(info.st_mode) ) return PATH_FILE;
  if( S_ISBLK(info.st_mode) ) return PATH_BLOCK;
  if( S_ISCHR(info.st_mode) ) return PATH_SERIAL;
  if( S_ISFIFO(info.st_mode) ) return PATH_FIFO;
  if( S_ISSOCK(info.st_mode) ) return PATH_SOCKET;
  return PATH_UNKNOWN;
}

bool path_exists(const char* path, int path_type)
{
  if( str_blank(path) ) {
    return PATH_NONE;
  }
  int type = get_path_type(path);
  if( type == 0 ) {
    return false;
  }
  if( path_type == PATH_ANY ) {
    return true;
  }
  return ( (type&path_type) != 0 );
}

//----------------------------------------------------------------

void check_dir(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank directory path (check_dir)");
  }
  int type = get_path_type(path, true, true);
  if( type == PATH_NONE ) {
    string abspath = abs_path(path);
    throw Error("Missing directory: %s", abspath.c_str());
  }
  else if( type != PATH_DIR ) {
    string abspath = abs_path(path);
    throw Error("Not a directory: %s", abspath.c_str());
  }
}

bool ensure_dir(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank directory path (ensure_dir)");
  }
  int type = get_path_type(path, true, true);
  if( type == PATH_NONE ) {
    // ensure parent (one level up)
    string parent = parent_path(path);
    if( !parent.empty() && parent!="/" && intsize(parent)<str_size(path) ) {
      int type = get_path_type(parent.c_str(), true, true);
      if( type == PATH_NONE ) {
        int result = mkdir(parent.c_str(), 0777);
        if( result != 0 ) {
          int code = errno;
          throw Error("Failed to create directory: %s (%d)", path, code);
        }
      }
    }
    // ensure path
    int result = mkdir(path, 0777); // 0777 & umask => actual mode
    if( result != 0 ) {
      int code = errno;
      throw Error("Failed to create directory: %s (%d)", path, code);
    }
    // created
    return true;
  }
  if( type != PATH_DIR ) {
    throw Error("Not a directory: %s", path);
  }
  // exists
  return false;
}

bool ensure_no_dir(const char* path, int content_type)
{
  int type = get_path_type(path);
  if( type == PATH_NONE ) {
    return false;
  }
  if( type != PATH_DIR ) {
    throw Error("Path is not a directory: %s", path);
  }
  // delete content
  ensure_empty_dir(path, content_type);
  // delete directory
  int result = ::rmdir(path);
  if( result != 0 ) {
    int code = errno;
    throw Error("Failed to delete directory: %s (%d)", path, code);
  }
  return true;
}

bool ensure_empty_dir(const char* path, int content_type)
{
  if( str_blank(path) ) {
    throw Error("Blank path (ensure_empty_dir)");
  }
  if( str_equal(path, "/") ) {
    throw Error("Will not clear root path (ensure_empty_dir)");
  }
  // check
  check_dir(path);
  {
    DirWalker walker;
    walker.open(path);
    string name;
    int is;
    while( walker.step(&name,PATH_ANY,&is) ) {
      if( !(is & content_type) || (is & PATH_SPECIAL) ) {
        throw Error("Directory can not be cleared, unexepected content (%s)", name.c_str());
      }
    }
  }
  // clear
  bool deleted = false;
  {
    DirWalker walker;
    walker.open(path);
    string name;
    int is;
    while( walker.step(&name,PATH_ANY,&is) ) {
      string sub_path = join_path(path, name.c_str());
      if( is == PATH_DIR ) {
        // recursive!
        deleted |= ensure_no_dir(sub_path.c_str(), PATH_ANY);
      }
      else if( is == PATH_LINK ) {
        deleted |= ensure_no_link(sub_path.c_str());
      }
      else {
        deleted |= ensure_no_file(sub_path.c_str());
      }
    }
  }
  return deleted;
}

//----------------------------------------------------------------

bool ensure_link(const char* target_path, const char* path)
{
  int type = get_path_type(path, false);
  if( type == PATH_LINK ) {
    // compare
    char buffer[256+4];
    int result = (int)::readlink(path, buffer, 256);
    if( result>0 && str_equal(target_path,buffer) ) {
      return false; // no change needed
    }
    // remove old
    ensure_no_link(path);
    type = PATH_NONE;
  }
  if( type != PATH_NONE ) {
    // error
    string abspath = abs_path(path);
    throw Error("Not a link: %s", abspath.c_str());
  }
  // create new
  int result = ::symlink(target_path, path);
  if( result != 0 ) {
    int code = errno;
    throw Error("Failed to create link: %s (%d)", path, code);
  }
  return true; // created
}

bool ensure_no_link(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (ensure_no_link)");
  }
  int type = get_path_type(path, false, true);
  if( type == PATH_NONE ) {
    return false; // no action
  }
  if( type != PATH_LINK ) {
    string abspath = abs_path(path);
    throw Error("Not a link: %s", abspath.c_str());
  }
  int result = ::unlink(path);
  if( result != 0 ) {
    int code = errno;
    string abspath = abs_path(path);
    throw Error("Failed to remove link: %s (%d)", abspath.c_str(), code);
  }
  return true; // removed
}

//----------------------------------------------------------------

void check_file(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (check_file)");
  }
  string abspath = abs_path(path);
  struct stat info;
  int result = ::stat(abspath.c_str(), &info);
  if( result != 0 ) {
    int code = errno;
    if( code == ENOENT ) {
      throw Error("Missing file: %s", abspath.c_str());
    }
    else {
      throw Error("Inaccessible: %s (%d)", abspath.c_str(), code);
    }
  }
  else if( S_ISDIR(info.st_mode) ) {
    throw Error("Not a file: %s", abspath.c_str());
  }
  else if( !S_ISREG(info.st_mode) ) {
    throw Error("Not a regular file: %s", abspath.c_str());
  }
}

bool ensure_no_file(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (ensure_no_file)");
  }
  struct stat info;
  int result = ::stat(path, &info);
  if( result != 0 ) {
    int code = errno;
    if( code == ENOENT ) {
      return false; // not found!
    }
    else {
      throw Error("Inaccessible: %s (%d)", path, code);
    }
  }
  else if( S_ISDIR(info.st_mode) ) {
    throw Error("Not a file: %s", path);
  }
  else if( !S_ISREG(info.st_mode) ) {
    throw Error("Not a regular file: %s", path);
  }
  // delete file
  result = ::remove(path);
  if( result != 0 ) {
    int code = errno;
    throw Error("Failed to delete %s (%d)", path, code);
  }
  return true;
}

//----------------------------------------------------------------

int64_t get_file_size(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (get_file_size)");
  }
  struct stat info;
  int result = ::stat(path, &info);
  if( result != 0 ) {
    int code = errno;
    string abspath = abs_path(path);
    if( code == ENOENT ) {
      throw Error("Missing file: %s", abspath.c_str());
    }
    else {
      throw Error("Inaccessible: %s (%d)", abspath.c_str(), code);
    }
  }
  else if( S_ISDIR(info.st_mode) ) {
    throw Error("Not a file: %s", path);
  }
  return (int64_t)info.st_size;
}

int64_t get_file_mtime(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (get_file_mtime)");
  }
  struct stat info;
  int result = ::stat(path, &info);
  if( result != 0 ) {
    int code = errno;
    if( code == ENOENT ) {
      throw Error("Missing file: %s", path);
    }
    else {
      throw Error("Inaccessible: %s (%d)", path, code);
    }
  }
  else if( S_ISDIR(info.st_mode) ) {
    throw Error("Not a file: %s", path);
  }
  // sec+nsec => ms
  int64_t mtime = FROM_SECOND((int64_t)info.st_mtim.tv_sec);
  mtime += FROM_NANO((int64_t)info.st_mtim.tv_nsec);
  return mtime;
}

//----------------------------------------------------------------

int load_file(const char* path, Buffer* buffer, bool add)
{
  assert( buffer != nullptr );
  if( str_blank(path) ) {
    throw Error("Blank path (load_file)");
  }
  if( !add ) {
    buffer->clear();
  }
  int64_t size64 = get_file_size(path);
  if( size64 > BLOCK_MAX ) {
    throw Error("File too large: %s", path);
  }
  int size = (int)size64;
  buffer->reserve(buffer->size + size);
  int handle = ::open(path, O_RDONLY);
  if( handle < 0 ) {
    int code = errno;
    throw Error("Inaccessible: %s (%d)", path, code);
  }
  char* end = buffer->end();
  int result = (int)::read(handle, end, size);
  ::close(handle);
  if( result < 0 ) {
    *end = '\0';
    int code = errno;
    throw Error("Failed to read from %s (%d)", path, code);
  }
  if( result != size ) {
    *end = '\0';
    throw Error("Failed to load %s", path);
  }
  buffer->add(size);
  return size;
}

//----------------------------------------------------------------

string load_first_line(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (load_first_line)");
  }
  Buffer buffer(STRINGF_MAX);
  File file;
  file.open_r(path);
  file.read(&buffer);
  int end = str_find_asciiset(buffer.data, buffer.size, "\r\n", buffer.size);
  string line(buffer.data, end);
  return line;
}

//----------------------------------------------------------------

ValMap<string,string> load_property_file(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (load_property_file)");
  }
  ValMap<string,string> result;
  Buffer buffer;
  load_file(path, &buffer);
  Subref key;
  Subref value;
  Subref tail;
  Subref line(buffer.data,0,buffer.size);
  int pos;
  for(;;) {
    // get line
    pos = str_find_asciiset(line.ptr, "\r\n");
    if( pos == NULL_POS ) {
      pos = line.space;
    }
    if( pos <= 0 ) {
      break; // end
    }
    line.span(pos);
    // key=value
    key.init(line.ptr, 0, line.size);
    pos = str_span_asciiset(key.ptr, key.space, " \t");
    key.forward(pos);
    if( *key.ptr != '#' ) {
      // not a comment
      pos = str_find_ascii(key.ptr, key.space, '=');
      if( pos == NULL_POS ) {
        // no (blank) value
        pos = str_find_ascii(key.ptr, key.space, '#', key.space);
        key.span(pos);
        value.init(key.ptr+key.space, 0, 0); // blank
      }
      else {
        // key=value
        key.span(pos);
        value.init(key.ptr, 0, key.space);
        value.forward(pos+1);
      }
      // keystr
      pos = str_rspan_asciiset(key.ptr, key.size, " \t");
      key.span(pos);
      string keystr(key.ptr, key.size);
      if( !str_is_label(keystr.c_str()) ) {
        throw Error("Illegal key (%s) in %s", keystr.c_str(), path);
      }
      // valuestr
      pos = str_span_asciiset(value.ptr, value.space, " \t");
      value.forward(pos);
      if( *value.ptr == '"' ) {
        // double-quoted value
        value.forward(1);
        pos = str_find_ascii(value.ptr, value.space, '"');
        if( pos == NULL_POS ) {
          throw Error("Missing end quote (%s) in %s", keystr.c_str(), path);
        }
        value.span(pos);
        tail.init(value.ptr, 0, value.space);
        tail.forward(pos+1);
      }
      else if( *value.ptr == '\'' ) {
        // single-quoted value
        value.forward(1);
        pos = str_find_ascii(value.ptr, value.space, '\'');
        if( pos == NULL_POS ) {
          throw Error("Missing end quote (%s) in %s", keystr.c_str(), path);
        }
        value.span(pos);
        tail.init(value.ptr, 0, value.space);
        tail.forward(pos+1);
      }
      else {
        // no-quote value
        pos = str_find_asciiset(value.ptr, value.space, " \t", value.space);
        value.span(pos);
        tail.init(value.ptr, 0, value.space);
        tail.forward(pos);
      }
      string valuestr(value.ptr, value.size);
      // check tail
      pos = str_span_asciiset(tail.ptr, tail.space, " \t");
      tail.forward(pos);
      if( tail.space!=0 && *tail.ptr!='#' ) {
        throw Error("Illegal value (%s) in %s", keystr.c_str(), path);
      }
      result.add(keystr, valuestr);
    }
    // next line
    line.step();
    pos = str_span_asciiset(line.ptr, "\r\n");
    line.forward(pos);
  }
  return result;
}

//----------------------------------------------------------------

bool save_file(const char* path, const char* data, int size)
{
  if( str_blank(path) ) {
    throw Error("Blank path (save_file)");
  }
  bool create = !path_exists(path);
  int handle = ::open(path, O_WRONLY|O_CREAT|O_TRUNC, 0666);
  if( handle < 0 ) {
    int code = errno;
    throw Error("Inaccessible: %s (%d)", path, code);
  }
  int result = (int)::write(handle, data, size);
  ::close(handle);
  if( result < 0 ) {
    int code = errno;
    throw Error("Failed to save %s (%d)", path, code);
  }
  if( result != size ) {
    throw Error("Failed to save %s", path);
  }
  return create;
}

//----------------------------------------------------------------

void rename_file(const char* src, const char* dest)
{
  if( str_blank(dest) ) {
    throw Error("Blank path (rename_file)");
  }
  check_file(src);
  int result = ::rename(src, dest);
  if( result != 0 ) {
    int code = errno;
    throw Error("Failed to rename %s to %s (%d)", src, dest, code);
  }
}

//----------------------------------------------------------------

void copy_file(const char* src, const char* dest)
{
  if( str_blank(dest) ) {
    throw Error("Blank path (copy_file)");
  }
  int64_t size64 = get_file_size(src);
  int64_t loaded = 0;
  if( size64 <= FILE_CHUNK_MAX ) {
    // small file
    Buffer buffer((int)size64);
    {
      File srcfile;
      srcfile.open_r(src);
      loaded = srcfile.read(&buffer);
    }
    save_file(dest, buffer.data, buffer.size);
  }
  else {
    // large file
    Buffer buffer(FILE_CHUNK_MAX);
    string destpath = true_path(dest);
    string destpart = destpath + ".filepart";
    ensure_no_file(destpart.c_str());
    File srcfile;
    srcfile.open_r(src);
    File destfile;
    int64_t flush_size = 0;
    destfile.open_w(destpart.c_str());
    for(;;) {
      app_check();
      int part = srcfile.read(&buffer);
      if( part <= 0 ) {
        break; // done
      }
      loaded += part;
      // we may hang here for some time if a build-up
      // of cached writes needs to be commited to disk
      destfile.write(buffer.data, buffer.size);
      flush_size += part;
      if( flush_size >= 1024*MIB ) {
        // force commit to disk
        destfile.flush();
        flush_size = 0;
      }
    }
    ensure_no_file(destpath.c_str());
    rename_file(destpart.c_str(), destpath.c_str());
  }
  if( loaded < size64 ) {
    throw Error("File copy failed: %s to %s", src, dest);
  }
}

//----------------------------------------------------------------

string get_current_user()
{
  char buffer[STRINGF_MAX+4];
  int code = getlogin_r(buffer, STRINGF_MAX);
  if( code != 0 ) {
    throw Error("Failed to get current user (getlogin_r)");
  }
  string result = buffer;
  return result;
}

//----------------------------------------------------------------

int get_path_mode(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (get_path_mode)");
  }
  struct stat info;
  int result = ::stat(path, &info);
  if( result != 0 ) {
    int code = errno;
    if( code == ENOENT ) {
      throw Error("Missing: %s", path);
    }
    else {
      throw Error("Inaccessible: %s (%d)", path, code);
    }
  }
  return (int)(info.st_mode & 0xFFF);
}

string get_path_owner(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (get_path_owner)");
  }

  struct stat info;
  int result = ::stat(path, &info);
  if( result != 0 ) {
    int code = errno;
    if( code == ENOENT ) {
      throw Error("Missing: %s", path);
    }
    else {
      throw Error("Inaccessible: %s (%d)", path, code);
    }
  }
  uid_t uid = info.st_uid;

  string name;
  struct passwd pwd;
  Buffer buffer(16384);
  struct passwd* result_ptr = nullptr;
  getpwuid_r(uid, &pwd, buffer.data, buffer.space, &result_ptr);
  if( result_ptr == nullptr ) {
    name = stringf("%d", (int)uid);
  }
  else {
    name = result_ptr->pw_name;
  }
  return name;
}

string get_path_group(const char* path)
{
  if( str_blank(path) ) {
    throw Error("Blank path (get_path_group)");
  }

  struct stat info;
  int result = ::stat(path, &info);
  if( result != 0 ) {
    int code = errno;
    if( code == ENOENT ) {
      throw Error("Missing: %s", path);
    }
    else {
      throw Error("Inaccessible: %s (%d)", path, code);
    }
  }
  gid_t gid = info.st_gid;

  string name;
  struct group grp;
  struct group* result_ptr;
  Buffer buffer(16384);
  getgrgid_r(gid, &grp, buffer.data, buffer.space, &result_ptr);
  if( result_ptr == nullptr ) {
    name = stringf("%d", (int)gid);
  }
  else {
    name = result_ptr->gr_name;
  }
  return name;
}

//----------------------------------------------------------------

void get_disk_info(const char* mount_path, int64_t* free_bytes, int64_t* disk_bytes)
{
  if( str_blank(mount_path) ) {
    throw Error("Blank path (get_disk_info)");
  }
  struct statvfs stat;
  int code = statvfs(mount_path, &stat);
  if( code != 0 ) {
    throw Error("Failed to get disk size (%s)", mount_path);
  }
  int64_t size = (int64_t)stat.f_blocks * (int64_t)stat.f_frsize;
  if( free_bytes != nullptr ) {
    *free_bytes = (int64_t)stat.f_bfree * (int64_t)stat.f_frsize;
  }
  if( disk_bytes != nullptr ) {
    *disk_bytes = size;
  }
  if( size == 0 ) {
    throw Error("Could not get valid disk size (%s)", mount_path);
  }
}

//================================================================
// File
//----------------------------------------------------------------

void File::open_r()
{
  close();
  if( this->path.empty() ) {
    throw Error("No file (open)");
  }
  check_file(this->path.c_str());
  // open
  int handle = ::open(this->path.c_str(), O_RDONLY, 0666);
  if( handle < 0 ) {
    int code = errno;
    throw Error(this, "Could not open %s (%d)", this->path.c_str(), code);
  }
  Handle::set(handle);
}

void File::open_a(bool create)
{
  close();
  if( this->path.empty() ) {
    throw Error("No file (open)");
  }
  int access = O_WRONLY|O_APPEND;
  // ensure or check
  if( create ) {
    access |= O_CREAT;
  }
  else {
    check_file(this->path.c_str());
  }
  // open
  int handle = ::open(this->path.c_str(), access, 0666);
  if( handle < 0 ) {
    int code = errno;
    throw Error(this, "Could not open %s (%d)", this->path.c_str(), code);
  }
  Handle::set(handle);
}

void File::open_w(bool create)
{
  close();
  if( this->path.empty() ) {
    throw Error("No file (open)");
  }
  int access = O_WRONLY;
  // ensure or check
  if( create ) {
    access |= O_CREAT|O_TRUNC;
  }
  else {
    check_file(this->path.c_str());
  }
  // open
  int handle = ::open(this->path.c_str(), access, 0666);
  if( handle < 0 ) {
    int code = errno;
    throw Error(this, "Could not open %s (%d)", this->path.c_str(), code);
  }
  Handle::set(handle);
}

void File::open_rw(bool create)
{
  close();
  if( this->path.empty() ) {
    throw Error("No file (open)");
  }
  int access = O_RDWR;
  // ensure or check
  if( create ) {
    access |= O_CREAT|O_TRUNC;
  }
  else {
    check_file(this->path.c_str());
  }
  // open
  int handle = ::open(this->path.c_str(), access, 0666);
  if( handle < 0 ) {
    int code = errno;
    throw Error(this, "Could not open %s (%d)", this->path.c_str(), code);
  }
  Handle::set(handle);
}

//----------------------------------------------------------------

// set position
void File::seek(int64_t pos)
{
  if( this->handle < 0 || this->path.empty() ) {
    throw Error(this, "No file");
  }
  int64_t new_pos = (int64_t)::lseek64(this->handle, (off64_t)pos, SEEK_SET);
  if( pos != new_pos ) {
    throw Error(this, "Failed to change file position (seek)");
  }
}

// seek to end and return position
int64_t File::end()
{
  if( this->handle < 0 || this->path.empty() ) {
    throw Error(this, "No file");
  }
  int64_t pos = (int64_t) ::lseek64(this->handle, 0, SEEK_END);
  if( pos < 0 ) {
    throw Error(this, "Failed to change file position (seek)");
  }
  return pos;
}

//----------------------------------------------------------------

int File::read(char* buffer, int space)
{
  if( this->handle < 0 || this->path.empty() ) {
    throw Error(this, "No file");
  }
  if( space <= 0 ) {
    throw Error(this, "File buffer with no space");
  }
  int bytes = (int)::read(this->handle, buffer, space);
  if( bytes < 0 ) {
    int code = errno;
    close();
    throw Error(this, "Read failed: %s (%d)", this->path.c_str(), code);
  }
  return bytes;
}

int File::read(Buffer* buffer, bool add)
{
  if( !add ) {
    buffer->clear();
  }
  int vacant = buffer->vacant();
  if( !add ) {
    // set (expand up to FILE_CHUNK_MAX/4)
    if( vacant<(FILE_CHUNK_MAX/4) && buffer->space<buffer->limit ) {
      buffer->reserve(buffer->space+(buffer->space/2)+4096);
      vacant = buffer->vacant();
    }
  }
  else {
    // add (expand up to limit)
    if( vacant<4096 && buffer->space<buffer->limit ) {
      buffer->reserve(buffer->space+(buffer->space/2)+4096);
      vacant = buffer->vacant();
    }
  }
  int bytes = read(buffer->end(), vacant);
  buffer->add(bytes);
  return bytes;
}

void File::write(const char* data, int size)
{
  if( this->handle < 0 || this->path.empty() ) {
    throw Error(this, "No file");
  }
  if( size <= 0 ) {
    return; // nothing to write
  }
  int written = (int)::write(this->handle, data, size);
  if( written < 0 ) {
    int code = errno;
    close();
    throw Error(this, "Write failed: %s (%d)", this->path.c_str(), code);
  }
}

//================================================================

void DirWalker::open(const char* path)
{
  if( this->stream != nullptr ) {
    closedir(this->stream);
    this->stream = nullptr;
    RESOURCE_MINUS;
  }
  if( this->path != path ) {
    this->path = path;
  }
  errno = 0;
  this->stream = opendir(path);
  if( this->stream == nullptr ) {
    int code = errno;
    throw Error(this, "Could not open directory: %s (%d)", path, code);
  }
  RESOURCE_PLUS;
};

void DirWalker::close() noexcept
{
  if( this->stream != nullptr ) {
    closedir(this->stream);
    this->stream = nullptr;
    RESOURCE_MINUS;
  }
  this->entry = nullptr;
  Handle::close();
}

bool DirWalker::find(string* name_out, const char* contains, int type, int* is_type)
{
  assert( name_out != nullptr );
  if( stream == nullptr ) {
    name_out->clear();
    throw Error(this, "Directory not open (step)");
  }
  for(;;) {
    // step
    errno = 0;
    this->entry = readdir(this->stream);
    if( this->entry == nullptr ) {
      int code = errno;
      name_out->clear();
      if( code == 0 ) {
        // end
        close();
        return false;
      }
      else {
        // error
        close();
        throw Error(this, "Could not read directory: %s (%d)", this->path.c_str(), code);
      }
    }
    // check name
    if( str_equal(this->entry->d_name,".") || str_equal(this->entry->d_name,"..") ) {
      continue; // skip
    }
    if( contains!=nullptr && str_contains(this->entry->d_name,contains) ) {
      continue; // skip
    }
    // convert type
    int is = PATH_UNKNOWN;
    switch( this->entry->d_type ) {
      case DT_LNK:  is = PATH_LINK; break;
      case DT_DIR:  is = PATH_DIR; break;
      case DT_REG:  is = PATH_FILE; break;
      case DT_BLK:  is = PATH_BLOCK; break;
      case DT_CHR:  is = PATH_SERIAL; break;
      case DT_FIFO: is = PATH_FIFO; break;
      case DT_SOCK: is = PATH_SOCKET; break;
    }
    if( !(is & type) ) {
      continue; // skip
    }
    // match
    if( name_out != nullptr ) {
      *name_out = this->entry->d_name;
    }
    if( is_type != nullptr ) {
      *is_type = is;
    }
    return true;
  }
}

//================================================================
